import {GetWaytypeTextAndColorByCode, CreateIconLeaflet,capitalize,baseweb,urlWS,FormatPercent, HasSurfaces,centerdefault, apikey_mapbox, GerSurfaceTextAndColorByCode, apiKey_openroute, GeoJsonOptions, perfilOptions, TracksOptions, cadadist,GetIconRouteType, GetIconRouteBicicleta, GetIconRouteActivity, baseurl, IconKm} from "../utils/constantes";
import {CalcPolylinesyTramosFromBD, calcularDatosRuta, formarperfil, decodePolyline, marcadoreskms,GetSurfacesChart} from '../utils/matematicas.js'
import {consoleLOG, GetParamsUserAgent, URLamigable  } from '../utils/ifelse.js'
import {latLngBounds} from 'leaflet'
import Dato from "./main/dato"
import React from "react";
import { Component } from "react";
import {Link, Router} from 'react-router-dom'
import Tramos from './main/tramos'
import ModoPolyline from './main/modopolyline'
import DatosIFrame from './main/datosiframe'
import Perfil from './main/perfil'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChartArea, faDownload,  faArrowsAltH ,  faArrowUp, faClock,  faPercent, faUser, faStar, faPencilAlt} from '@fortawesome/free-solid-svg-icons'
import { MapContainer, TileLayer, WMSTileLayer, LayersControl, Marker, Polyline,ZoomControl, Tooltip, GeoJSON, Popup,LayerGroup} from 'react-leaflet';
import DownloadViewControl from './main/downloadcontrol';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'leaflet/dist/leaflet.css';
import L, { polyline } from 'leaflet';
import ButterToast, { Cinnamon,POS_TOP, POS_CENTER  } from 'butter-toast';
import Legend from './main/legend'
import { loadModules } from 'esri-loader';
import Carousel from 'react-gallery-carousel';
import 'react-gallery-carousel/dist/index.css';

let params = null;
var kilometerschecked = true;
var markerschecked = true;
var lang;
var clase = "dafault";
var bounds = centerdefault;
let perfil = [];
let markerprofile=null;//posición del marcador que se mueve cuando avanzamos sobre el perfil
var colorhex = "#FFC625";
var clasemapa = "h-700"; //este es el alto para cuando hay imágens, porque si no lo especificamos se queda una línea gris. Esto se podría mejorar calulando el alto del div id=derecha y metiendole el mismo alto al mapa
/*VARIABLES QUE PODRIA IR EN ESTADO PERO  NO CAMBIAN*/
    var tipo_icono,tipo_texto,bicicleta_texto,bicicleta_icono,actividad_texto,actividad_icono = null;
    
/**/
var rutaID = null;
const ref = React.createRef();
class Ruta extends Component {
    
    constructor(props){
        super(props);
        var ruta = props.ruta;
        rutaID = ruta.rutaID;
        lang = props.lang;

        
        this.handleStateChangeLP  = this.handleStateChangeLP.bind(this);
        
       
        var ararryID = [];
        ruta.images.map((image) => {
            ararryID.push(image.rutaImageID);
        })

        var path = decodePolyline(ruta.polyline);
        console.debug(path.length)
        //Recordar que me inventé

        bounds = latLngBounds([path[0].lat, path[0].lng])
        for (let index = 1; index < path.length; index++) {
            bounds.extend(path[index])            
        }
        
        

        //PROBLEMÓN, YO SE LAS ELEVACIONES PERO NO SÉ DE QUÉ PUNTOS DEL PATH, PORQUE ELEVACIONES TENGO 20 Y PATH 80
        //DEBO CALCULAR QUÉ PUNTOS SON LOS QUE ESTÁN A 100M, QUE VI A HACER
        var arrayTrans = formarperfil(ruta.perfilLocations, ruta.perfilElevations);
        
        
        //Esto hay que cambiarlo, porque no tendremos los arrays que devuelve ORS tal cuales, tendremos la info como la guardamos, simples coordenadas [ini,fin,waytype,surface]
        
        var arrayResult = CalcPolylinesyTramosFromBD(path, ruta.surfaces);      
        var polylines = arrayResult[0];
        var markersTodos = arrayResult[1];
        //var markers = arrayResult[1];
        var tramosDistancias = arrayResult[2];

        var markers = [markersTodos[0], markersTodos[markersTodos.length-1]]

        var marcadoreskm = marcadoreskms(polylines, tramosDistancias);
        
        

        ruta.elevations = arrayTrans;
        if(ruta.anns != undefined && ruta.anns != null && !Array.isArray(ruta.anns))
            ruta.annotations = JSON.parse(ruta.anns);
        else
            ruta.annotations = [];
        perfil = arrayTrans;

        //ESTO NO HACE FALTA EN PRINCIPIO, PERO SI QUISIÉRAMOS RECALCULAR LOS DATOS SI QUE HARÍA FALTA, POR EJEMPLO SI QUEREMOS RECALCULAR EL DESNIVEL
        var speedMode="22";
        if(ruta.velocidad)
            speedMode = ruta.velocidad;


        let distancia = tramosDistancias.reduce((a, b) => a + b, 0);
        var datosruta = calcularDatosRuta(arrayTrans, distancia, cadadist, speedMode);
        ruta.distancia = distancia;
        ruta.alturamin = datosruta[0];
        ruta.alturamax = datosruta[1];
        ruta.descend = datosruta[2];
        ruta.ascend = datosruta[3];
        ruta.dif_altura = datosruta[4];
        ruta.tiempo = datosruta[5];
        ruta.coeficiente = datosruta[6];
        ruta.categoria = datosruta[7];


        
        var [html_tramosJOIN, percentSurfaces, percentWaytypes] = GetSurfacesChart(polylines, distancia);

        ruta.percentSurfaces = percentSurfaces;
        ruta.percentWaytypes = percentWaytypes;

        /*///*/
        var tipo_html = GetIconRouteType(ruta.tipo)
        tipo_icono=tipo_html[0];
        tipo_texto=lang[tipo_html[1]];

        var bicicleta_html = GetIconRouteBicicleta(ruta.bicicleta)
        bicicleta_icono = bicicleta_html[0];
        bicicleta_texto = lang[bicicleta_html[1]];

        var actividad_html = GetIconRouteActivity(ruta.actividad)
        actividad_icono = actividad_html[0];
        actividad_texto = lang[actividad_html[1]];

        consoleLOG("ruta CONSTRUCTOR");
        consoleLOG(ruta);

        //IMAGEs*/
        var images = [];
        ruta.images.map((image) => {
            var url = baseurl+image.image;  
                images.push({"src":url}); 
        })
        
        var optionsP = perfilOptions;
        if(ruta.opcionesPerfil)
            optionsP = JSON.parse(ruta.opcionesPerfil);

        this.state ={
            datosRuta:ruta,
            tramosDistancias:tramosDistancias,
            rutaID:ruta.rutaID,
            arrayLocalPhotos:[],
            loading:false,
            completed:true,
            resultado:true,
            streetView: 'hidden',
            openPerfil:true,
            optionsPerfil:optionsP,
            tracks:[],
            actualizado:true, 
            markers:markers,
            kms:marcadoreskm,
            images:images,
            flag:1,
            repintarPerfil:false,
            mapacargado : false,
            map:null,
            polylines:polylines,
            rating: -1,
            enviado: false,
            comment:'',
            mostrarLayers:false,
            html_tramosJOIN:html_tramosJOIN,
            tramosOterreno:1
        }

        
    }



    convertToParamas(objeto){ //operations
       return {
        'titulo':objeto.titulo,
        'descripcion':objeto.descripcion,
        'tipo':objeto.tipo,
        'terreno':objeto.terreno,
        'dificultad':objeto.dificultad,
        'ciclabilidad':objeto.ciclabilidad,
        'images':objeto.imagesID,
        'rutaID':objeto.rutaID
       }
    }

      removePhoto(id){
            var imagesID = [];
            var arrayI = [];
            this.state.images.map((image) => {
                if(image.rutaImageID != id){
                    arrayI.push(image);
                    imagesID.push(image.rutaImageID);
                }
            })
            consoleLOG(this.state.images);
            consoleLOG(arrayI);
            this.setState({images:arrayI, imagesID:imagesID});
      }

      shouldComponentUpdate(nextProps, nextState) {
        consoleLOG(this.state);
        consoleLOG(nextState);
        return this.state.flag != nextState.flag;
    }

    componentDidUpdate(prevProps, prevState) {
    
        const { map } = this.state;
        const este = this;
        if (prevState.map !== map && map) {
            map.on('zoomend', function() {
                este.changeZoom()
            });                
        }
    }

    TramoOTerreno(val)
        {
            if(this.state.tramosOterreno != val)
                this.setState({ tramosOterreno:val,   flag: this.state.flag+1});  
        }

    changeZoom(){
        const { map } = this.state;
        var zoom = map.getZoom();
        //si quitamos zoom y estaba activo, lo desactivamos
        if(zoom <= 10 && this.state.mostrarLayers){
            this.setState({ mostrarLayers:false, flag: this.state.flag+1});  
        }
        //si hacemos zoom y estaba desactivo, lo activamos
        if(zoom > 10 && !this.state.mostrarLayers){
            this.setState({ mostrarLayers:true, flag: this.state.flag+1});  
        }          
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
      }

    myClickHandler(object){
        var props = object.props;
        var nam = props.campo;
        var val = props.valor;
        this.setState({[nam]: val});
    }


    handleStateChangeLP(files) {
        this.setState({arrayLocalPhotos:files})
    }


    handeClickDownload(){
        window.open("https://lovelo.app/"+lang.lang+"/route/route_"+rutaID, "_blank");
    }

    showToastTramo(tipo, valor){
        class HtmlDiv extends React.Component {
            render() {
              return <div className="toastInfo">{this.props.pre}<b>{this.props.valor}</b>{this.props.post}</div>
            }
        }
        
        valor = valor.toUpperCase();

        if(valor == "BICYCLING"){
            valor = lang.mod_bike;
        }
        if(valor == "DRIVING"){
            valor = lang.mod_drive;
        }

        var mensaje = "";
        if(tipo == 1){
            mensaje = lang.providerInfo ;
        }
        else if(tipo == 0){
            mensaje = lang.travelModeInfo;
        }

        this.OcultarButterToastAndDiv();
        ButterToast.raise({
            timeout: 3500,
            content: <Cinnamon.Crisp scheme={Cinnamon.Crisp.SCHEME_BLUE}
            content={<HtmlDiv pre={mensaje} valor={valor} post={lang.action}/>}/>
            });
    }

    OcultarButterToastAndDiv(){
        ButterToast.dismissAll();
    }

    showToastLegend() {
        this.OcultarButterToastAndDiv();
        ButterToast.raise({
            timeout: 3500,
            content: <Cinnamon.Crisp scheme={Cinnamon.Crisp.SCHEME_BLUE}
            content={<Legend lang={lang}/>}/>
            });
    }

    HandleMouseOutChart(){
        consoleLOG("mouseout")
        if(markerprofile != null){
            this.state.map.removeLayer(markerprofile);
            markerprofile = null;
        }
    }

    HandleMouseOverChart(lat ,lng){
        var newLatLng = new L.LatLng(lat, lng);
        //consoleLOG(newLatLng);
        if(markerprofile== null){
            var iconRueda = CreateIconLeaflet("mtbpin",20);
            markerprofile = L.marker(newLatLng, {icon: iconRueda}).addTo(this.state.map);
        }
        else{
            markerprofile.setLatLng(newLatLng);
        }
    }

    AbrirPefil(){
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {openPerfil:true,repintarPerfil:false, flag: this.state.flag+1}
        });
    }

    CerrarPerfil(){
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {openPerfil:false,repintarPerfil:false, flag: this.state.flag+1}
        });
    }
    
    EditarPerfil(){
        Router.push("https://lovelo.app/planner?id="+rutaID);
        return null;
    }

    clickpolyline (event, index){
        event.originalEvent.view.L.DomEvent.stopPropagation(event)
        var poly = this.state.polylines[index];
        var travelMod = poly.travelMode;
        //clickpolycount++;        
    }

    HandleClickChart(row){
        /*
        NO PERMITIMOS EDITAR DESDE RUTA, ASÍ QUE NO HACEMOS NADA
        consoleLOG("fila clicada num:" + row)
        var pos = this.PosAnnotation(row);
        var text = '';
        var type = 'texto';
        if(pos >= 0){
            text = this.state.datosRuta.annotations[pos].text;
            type = this.state.datosRuta.annotations[pos].type;
        }
            

        this.setState((state) => {
            return {modalAnnotationIsOpen:true, RowAnnotationclicked:row, TextAnnotationclicked:text,typeAnnotationclicked:type, repintarPerfil:false, flag: this.state.flag+1}
        });*/
    }


    

    render() {
        
            
        if(this.state.loading == true){
            return(
            <div className="loading_center">
                <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-secondary" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-success" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-danger" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-warning" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-info" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-light" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-dark" role="status">
                <span className="sr-only">Loading...</span>
                </div>
            </div>
            );
        
        }else if(this.state.completed && !this.state.resultado){
            
            return(
                <div className="loading_center">
                    <div className="alert alert-danger">
                        <div>{lang.error_route_created}</div>                 
                        <Link to={"./"}>{lang.home}</Link>       
                        <Link to={"/ruta/"+params.rutaID}>{lang.goruta}</Link>
                    </div>
                </div>
                );
                
        }
        else{

            consoleLOG("render main");
            
            const {error, isLoading } = this.state;
            if (error) {
                return <div>Error: {error.message}</div>;
            } 
            
            
                   
            consoleLOG("se renderiza y el streetview es " + this.state.streetView);

            var urlfondo = "";
            if(this.state.datosRuta.images.length > 0){
                urlfondo=baseurl+this.state.datosRuta.images[0].image;
            }


            var usernameRuta = this.state.datosRuta.username;
            var titulo = this.state.datosRuta.titulo;
            var descripcion = (this.state.datosRuta.descripcion.length == 0) ? lang.no_description : this.state.datosRuta.descripcion;
            var numratings = this.state.datosRuta.numRatings;
            var rating = this.state.datosRuta.rating;
            var comments = this.state.datosRuta.comments;

            var percentSurfaces = this.state.datosRuta.percentSurfaces;
            var percentWaytypes = this.state.datosRuta.percentWaytypes;

            var mostrarTerrenoBajoPerfil = true;

            var surfacePrintPerfil  = this.state.html_tramosJOIN && mostrarTerrenoBajoPerfil && HasSurfaces(percentSurfaces);



            let distancia = this.state.datosRuta.distancia;
            var pendiente = 0;
            if(distancia > 0)
                pendiente = ((this.state.datosRuta.dif_altura / distancia) * 100).toFixed(1) ;        
            distancia = (distancia /1000).toFixed(1);
            var ascenso = this.state.datosRuta.ascend;
            var tiempo = this.state.datosRuta.tiempo;

            return (        
                    
                    <div className="w-100 h-100" lang={lang.lang} style={{background:"white"}}>                                  
                        
                        
                        {isLoading &&
                            <div className="loading_center h-100-50">
                                <div className="spinner-grow text-primary" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-secondary" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-success" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-danger" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-warning" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-info" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-light" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-dark" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                            </div>
                        } 
                        
                            <div className="col-12 h-100">
                                
                                <ButterToast timeout={100000} position={{vertical: POS_TOP,  horizontal: POS_CENTER }} style={{ top: '100px',  right: '100px' }}/>
                                <div className="h-100">
                                                                  
                                    { !this.state.openPerfil && this.state.datosRuta.elevations.length > 0 &&
                                        <div className="btn-grupo abrirPerfil" role="group" aria-label="..." >
                                            <button onClick ={()=>this.AbrirPefil()} title={lang.abrir_perfil} name="mode" type="button" className={"btn-light btn-sm"}>
                                                <FontAwesomeIcon icon={faChartArea} aria-hidden="true" size="lg"/>
                                            </button>
                                        </div>
                                    }        
                                  
                                    { this.state.openPerfil && this.state.datosRuta.elevations.length > 0 &&
                                        <div id="menuPerfil" className="container-fluid" onMouseLeave={()=>this.HandleMouseOutChart()} >
                                            <div className="cajablanca h-100" >
                                                <div className="col-12 h-100-10">
                                                    <Perfil titulo={" - " + titulo} polylines={this.state.polylines} surfacePrintPerfil={surfacePrintPerfil} page="iframe" optionsPerfil={this.state.optionsPerfil} repintar={this.state.repintarPerfil} EditarPerfil={this.EditarPerfil.bind(this)} CerrarPerfil={this.CerrarPerfil.bind(this)} HandleMouseOverChart={this.HandleMouseOverChart.bind(this)} HandleClickChart={this.HandleClickChart.bind(this)} datos = {this.state.datosRuta} lang={lang}/>
                                                </div>

                                                {this.state.html_tramosJOIN && mostrarTerrenoBajoPerfil && HasSurfaces(percentSurfaces) &&
                                                    <div className="col-12">
                                                        <div className="progress col-12 tramoJoin" style={{paddingBottom:'2px'}} onClick={() =>this.showToastLegend()}>
                                                        {this.state.html_tramosJOIN}
                                                        </div>
                                                    </div>

                                                }
                                            </div>
                                        
                                            

                                        </div>   
                                    }
                                    

                                    <MapContainer className="h-100 iframediv" bounds={bounds} zoomControl={false} scrollWheelZoom={true} whenCreated={(map) => this.setState({ map, flag: this.state.flag+1 })}>
                                        <DownloadViewControl
                                                title={"Download"} handeClickDownload={this.handeClickDownload.bind(this)}lang={lang}/>

                                        <LayersControl position="topright">
                                            <LayersControl.BaseLayer checked name="Gmaps">
                                                <TileLayer
                                                attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                                url='https://mt1.google.com/vt/lyrs=r&x={x}&y={y}&z={z}'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="Gmaps Satellite">
                                                <TileLayer
                                                attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                                url='https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="OpenStreetMap">
                                                <TileLayer
                                                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="Esri">
                                                <TileLayer
                                                attribution='Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
                                                url='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="ArcGIS">
                                                <TileLayer
                                                attribution='ArcGIS Streets'
                                                url='http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="MapBox">
                                                <TileLayer       
                                                attribution='© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>'                             
                                                maxZoom='24'
                                                id='mapbox/streets-v11'
                                                accessToken='pk.eyJ1IjoiaGlnaW5pbyIsImEiOiJjanN5bXR1OXAwZ3dlNDRyMnk3Mzc0emY0In0.w19t4Sn9OjLjr9qaXFfkcQ'
                                                url={"https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token="+apikey_mapbox}
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="OpenCycleMap">
                                                <TileLayer
                                                attribution='&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                                url='https://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey={apikey}'
                                                apikey='569b8242922348a2a54db0b454ef67ae'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="IGN">
                                                <WMSTileLayer
                                                    attribution='&copy; <a href="https://www.ign.es/web/ign/portal">IGN</a>'
                                                    projection= 'EPSG:3857'    
                                                    url="https://tms-mapa-raster.ign.es/1.0.0/mapa-raster/{z}/{x}/{-y}.jpeg"
                                                />
                                            </LayersControl.BaseLayer>

                                           
                                            <LayersControl.Overlay name="routes">
                                                <TileLayer
                                                attribution='Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | Map style: &copy; <a href="https://waymarkedtrails.org">waymarkedtrails.org</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
                                                url='https://tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png'
                                                opacity='0.5'
                                                />
                                            </LayersControl.Overlay>
                                            <LayersControl.Overlay name="googleroads">
                                                <TileLayer
                                                attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                                url='https://mt1.google.com/vt/lyrs=h&x={x}&y={y}&z={z}'
                                                opacity='0.5'
                                                />
                                            </LayersControl.Overlay>
                                            <LayersControl.Overlay name="catastro">
                                                <WMSTileLayer
                                                attribution='<a target="_BLANK" href="https://www.google.es/maps/preview">Google Maps</a>'
                                                url="https://ovc.catastro.meh.es/cartografia/INSPIRE/spadgcwms.aspx"
                                                opacity='0.4'
                                                layers={'CP.CadastralParcel'}
                                                />
                                            </LayersControl.Overlay>
                                            <LayersControl.Overlay name="P.National">
                                                <WMSTileLayer
                                                attribution='Ministerio para la Transici&oacute;n Ecol&oacute;gica y el Reto Demogr&aacute;fico'
                                                url='http://sigred.oapn.es/geoserverOAPN/LimitesParquesNacionalesZPP/ows?'
                                                opacity='0.4'
                                                layers={'view_red_oapn_limite_pn'}
                                                format= {'image/png'}
                                                />
                                            </LayersControl.Overlay>

                                        
                                            {this.state.tracks.map((track, idy) => {
                                                return(      
                                                    <LayersControl.Overlay key={"layerc"+idy} checked name={track.properties.name}>
                                                        <LayerGroup>    
                                                        <GeoJSON style={TracksOptions} key={'track-'+track.properties.key} data={track}>
                                                            </GeoJSON>
                                                        </LayerGroup>
                                                    </LayersControl.Overlay>
                                                )
                                            })}
                                        </LayersControl> 
                                        <ZoomControl position="topright" zoomInText="+" zoomOutText="-" />
                                        

                                        {this.state.mostrarLayers &&
                                            this.state.kms.map((km, idx) => {
                                                //consoleLOG(marker)
                                                return (
                                                    <Marker
                                                        key={`km-${idx}`} 
                                                        ref={`km-${idx}`} 
                                                        position={km}
                                                        draggable={false}
                                                        icon={IconKm(idx+1)}>
                                                    </Marker>
                                                )
                                            })
                                            
                                        }
                                        {                                             
                                            this.state.markers.map((marker, idx) => {
                                                //consoleLOG(marker)
                                                var iconi;
                                                if(idx == 0){
                                                    iconi = CreateIconLeaflet("circleini",10);
                                                }
                                                else{
                                                    iconi = CreateIconLeaflet("circlefin",10);
                                                }
                                                return (
                                                    <Marker
                                                        key={`marker2-${idx}`} 
                                                        ref={`marker2-${idx}`} 
                                                        position={marker}
                                                        draggable={false}
                                                        icon={iconi}>
                                                    </Marker>
                                                )
                                            })
                                            
                                        }
                                        

                                        

                                        {this.state.polylines.map((polyl, idy) => {
                                            //consoleLOG("GEOOOO PINTA")
                                            //consoleLOG(polyl);
                                            return(                                         
                                            <GeoJSON ref={this.btnRef} style={GeoJsonOptions} eventHandlers={{
                                                click: (e) => {
                                                this.clickpolyline(e, idy);
                                                } 
                                            }} key={'geojson-'+polyl.key} data={polyl.pathSurfaces}>
                                            
                                            <Popup keepInView={true} className="PopupPolyline">
                                                <ModoPolyline distance={polyl.distance} id={idy} travelMode={polyl.travelMode} lang={lang} directions_prov={polyl.provider} mode="show"></ModoPolyline>    
                                            </Popup>
                                            
                                            </GeoJSON>
                                            )
                                        })}
                                    
                                    </MapContainer>
                                </div>
                        
                                    
                                    
                                    
                                <div id="menuDatos" className="col-lg-4 col-12" >
                                        


                                        <div className="panel panel-container" style={{backgroundColor:"white"}}>
                                            <div className="row   border-bottom border-top">
                                                    <Dato icono={faArrowsAltH} texto={lang.distancia} unidad="km" valor={distancia} border="border-right" resalta="true"/>
                                                    <Dato icono={faArrowUp} texto={lang.desnivel} unidad="m" valor={ascenso} border=""  resalta="true"/>
                                                    <Dato icono={faClock} texto={lang.tiempo} unidad="" valor={tiempo+"'"} border="border-left"  resalta="true"/>
                                                    <Dato icono={faPercent} texto={lang.pendiente} unidad="%" valor={pendiente} border="border-left"  resalta="true"/>
                                            </div>
                                            
                                        
                                    </div>
                                </div>

                              
                               
                                
                            </div>
                                            
                        
                       
                        
                        
                        
                   
                            
                    </div>
            );
        }
        
    }
};

export default Ruta;