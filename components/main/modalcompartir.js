import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBrush, faEdit, faLayerGroup, faPaintBrush, faPencilAlt, faPlug, faPlus, faPlusCircle, faUpload } from "@fortawesome/free-solid-svg-icons";
import React, { createRef } from "react";
import Modal from 'react-responsive-modal'
import { withRouter } from "react-router-dom";
import logo from '../../public/images/layers.png'
import { route } from 'next/dist/server/router';

let bg = {
    overlay: {
      //lo que queda feura del modal
    },
    modal:{
        background: "white",
        width:'90%',
        textAlign: "center",
        fontFamily: "Helvetica, Helvetica Neue, Arial"
    },
    closeButton:{
        width:'25px',
        top:'2px',
        rigth:'0px'
    }
  };

let lang = null;
class ModalCompartir extends React.Component {
    
    constructor(props){
        super(props);
        lang=this.props.lang;
    }

    
    updateParent(){
        this.props.toggleShowModalCallback()
        this.setState({texto: '', type: ''});
    }

    

    render(){      
                    
            return (             
                
                <Modal open={this.props.abierto} onClose={this.updateParent.bind(this)} center styles={bg}>
                    <div className="modalperfil_title">{lang.compartir_title}<hr></hr></div>
                    <div className="container-fluid margintop modalimportar">
                                
                                <div className="form-group">                                    
                                        <span>{lang.compartir_ruta}</span>                                        
                                </div>
                                <div className="form-group">                                    
                                    <label>{this.props.link}</label>
                                </div>
                                <div className="form-group mt-5">                                    
                                        <span>{lang.insertar_ruta}</span>
                                </div>
                                <div className="form-group">                                    
                                <label>{this.props.iframe}</label>
                                </div>
                    </div>                         
                </Modal>
            );

     }
}
 
export default ModalCompartir;