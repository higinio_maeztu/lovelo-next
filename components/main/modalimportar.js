import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBrush, faEdit, faLayerGroup, faPaintBrush, faPencilAlt, faPlug, faPlus, faPlusCircle, faUpload } from "@fortawesome/free-solid-svg-icons";
import React, { createRef } from "react";
import Modal from 'react-responsive-modal'
import { withRouter } from "react-router-dom";
import logo from '../../public/images/layers.png'

let bg = {
    overlay: {
      //lo que queda feura del modal
    },
    modal:{
        background: "white",
        width:'90%',
        textAlign: "center",
        fontFamily: "Helvetica, Helvetica Neue, Arial"
    },
    closeButton:{
        width:'25px',
        top:'2px',
        rigth:'0px'
    }
  };

let lang = null;
class ModalImportar extends React.Component {
    
    constructor(props){
        super(props);
        lang=this.props.lang;
    }

    
    updateParent(){
        this.props.toggleShowModalCallback()
        this.setState({texto: '', type: ''});
    }

    

    render(){      
                    
            return (             
                
                <Modal open={this.props.abierto} onClose={this.updateParent.bind(this)} center styles={bg}>
                    <div className="modalperfil_title">{lang.gpxmodal}<hr></hr></div>
                    <div className="container-fluid margintop">
                                
                                <div className="well form-horizontal"  id="contact_form">
                                    <div className="form-group modalperfil_title3"><b><FontAwesomeIcon size="lg" icon={faUpload}/></b>{lang.gpxmodal_route}</div>                                    
                                </div>

                                <div className="well form-horizontal"  id="contact_form">
                                    <div className="form-group modalperfil_title3"><b><FontAwesomeIcon size="lg" icon={faPaintBrush}/></b>{lang.gpxmodal_layer}<FontAwesomeIcon icon={faLayerGroup}/></div>                                    
                                </div>

                                <div className="form-group row modalperfil_botones">
                                    <div className="editor-footer col-lg-3 col offset-lg-3 offset-1">
                                        <button onClick={this.props.CargarRuta} className="btn btn-secondary btn-block text-center">
                                            <FontAwesomeIcon size="md" icon={faUpload}/>
                                            <span>{lang.cargar}</span>
                                        </button>
                                    </div>
                                    <div className="editor-footer col-lg-3 col offset-1 text-center">
                                        <button onClick={this.props.PintarRuta} className="btn btn-secondary btn-block">
                                            <FontAwesomeIcon size="md" icon={faPaintBrush}/>
                                            <span>{lang.pintar}</span>
                                        </button>
                                    </div>
                                </div>
                    </div>                         
                </Modal>
            );

     }
}
 
export default ModalImportar;