import React, { Component } from "react";
import { useMap } from "react-leaflet";
import L from "leaflet";
import {consoleLOG} from '../../utils/ifelse'


  let counter =0;
  let marker = null;
  let mapa = null;
  let helpDiv = null;
  var lang = null;
class DownloadViewControl extends React.Component {
  helpDiv;
  

  createButtonControl() {
    const MapHelp = L.Control.extend({
      onAdd: (map) => {
        mapa = map;
        helpDiv = L.DomUtil.create("button", "click");
        this.helpDiv = helpDiv;
        helpDiv.innerHTML = '<img width="20px" src="/images/download.svg"/>';
        helpDiv.style.backgroundColor = "#24B8AA";
        helpDiv.addEventListener("click", (ev) => {
            const prop = this.props;  
            ev.stopPropagation();       
                      
            prop.handeClickDownload();

        });

        //a bit clueless how to add a click event listener to this button and then
        // open a popup div on the map
        return helpDiv;
      }
    });
    return new MapHelp({ position: "topright" });
  }

   componentDidMount() {
    lang=this.props.lang;
    const { map } = this.props;
    const control = this.createButtonControl();
    control.addTo(map);
  }

  componentWillUnmount() {
    this.helpDiv.remove();
  }

  render() {
    return null;
  }
}

function withMap(Component) {
  return function WrappedComponent(props) {
    const map = useMap();
    return <Component {...props} map={map} />;
  };
}

export default withMap(DownloadViewControl);
