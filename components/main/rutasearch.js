import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faStar,  faUser, faPaintBrush, faArrowUp, faArrowsAltH, faEraser} from '@fortawesome/free-solid-svg-icons'
import {baseurl, capitalize,GetIconRouteType, GetIconRouteBicicleta, GetIconRouteActivity,colorStars} from "../../utils/constantes";
import {URLamigable} from '../../utils/ifelse'
import Link from 'next/link'


let lang = null;
class RutaSearch extends React.Component{

    constructor(props){
        super(props);
        lang=this.props.lang;
        this.state ={
            pintada:false
        }
    }
    
    

    clickPintarRuta(e){
        e.stopPropagation();
        e.preventDefault()
        this.setState({ 
                pintada: !this.state.pintada
        });
        this.props.PintarRuta();
    }

    

    render(){
        const ruta = this.props.ruta;
        const title= ruta.titulo;
        const rating= ruta.rating;
        const userID = ruta.userID;

        var distancia = ruta.distancia;        
        distancia = (distancia /1000).toFixed(1);

        const ascenso = ruta.desnivel;

        var images = ruta.images;
        var foto = <div className="imagen3"></div>
        if(images.length > 0){
            foto = <img className="imagen3" src={baseurl+"index.php?src="+images[0].image} />
        }

        
        var tipo_html = GetIconRouteType(ruta.tipo)
        var tipo_icono=tipo_html[0];
        var tipo_texto=lang[tipo_html[1]];

        var bicicleta_html = GetIconRouteBicicleta(ruta.bicicleta)
        var bicicleta_icono = bicicleta_html[0];
        var bicicleta_texto = lang[bicicleta_html[1]];

        var actividad_html = GetIconRouteActivity(ruta.actividad)
        var actividad_icono = actividad_html[0];
        var actividad_texto = lang[actividad_html[1]];

        var usernameRuta = ruta.username;
        
        var urlRuta = URLamigable(ruta.titulo+"_"+ruta.rutaID)
        return(
            <Link href={"/route/"+urlRuta}>
                <a target="_blank">
                    <div className="rutaSearchContent">      
                            
                    <div className='row'>
                            <div className='col-lg-9 col-8 d-flex align-items-start flex-column'>
                                
                                
                                
                                <div className='row RutaSearchTitle nopadding'>
                                    {capitalize(title)}
                                </div>
                                <div className="stars nopadding">
                                                <span><FontAwesomeIcon color={rating > 0 ? colorStars : '#bbb'} icon={faStar}/></span>
                                                <span><FontAwesomeIcon color={rating > 1 ? colorStars : '#bbb'} icon={faStar}/></span>
                                                <span><FontAwesomeIcon color={rating > 2 ? colorStars : '#bbb'} icon={faStar}/></span>
                                                <span><FontAwesomeIcon color={rating > 3 ? colorStars : '#bbb'} icon={faStar}/></span>
                                                <span><FontAwesomeIcon color={rating > 4 ? colorStars : '#bbb'} icon={faStar}/></span>
                                </div>
                                <div className='row w-100'>
                                            <div className="editor-footer pintar_mapa col-lg-6 col-12">
                                                {this.state.pintada &&
                                                    <button onClick={(e)=>this.clickPintarRuta(e)} className={'btn btn-outline-danger active'}><FontAwesomeIcon size="lg" icon={faEraser}/><span>{lang.nopintar_mapa}</span></button>
                                                }
                                                {!this.state.pintada &&
                                                    <button onClick={(e)=>this.clickPintarRuta(e)} className={'btn btn-outline-danger'}><FontAwesomeIcon size="lg" icon={faPaintBrush}/><span>{lang.pintar_mapa}</span></button>
                                                }
                                            </div>
                                            <div className='col-lg-6 col-12'>
                                                {tipo_icono &&
                                                    <div className="tag">{tipo_texto}</div>
                                                }
                                                {bicicleta_icono &&
                                                    <div className="tag">{bicicleta_texto}</div>
                                                }
                                                {actividad_icono &&
                                                    <div className="tag">{actividad_texto}</div>
                                                }
                                            </div>
                                                
                                </div>     

                                <div className='mt-auto container'>
                                    <div className='row'>
                                        <div className="tag2 col-6" >
                                            <div>                                            
                                                <span className="dato-valor">{distancia}</span>
                                                <span className="dato-unit">Km</span>
                                                <span><FontAwesomeIcon icon={faArrowsAltH} aria-hidden="true" size="md" className="dato-icon"/></span>
                                            </div>                                    
                                        </div>

                                        <div className="tag2 col-6" >
                                            <div>
                                                <span className="dato-valor">{ascenso}</span>
                                                <span className="dato-unit">m</span>
                                                <span><FontAwesomeIcon icon={faArrowUp} aria-hidden="true" size="md" className="dato-icon"/></span>
                                            </div>                                    
                                        </div>     

                                                            
                                    </div>
                                </div>  
                            </div>      

                            

                            <div className='col-lg-3 col-4'>
                                {foto}
                                <Link href={"/profile/"+userID}>
                                    <a target="_BLANK" ><FontAwesomeIcon icon={faUser} aria-hidden="true" size="lg" className="dato-icon d-inline-block" style={{marginRight:"4px"}}/><span className="d-inline-block" >{usernameRuta}</span></a>
                                </Link>
                            </div>   
                        </div>

                        
                    </div>
                </a>
            </Link>
            

                
        );
        
    }
}
export default RutaSearch;