import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faChartArea,  faGaugeHigh,  faPlay,  faRedo, faRoad, faTrash, faUndo, faUpload} from '@fortawesome/free-solid-svg-icons'
import 'bootstrap/dist/css/bootstrap.min.css';
import Dropdown from 'react-bootstrap/Dropdown'
import {list_directions_prov, list_elevations_prov} from "../../utils/constantes";
import {consoleLOG} from '../../utils/ifelse'
import ReactFileReader from 'react-file-reader';
import Image from 'next/image'
import {isMobile
} from "react-device-detect";

var lang = null;


class Herramientas extends Component{
    constructor(props){
        super(props);
        lang=this.props.lang;

        this.state ={
            travelMode:1//se define el travel mode a 1 que es coche-> normal maps. Directo es = 0,
        }
    } 
    

    handleFiles = files => {   
        var este = this;
        var reader = new FileReader();
        reader.onload = e => {
        // Use reader.result
            var content = reader.result;
            consoleLOG(content)
            este.props.importar(content);
        };
        reader.readAsText(files[0]);
    }

    
    render(){
        var mode1,mode2,mode3,mode0="";
        if(this.props.estado.travelMode == "DRIVING"){
            mode1 =" active";
        }
        else if(this.props.estado.travelMode == "WALKING"){
            mode2 =" active";
        }
        else if(this.props.estado.travelMode == "BICYCLING"){
            mode3 =" active";
        }
        else if(this.props.estado.travelMode == "DIRECT"){
            mode0 =" active";
        }

        var este = this;

        var direactionsactive = this.props.estado.directions_prov;
        var menudirections = [];
        Object.keys(list_directions_prov).map(function(key) {
            var prov = list_directions_prov[key];
            if(direactionsactive == key)
                menudirections.push(<Dropdown.Item onClick ={()=>este.props.HandledirectionsProv(key)} active>{prov}</Dropdown.Item>);
            else
                menudirections.push(<Dropdown.Item onClick ={()=>este.props.HandledirectionsProv(key)}>{prov}</Dropdown.Item>);
        });

        var elevationssactive = this.props.estado.elevations_prov;
        var menuelevations = [];
        Object.keys(list_elevations_prov).map(function(key) {
            var prov = list_elevations_prov[key];
            if(elevationssactive == key)
                menuelevations.push(<Dropdown.Item onClick ={()=>este.props.HandleelevationProv(key)} active>{prov}</Dropdown.Item>);
            else
                menuelevations.push(<Dropdown.Item onClick ={()=>este.props.HandleelevationProv(key)}>{prov}</Dropdown.Item>);
        });


        
        var menuvelocidad = [];
        if(this.props.estado.speedMode == "17")
            menuvelocidad.push(<Dropdown.Item onClick ={()=>este.props.HandleSpeed("17")} active>15-20 Km/h</Dropdown.Item>);
        else
            menuvelocidad.push(<Dropdown.Item onClick ={()=>este.props.HandleSpeed("17")}>15-20 Km/h</Dropdown.Item>);

        if(this.props.estado.speedMode == "22")
            menuvelocidad.push(<Dropdown.Item onClick ={()=>este.props.HandleSpeed("22")} active>20-25 Km/h</Dropdown.Item>);
        else
            menuvelocidad.push(<Dropdown.Item onClick ={()=>este.props.HandleSpeed("22")}>20-25 Km/h</Dropdown.Item>);

        if(this.props.estado.speedMode == "27")
            menuvelocidad.push(<Dropdown.Item onClick ={()=>este.props.HandleSpeed("27")} active>25-30 Km/h</Dropdown.Item>);
        else
            menuvelocidad.push(<Dropdown.Item onClick ={()=>este.props.HandleSpeed("27")}>25-30 Km/h</Dropdown.Item>);

        return (            
            
            <div id="menuTools" >
                <div className="vertical-menu">
                    <div className="btn-grupo margin0" role="group" aria-label="..." >
                                <Dropdown className="Dropdown">
                                    <Dropdown.Toggle variant="light" id="dropdown-basic"> 
                                        <FontAwesomeIcon icon={faRoad} aria-hidden="true" size="lg"/>
                                    </Dropdown.Toggle>

                                    <Dropdown.Menu>
                                        <Dropdown.Header>{lang.roads_prov}</Dropdown.Header>
                                        {menudirections}
                                    </Dropdown.Menu>
                                </Dropdown>
                    </div>
                    <div className="btn-grupo margin0" role="group" aria-label="..." >
                                <Dropdown className="Dropdown">
                                    <Dropdown.Toggle variant="light" id="dropdown-basic">
                                        <FontAwesomeIcon icon={faChartArea} aria-hidden="true" size="lg"/>
                                    </Dropdown.Toggle>

                                    <Dropdown.Menu>
                                        <Dropdown.Header>{lang.elevation_prov}</Dropdown.Header>
                                        {menuelevations}                                        
                                    </Dropdown.Menu>
                                </Dropdown>                        
                    </div>
                    {!isMobile &&
                        <div className="btn-grupo margin0" role="group" aria-label="..." >
                                    <Dropdown className="Dropdown">
                                        <Dropdown.Toggle variant="light" id="dropdown-basic">
                                            <FontAwesomeIcon icon={faGaugeHigh} aria-hidden="true" size="lg"/>
                                        </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Header>{lang.velocidad_llano}</Dropdown.Header>
                                            {menuvelocidad}                                        
                                        </Dropdown.Menu>
                                    </Dropdown>                        
                        </div>
                    }

                    <div className="btn-grupo margin0" role="group" aria-label="..." >
                        <button  onClick ={()=>this.props.handeTravelMode("WALKING")} title={lang.mod_walking} name="mode" type="button" className={"btn-light btn-sm mode"+mode2}>
                            <Image src="/images/hiking.png" width="100%" height="100%"/>
                        </button>
                        <button onClick ={()=>this.props.handeTravelMode("BICYCLING")} title={lang.mod_bike} name="mode" type="button" className={"btn-light btn-sm mode"+mode3}>
                            <Image src="/images/mtb.png" width="100%" height="100%"/>
                        </button>
                        <button onClick ={()=>this.props.handeTravelMode("DRIVING")} title={lang.mod_drive} type="button" name="mode" className={"btn-light btn-sm mode"+mode1}>
                            <Image src="/images/road.png" width="100%" height="100%"/>
                        </button>
                        <button onClick ={()=>this.props.handeTravelMode("DIRECT")} title={lang.mod_eki} name="mode" type="button" className={"btn-light btn-sm mode"+mode0}>
                            <Image src="/images/direct.png" width="100%" height="100%"/>
                        </button>    
                    </div>

                    <div className="btn-grupo margin0" role="group" aria-label="..." >
                            <button onClick = {this.props.deshacer} title={lang.undo} type="button" className="btn-light btn-sm">
                                <FontAwesomeIcon icon={faUndo} aria-hidden="true" size="lg"/>
                            </button> 
                        
                            <button onClick = {this.props.rehacer} title={lang.redo} type="button" className="btn-light btn-sm">
                                <FontAwesomeIcon icon={faRedo} aria-hidden="true" size="lg"/>
                            </button> 
                            
                            <button onClick ={this.props.reset} title={lang.borrar_todo} type="button" className="btn-light btn-sm">
                                <FontAwesomeIcon icon={faTrash} aria-hidden="true" size="lg"/>
                            </button> 
                    </div>

                    <div className="btn-grupo margin0" role="group" aria-label="...">
                        <button onClick ={this.props.play} title="calcular_ruta" type="button" className="btn-light btn-sm" >
                            <FontAwesomeIcon icon={faPlay} aria-hidden="true" size="lg"/>
                        </button> 
                    </div>

                    <div className="btn-grupo margin0" role="group" aria-label="...">
                        <ReactFileReader handleFiles={this.handleFiles} fileTypes={['.gpx','.kml']}>
                            <button type="button" className="btn-light btn-sm" >
                                <FontAwesomeIcon icon={faUpload} aria-hidden="true" size="lg"/>
                            </button>
                        </ReactFileReader>
                        
                    </div>
                </div>
            </div>
        );
    }
}
export default Herramientas;