import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {colorGris} from "../../utils/constantes";

class Dato extends Component{
    constructor(props){
        super(props);
    } 

    
    render(){
        var over = this.props.resalta;
        return (            
            <div className={"col-3 dato-box nopadding " + this.props.border} >
            	<div>
                    
                    <span className="dato-text2" style={over ? { fontWeight: "bold" } : {}}>{this.props.texto}</span>
                </div>
                
                <div>
                    <span className="dato-valor">{this.props.valor}</span>
                    <span className="dato-unit">{this.props.unidad}</span>
                </div>
                
			</div>
        );
        /*
        return (            
            <div className={"col-3 dato-box nopadding " + this.props.border}>
            	<div>
                    <span className="dato-icon"><FontAwesomeIcon icon={this.props.icono} aria-hidden="true" color={colorGris} size="xs" className="dato-icon" style={over ? { color: "#dc3545" } : {}}/></span>
                </div>
                <div>
                    <span className="dato-valor">{this.props.valor}</span>
                    <span className="dato-unit">{this.props.unidad}</span>
                </div>
                <div>
                    
                    <span className="dato-text">{this.props.texto}</span>
                </div>
			</div>
        );*/
    }
}
export default Dato;