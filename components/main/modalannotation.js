////////////////////////////////////////////////////////////////////////////////////// DEFINE //////////////////////////////////////////////////
/*react / components*/
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faImage, faPencilAlt, faTextHeight } from "@fortawesome/free-solid-svg-icons";
import React, { createRef } from "react";
import Modal from 'react-responsive-modal'
import {capitalize,baseicon ,GetIconTag, GetListTag} from '../../utils/constantes'
import {consoleLOG} from '../../utils/ifelse'


////////////////////////////////////////////////////////////////////////////////////// FIN DEFINE //////////////////////////////////////////////////
let bg = {
    overlay: {
      //lo que queda feura del modal
    },
    modal:{
        background: "white",
        width:'100%',
        textAlign: "center",
        fontFamily: "Helvetica, Helvetica Neue, Arial"
    },
    closeButton:{
        width:'25px',
        top:'2px',
        rigth:'0px'
    }
  };

let lang = null;
class ModalAnnotation extends React.Component {
    
    constructor(props){
        super(props);
        lang=this.props.lang;
        this.textInput = createRef();
        //this.focusTextInput = this.focusTextInput.bind(this);
        this.state = {
            texto : props.texto,
            type: props.type,
            row:props.row,
            icon:null
        };

        consoleLOG("COSNTRUCTOR MODAL"+this.state);

        this.innerRef = React.createRef();
    }

    focusTextInput() {
        // Explicitly focus the text input using the raw DOM API
        // Note: we're accessing "current" to get the DOM node
        this.textInput.current.focus();  
    }

    handleChange(value){
        this.setState({
                texto: value
        });
    }

    handleChangetype(v){

        //solo dejamos modificar el tipo si es una anotación nueva
        if(this.props.texto == ''){
            consoleLOG(v);
                this.setState({
                    type: v
            });
        }        
    }

    shouldComponentUpdate(nextProps, nextState) {
        /*consoleLOG("¿DEBE ACTUALIZARSE?");
        consoleLOG("props")
        consoleLOG(this.props);
        consoleLOG(nextProps);
        consoleLOG("state")
        consoleLOG(this.state);
        consoleLOG(nextState);

        consoleLOG(nextProps.row +"!=="+ this.props.row +"||"+ this.state.texto +"!== "+nextState.texto)*/
        //consoleLOG((this.state.texto !== nextState.texto));
        //consoleLOG(this.state.texto +"!== "+nextState.texto)
        return (nextProps.row != this.props.row || this.state.texto != nextState.texto || this.state.type != nextState.type || this.props.abierto != nextProps.abierto)
    }

    componentDidUpdate(){
        //consoleLOG(this.state)
        //consoleLOG("SE ACTUALIZA modal anottation");
        // Add a timeout here
        if(this.innerRef.current){
            setTimeout(() => {
                this.innerRef.current.focus();
              }, 1)
        }
        
    }

    componentDidMount() {
        
      }
    

    updateParent(){
        this.props.toggleShowModalCallback()
        this.setState({texto: '', type: ''});
    }

    //esto hace POR FIN que se abra con el texto que debe abrirse
    UNSAFE_componentWillReceiveProps(props) {
        this.setState({ texto: props.texto, type:props.type, row:props.row })
    }

    crear(){
        this.props.CrearAnnotation(this.props.row, this.state.texto, this.state.type)
    }

    selectIcon(icon){
        var url = baseicon+icon;
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {texto: url, icon: icon}
        });
    }

    render(){      
        
        var arrayicos = GetListTag();
        var arraydivicos = [];

        for (let index = 0; index < arrayicos.length; index++) {
            const element = arrayicos[index];
            var classactive = "";
            if(element == this.state.icon){
                classactive = "activeicon";
            }
            arraydivicos.push(
                            <div className="mt-3 col-2" >
                                <img className={classactive+' iconsAnnotations'} src={GetIconTag(element).src} onClick={()=>this.selectIcon(element)}/>
                              </div> 
                            );
        }
        var htmlarryicos = <div className="row mt-1">{arraydivicos}</div>;

        var place = lang.write_some;
        if(this.state.type == "imagen"){            
            place = lang.url_imagen;
        }
        var input2 = React.createRef();
        
        consoleLOG("SE RENDER");
       // consoleLOG("El texto va a ser "+ this.state.texto);
        
        if(this.props.row >= 0){                
            consoleLOG(this.props.row)
            return (             
                
                <Modal  onEntered={() => input2.current.focus()} open={this.props.abierto} onClose={this.updateParent.bind(this)} center styles={bg}>
                    {this.state.type == "" &&
                        <div className="container modalAnnotation">
                         
                            <div className="card-body">
                                 <div className="row">
                                        <div className="col-12" style={{marginTop:"10px"}}>
                                            {lang.add_annotation}
                                            <FontAwesomeIcon icon={faPencilAlt} aria-hidden="true" size="md"/><br></br><br></br>
                                            {lang.add_annotation_2}<br></br>
                                        </div>   
                                    </div>
                                <div className="row mt-3">
                                    <div className="col">
                                        <figure onClick={(e)=>this.handleChangetype("texto")} className="figure"><FontAwesomeIcon icon={faTextHeight}aria-hidden="true" size="2x"/>
                                            <figcaption className="figure-caption text-center">{capitalize(lang.text)}</figcaption>
                                        </figure>
                                    </div>
                                    <div className="col">
                                        <figure onClick={(e)=>this.handleChangetype("imagen")} className="figure"><FontAwesomeIcon icon={faImage}aria-hidden="true" size="2x"/>
                                            <figcaption className="figure-caption text-center">{capitalize(lang.image)}</figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>


                    }

                    {this.state.type == "texto" &&

                        <div className="container modalAnnotation">                       
                            <div className="row">
                                <div className="col-12">   
                                    <div className="row mt-1">    
                                        <div className="col-12">
                                            {lang.add_annotation_3} <b>{(this.state.row/10)}</b>
                                        </div>   
                                    </div>                                 
                                    <div className="row mt-5">    
                                        <div className="col-12">
                                            <textarea ref={this.innerRef} placeholder={place} onChange={e => this.handleChange(e.target.value)}  type="text" className="textareamodal form-control">{this.state.texto}</textarea>
                                        </div>   
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-12">
                                            <button onClick ={()=>this.crear()} className="btn btn-secondary" >{lang.guardar}</button>
                                            <button onClick ={()=>this.props.BorrarAnnotation(this.props.row)} className="btn btn-secondary"  style={{marginLeft:"5px"}}>{lang.eliminar}</button>
                                        </div>   
                                    </div>
                                </div>

                            
                            </div>
                            
                        </div>     
                    }                      

                    {this.state.type == "imagen" &&

                        <div className="container modalAnnotation">                       
                            <div className="row">
                                <div className="col-12">   
                                    <div className="row mt-1">    
                                        <div className="col-12">
                                            {lang.add_annotation_4} <b>{(this.state.row/10)}</b>
                                        </div>   
                                    </div>     
                                    <div className="row mt-1">    
                                        <div className="col-12 text-left">
                                            1. {lang.select_icon}
                                        </div>   
                                    </div>    
                                    
                                    {htmlarryicos}
                                    
                                    <div className="row mt-5">    
                                        <div className="col-12 text-left">
                                            2. {place}
                                        </div>   
                                    </div>                  
                                    <div className="row mt-1">    
                                        <div className="col-12">
                                            <input ref={this.innerRef} placeholder="url" onChange={e => this.handleChange(e.target.value)}  type="text" className="textareamodal form-control" value={this.state.texto}/>
                                        </div>   
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-12">
                                            <button onClick ={()=>this.crear()} className="btn btn-secondary" >{lang.guardar}</button>
                                            <button onClick ={()=>this.props.BorrarAnnotation(this.props.row)} className="btn btn-secondary"  style={{marginLeft:"5px"}}>{lang.eliminar}</button>
                                        </div>   
                                    </div>
                                </div>

                            
                            </div>
                            
                        </div>     
                    }                             
                </Modal>
            );

        }else{
            return null;
        }
     }
}
 
export default ModalAnnotation;