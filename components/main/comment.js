import React from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { faStar } from '@fortawesome/free-solid-svg-icons'



class Comment extends React.Component{

    constructor(props){
        super(props);
    }


    render(){
        
        const comment = this.props.comment;
        var username = comment.username;
        var colorhex = this.props.color;
        var title = comment.username;
        var rating = comment.rating;

        var mesaje = comment.comment;
        const texto = mesaje.split('\n').map(str => <div key={"key-"+str} className="p_comment col-12">{str}</div>);
        
        return(
            
                <div className="comment col-12">
                    <div className="commentContent">
                    <div className="row">
                        <div><span className="userComment">{comment.username}</span><span className="fechaComment">{comment.created}</span></div>
                    </div>
                    <div className="row">
                        <div className="starsComment">
                                    <span><FontAwesomeIcon color={rating > 0 ? colorhex : '#bbb'} icon={faStar}/></span>
                                    <span><FontAwesomeIcon color={rating > 1 ? colorhex : '#bbb'} icon={faStar}/></span>
                                    <span><FontAwesomeIcon color={rating > 2 ? colorhex : '#bbb'} icon={faStar}/></span>
                                    <span><FontAwesomeIcon color={rating > 3 ? colorhex : '#bbb'} icon={faStar}/></span>
                                    <span><FontAwesomeIcon color={rating > 4 ? colorhex : '#bbb'} icon={faStar}/></span>
                        </div>
                    </div>
                    <div className="row">
                        {texto}
                    </div>
                    </div>
                                     
             </div>
        );
    }
}
export default Comment;