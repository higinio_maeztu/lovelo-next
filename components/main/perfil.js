import React, { Component } from 'react';
import Chart from "react-google-charts";
import {distancia3dEarth, GetSurfaceChartSVG} from '../../utils/matematicas.js'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowsAltH, faArrowsAltV, faPercent, faPrint, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import { renderToString } from 'react-dom/server'
import domtoimage from 'dom-to-image';
import { consoleLOG } from '../../utils/ifelse.js';
import { saveAs } from 'file-saver';
//import * as svg from 'save-svg-as-png';
//import toImg from 'react-svg-to-image';
//import Canvg from 'canvg';

var lang = null;
var elevations = null;
var globalchart = null;
var valorleft = 80;
var distanciaRuta=null;
var rutaname = "";

let EventosLanzados = 0;
class Perfil extends Component{
    constructor(props){
        super(props);
        lang=this.props.lang;
        rutaname = this.props.titulo;
        //Esto es necesario porque si no lo hacemos no funciona el mouseover sobre el perfil para ver por dónde seva
        EventosLanzados=0;
        this.state = {
            chartWrapper:null
          }
    } 

    downloadPNG(serializedSVG)
    {
        
        if (typeof window !== "undefined") {
            var SVGDomElement =  document.getElementById('chart_div2').getElementsByTagName('svg')[1];

            // 2. Serialize element into plain SVG
            var serializedSVG = new XMLSerializer().serializeToString(SVGDomElement);
            var base64Data = window.btoa(serializedSVG);
            consoleLOG(base64Data);
            var link=document.createElement('a');
            link.href = "data:image/svg;base64," + base64Data;
            link.download = "perfil.png";
            link.click();
            link.remove();
        }
    }

    svgToPng = (svgDataurl) => new Promise((resolve, reject) => {
        let canvas;
        let ctx;
        let img;
        consoleLOG(svgDataurl);
        img = new Image();
        img.src= svgDataurl;
        img.onload = () => {
            canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            var width = img.width;
            var height = img.height;
            ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, width, height);
    
            img = new Image();
            img.src = canvas.toDataURL('image/png');
            img.onload = () => {
                canvas = document.createElement('canvas');
                canvas.width = width;
                canvas.height = height;
                ctx = canvas.getContext('2d');
                ctx.drawImage(img, 0, 0);
                resolve(canvas.toDataURL('image/png'));
            }
        };
    });

    imprimirPerfil(tipo, conSuperficies){
        var este = this;
                
        if(tipo=="google"){
            var data = this.state.chartWrapper.getChart().getImageURI();
            var link=document.createElement('a');
            link.href = data;
            link.download = "perfil.jpg";
            link.click();
            link.remove();
        }
        else{
            if (typeof window !== "undefined") {
                this.imagen();
                var indice = 3;
                if(this.props.page == "ruta"){
                    indice = 1;
                }
                if(this.props.page == "iframe"){
                    indice = 2;
                }

                if(this.props.surfacePrintPerfil){
                    indice = indice +1;
                }
                //si añadimos o quitamos botones con fontawesome de aquí, el índice 2 cambiará, para ver nuevo indice descomentar siguiente línea
                //consoleLOG(document.getElementById('chart_div2').getElementsByTagName('svg'))
                var node = document.getElementById('chart_div2').getElementsByTagName('svg')[indice];

                var nodecopy = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                var width =node.getAttribute("width");
                
                var height =parseInt(node.getAttribute("height"));            
                nodecopy.innerHTML=node.innerHTML;   
                nodecopy.setAttribute("width", width);
                nodecopy.setAttribute("height", height);  
                nodecopy.setAttribute("viewBox","0 0 "+ width + " "+(height-50));
                //nodecopy.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
                var svgkm = this.AddKilometeres(width);
                //Esto lo posiciona abajo del todo, se le pasa la altura del gráfico
                svgkm.setAttributeNS(null,"y",height);  


                var mergesvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                //mergesvg.setAttribute('xlink','http://www.w3.org/1999/xlink');
                mergesvg.setAttribute('width',width);

                if(this.props.surfacePrintPerfil && conSuperficies){
                    var svgSurface = GetSurfaceChartSVG(this.props.polylines, width, valorleft, this.props.datos.distancia);
                    if(svgSurface){
                        svgSurface.setAttributeNS(null,"y",height+20);  
                        
                        mergesvg.setAttribute('height',height+15+15);
                        mergesvg.appendChild(nodecopy); 
                        mergesvg.appendChild(svgkm); 
                        mergesvg.appendChild(svgSurface);
                    }
                }
                else{
                    mergesvg.setAttribute('height',height+15);
                    mergesvg.appendChild(nodecopy); 
                    mergesvg.appendChild(svgkm); 
                }


           
            

            
            
            
                domtoimage.toSvg(mergesvg)
                .then(function (dataUrl) {
                    dataUrl = dataUrl.replace(/fill-opacity="0.3"/g,'fill-opacity="0.7"');   
                    dataUrl = dataUrl.replaceAll('xmlns="http://www.w3.org/1999/xhtml"', ""); 
                    saveAs(dataUrl, 'perfil.svg');
                }).catch(function (error) {
                    console.error('oops, something went wrong!', error);
                });
            }
            
        }
    }



    shouldComponentUpdate(nextProps, nextState) {
        consoleLOG("¿perfil se repinta?");
        consoleLOG(this.props.repintar);
        consoleLOG(nextProps.repintar);

        if(nextProps.repintar)
            return true;
        else
            return false;
    }
    
    IsAnnotationRow(row){
        var annotations = this.props.datos.annotations;
        for (var k=0; k <annotations.length; k++){
            if(annotations[k].row == row)
                return true;
        }
        return false;
    }

    AddKilometeres(width){
        var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        //svg.setAttribute('xlink','http://www.w3.org/1999/xlink');
        svg.setAttribute('width',width);
        svg.setAttribute('height','15');
      
        var KmsRutaSinUltimoTramo = elevations.length/10 - 2;
        var pxcada = width/KmsRutaSinUltimoTramo;
        var i=1;
        var container = document.getElementById('chart_div2');
        var este = this;
        var pxcada = 0;
        var posx1 = 0;

        var factorcorrector = 1;
        if(KmsRutaSinUltimoTramo > 500){
            factorcorrector = 30;
        }
        if(KmsRutaSinUltimoTramo > 300){
            //se van a pegar mucho los números.
            factorcorrector = 10;
        }
        else if(KmsRutaSinUltimoTramo > 200){
            factorcorrector = 5;
        }
        else if(KmsRutaSinUltimoTramo > 100){
            factorcorrector = 2;
        }

        Array.prototype.forEach.call(container.getElementsByTagName('rect'), function(rect) {            
            //para rotar los textos
            if ((rect.getAttribute('fill') === 'none') && (rect.getAttribute('width') === '1') && (rect.getAttribute('height') === '2') && (rect.getAttribute('fill-opacity') == "1")) {
                if(i > elevations.length/10)
                    return svg;

                if(i==2){
                    pxcada = rect.getAttribute('x') - posx1;//esto es la diferencia entre dos líneas que marca cada cuántos px hay 1km.
                    
                    var pos0 = posx1+parseFloat(pxcada/2 -5) - pxcada;

                    var text = este.createText("KM", pos0 - 20);
                    svg.appendChild(text); 

                    var text = este.createText("0", pos0);
                    svg.appendChild(text); 
                    
                    if(1 % factorcorrector == 0){
                        var text = este.createText("1", posx1+parseFloat(pxcada/2 -5));
                        svg.appendChild(text);
                    }
                }          
                
                if(i==1){
                    posx1 = parseFloat(rect.getAttribute('x'));
                }
                else{
                    if(i % factorcorrector == 0){
                        var text = este.createText(i.toString(), parseFloat(rect.getAttribute('x'))+parseFloat(pxcada/2 -5));
                        svg.appendChild(text); 
                    }
                }
                
                
                i++;
                
            }
        });

        

        return svg;

    }

    createText(text, posx) {
        var newText = document.createElementNS('http://www.w3.org/2000/svg', 'text');
        newText.setAttributeNS(null,"x",posx);      
        newText.setAttributeNS(null,"y","15");   
        newText.setAttributeNS(null,"font-size","9px");
        newText.setAttributeNS(null,"fill","#aaa");
        var textNode = document.createTextNode(text);
        newText.appendChild(textNode);
        return newText;
      }

    imagen(){
        //ICONOS : https://www.flaticon.es/resultados/6?word=bicicleta
        //ATENCIÓN, así se consigue pintar iconos en el perfil
        var este = this;
        var container = document.getElementById('chart_div2');
        var arrayimagnes = [];
        var arrayurls = [];
        var annotations = this.props.datos.annotations;
        for (var k=0; k <annotations.length; k++){
        	if(annotations[k].type == 'imagen'){
                arrayimagnes.push(annotations[k].row+"id");
                arrayurls.push(annotations[k].text);
            }
        }


        Array.prototype.forEach.call(container.getElementsByTagName('text'), function(text) {
            
            //para rotar los textos
            if ((text.getAttribute('fill') === '#000001')) {
                //text.setAttribute("class", "textrotate");
            }
            
            
            //para añadir las imágenes
            if(arrayimagnes.indexOf(text.innerHTML) > -1){
                var k = arrayimagnes.indexOf(text.innerHTML);
                consoleLOG(text.innerHTML + k + "encontrado con text "+ arrayurls[k]);
                var xPos = parseFloat(text.getAttribute('x'));
                var yPos = parseFloat(text.getAttribute('y'));
                var svgimg = document.createElementNS('http://www.w3.org/2000/svg','image');
                svgimg.setAttributeNS(null,'height',este.props.optionsPerfil.tags_size_icon+'px');
                svgimg.setAttributeNS(null,'width',este.props.optionsPerfil.tags_size_icon+'px');
                svgimg.setAttributeNS(null, "id", text.innerHTML);
                
                svgimg.setAttributeNS('http://www.w3.org/1999/xlink','href', arrayurls[k]);
                svgimg.setAttributeNS(null,'x',xPos -este.props.optionsPerfil.tags_size_icon/2);
                svgimg.setAttributeNS(null,'y',yPos -este.props.optionsPerfil.tags_size_icon/1.3);
                svgimg.setAttributeNS(null, 'visibility', 'visible');
              
                var div = text.parentNode;
                div.replaceChild(svgimg, text);              
            }
        });
    }


    
    render(){    
        consoleLOG("RENDER PERFIL");
        consoleLOG(this.props)
        var cadadist = 100;
        elevations = this.props.datos.elevations;
        var annotations = this.props.datos.annotations;
        
        const alturamin = this.props.datos.alturamin;
        const alturamax = this.props.datos.alturamax;

        //var altura_min_g = alturamin - (alturamin %100);
        var altura_min_g = alturamin;

        if(this.props.optionsPerfil.altura_max > 0)
            var altura_max_g = this.props.optionsPerfil.altura_max;
        else{
            var margen = (alturamax-alturamin) / 5;
            var altura_max_g = alturamax + margen;
        }

        if(alturamax > altura_max_g){
            var margen = (alturamax-alturamin) / 5;
            var altura_max_g = alturamax + margen;
        }
        

        var porcentaje = 0;

        var stroke = "point {stroke-width: 2;  stroke-color: #24B8AA; fill-color: transparent }"; 
        var data = [];
        
        //elevs y dists deben tener la misma longitud o algo ha ido mal
        //calcular porcentaje del ultimo km no comleto
        var locat1 = elevations[elevations.length - 2].location;
        var locat2 = elevations[elevations.length - 1].location;
        var loca1 = [locat1.lat, locat1.lng];
        var loca2 = [locat2.lat, locat2.lng];
        var distultimotramo = distancia3dEarth(loca1, loca2);
        distanciaRuta = ((elevations.length - 2) * cadadist) + distultimotramo; //1202 por ejemplo
        var x = parseInt(distanciaRuta/1000)*(1000/cadadist);
        var distTramo = distanciaRuta%1000;
        
        var porc_symbol = "";
        if(this.props.optionsPerfil.porc_symbol)
            porc_symbol = "%";
        
        var porcentajesmayores = this.props.optionsPerfil.porcentajesmayores;
        var decimales = this.props.optionsPerfil.decimales;
        var lineaskm = this.props.optionsPerfil.lineaskm;
        
        //dividimos el tramo entre 2 para situarlo en la mitad.
        //x es la lineakm del último km, y tenemos que sumarle la mitad del último tramo
        //var pos = x + parseInt((elevations.length - 1 - x)/2);
        var pos = x + parseInt((elevations.length - x)/2);
        for (var i = 0; i < elevations.length; i++) {
            if(i == elevations.length -1)
                var porcentaje = 0;
            else
                var porcentaje = ((elevations[i+1].elevation - elevations[i].elevation)* 100 / cadadist).toFixed(1);
            
            var HTMLtooltip = <div className='container' style = {{width:"100px",fontSize:"12px", textDecoration:"bold", padding:"2px"}}>
                                <div className='row'>
                                    <div className='col-3'><FontAwesomeIcon icon={faArrowsAltV} aria-hidden="true" size="sm"/></div>
                                    <div className='col-9'>{(elevations[i].elevation).toFixed(0) + " " + lang.meters}</div>
                                </div>
                                <div className='row'>
                                    <div className='col-3'><FontAwesomeIcon icon={faArrowsAltH} aria-hidden="true" size="sm"/></div>
                                    <div className='col-9'>{i/(1000/cadadist)+" Km"}</div>
                                </div>
                                <div className='row'>
                                    <div className='col-3'><FontAwesomeIcon icon={faPercent} aria-hidden="true" size="sm"/></div>
                                    <div className='col-9'>{porcentaje+ " %"}</div>
                                </div>
                            </div>;

            var tooltip = renderToString(HTMLtooltip);



            if(i == pos && distTramo > 1){
                var alt = (elevations[elevations.length -1].elevation - elevations[x].elevation)/(distTramo);
                alt = alt *100;
                var pm = alt.toFixed(decimales);
                
                if(Math.abs(alt) > porcentajesmayores)
                    data.push([null,altura_min_g,pm+porc_symbol,elevations[i].elevation,null, null, tooltip,null,stroke]);
                else
                    data.push([null,altura_min_g,null,elevations[i].elevation,null, null, tooltip,null,stroke]);
            }
            else{
                if(i%(1000/cadadist) != 0 && i%(1000/cadadist/2) == 0 && i != 0 && (i+(1000/cadadist/2))<elevations.length){
                    alt = (elevations[i+(1000/cadadist/2)].elevation - elevations[i-(1000/cadadist/2)].elevation)/10;
                    var pm = alt.toFixed(decimales);

                    if(Math.abs(alt) > porcentajesmayores)
                        data.push([null,altura_min_g,pm+porc_symbol,elevations[i].elevation,null, null, tooltip,null,stroke]);
                    else
                        data.push([null,altura_min_g,null,elevations[i].elevation,null, null, tooltip,null,stroke]);
                }
                
                if(i%(1000/cadadist) == 0){
                    if(lineaskm)
                        data.push([i/(1000/cadadist)+'',altura_min_g,null,elevations[i].elevation,altura_min_g, elevations[i].elevation, tooltip,null,stroke]);
                    else
                    data.push([i/(1000/cadadist)+'',altura_min_g,null,elevations[i].elevation,altura_min_g, 0, tooltip,null,stroke]);
                }
                if(i%(1000/cadadist) != 0 && i%(1000/cadadist/2) != 0)
                    data.push([null,altura_min_g,null,elevations[i].elevation,null,null ,tooltip, null,stroke]);
                //ultimo tramo de km no completo
            }
        }

        for (var k=0; k <annotations.length; k++){
            //consoleLOG(" es de tipo " + annotations[k].type)
            //Si el annotation se añadió pero luego se ha cambiado el perfil y ahora es más corto, que no de error por quedarse fuera del gráfico
            if(annotations[k].row < data.length){
                if(annotations[k].type == "texto"){
                    //si es tipo texto, metemos el texto en sus anotaciones
                    //de momento siempre va a ser tipo texto
                    data[annotations[k].row][7] = annotations[k].text;
                }
                else{
                    //si es tipo imagen creamos el palito sin texto en la segunda series de anotaciones
                    data[annotations[k].row][7] = annotations[k].row+"id";
                    //data[annotations[k].row][8] = annotations[k].text;
                }
            }   
        }
        var este = this;
        var widthC = este.props.optionsPerfil.width+"%";
        if(this.props.page != "main"){
            valorleft=80;
        }

        
        return (      
            
            
            
                <div id="chart_div2" style={{height:'100%',position:'relative'}}>
                    <div className="btn-grupo margin0 botonesPerfil" role="group" aria-label="...">
                        {(this.props.page == "main" || this.props.page == "iframe") &&
                            <button onClick ={()=>this.props.CerrarPerfil()} title={lang.cerrar_perfil} type="button" className="btn-light btn-sm" >
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-lg" viewBox="0 0 16 16">
                                    <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
                                </svg>
                            </button>
                        }{this.props.page == "main" &&
                            <button onClick ={ () => this.props.EditarPerfil()} title={lang.editar_perfil} type="button" className="btn-light btn-sm" >
                                <FontAwesomeIcon icon={faPencilAlt} aria-hidden="true" size="lg"/>
                            </button> 
                        }
                        <button onClick ={ () => this.imprimirPerfil("svg", false)} title={lang.print_save} type="button" className="btn-light btn-sm" >
                            <FontAwesomeIcon icon={faPrint} aria-hidden="true" size="lg"/>
                        </button> 

                        {this.props.surfacePrintPerfil &&
                            <button onClick ={ () => this.imprimirPerfil("svg",true)} title={lang.print_save_surface} type="button" className="btn-light btn-sm fondoColores" >
                            <FontAwesomeIcon icon={faPrint} aria-hidden="true" size="lg"/>
                            </button>
                        }

                        <button onClick ={ () => this.imprimirPerfil("google")} title={lang.print_save} type="button" className="btn-light btn-sm" >
                            png
                        </button> 
                    </div>
                    <Chart
                        getChartWrapper={chartWrapper => {
                            this.setState({ chartWrapper });
                          }}
                        width={widthC}
                        height={'100%'}
                        chartType="AreaChart"
                        loader={<div>Loading Chart</div>}
                        columns={[
                            {
                                type: "string",
                                label: "KM"
                            },
                            {
                                type: "number",
                                label: "porcentaje"
                            },
                            {
                                type: "string",
                                role: 'annotation'
                            },
                            {
                                type: "number",
                                label: "elevación"
                            },
                            {
                                type: "number",
                                role:'interval'
                            },
                            {
                                type: "number",
                                role:'interval'
                            },
                            {
                                type: "string",
                                role:'tooltip', 
                                p: {'html': true}
                            },
                            {
                                type: "string",
                                role: 'annotation',
                                p: {'html': true}
                            },
                            {
                                type: 'string',
                                role: 'style'
                            }
                        ]}
                        rows={data}
                        options={{
                            //enableInteractivity: false,
                            tooltip: { isHtml: true},
                            intervals: { pointSize:0,  fillOpacity: '0.9'},   //style: 'line', 'lineWidth':0.5, 'barWidth': 0.5* .... pointSize:0 aquí quita unos bullet que aparecen cada x kms
                            legend: 'none', 
                            title: 'Lovelo.app'+rutaname,
                            chartArea:{left:valorleft,top:40, bottom:25,right:10,width:widthC},
                            vAxis: {
                                viewWindow: {
                                    max:altura_max_g,
                                    min:altura_min_g
                                },
                                gridlines:{color:este.props.optionsPerfil.color_lineashorizontales, count:este.props.optionsPerfil.lineashorizontales},
                                baselineColor: '#fff'//elimina una línea negra que aparecía abajo.
                            },
                            hAxis : { 
                                textStyle : {
                                    fontSize: 10,
                                    color:'#000'
                                }      
                            },
                            //point: { size: 0, visible: true},
                            //dataOpacity: 1,
                            backgroundColor: {fill: 'transparent'},
                            pointSize: 0,
                            pointsVisible: false,
                            series: {
                                //esta serie corresponde a los porcentajes de cada km fijos
                                0: {
                                    enableInteractivity:false,
                                    color:este.props.optionsPerfil,
                                    background:'transparent',
                                    annotations: {
                                        alwaysOutside:false,
                                        textStyle: {
                                            fontName: este.props.optionsPerfil.fuente,
                                            fontSize: this.props.optionsPerfil.porc_pixels,
                                            color: this.props.optionsPerfil.porc_color,
                                            bold: false
                                        },
                                        stem: {
                                            color: 'transparent',
                                            length: '2'
                                        }
                                    }
                                },
                                //esta serie corresponde a las etiquetas o anotaciones en el perfil
                                1: {
                                    color:este.props.optionsPerfil.color,
                                    annotations: {
                                        alwaysOutside: true,
                                        textStyle: {
                                            fontName: este.props.optionsPerfil.fuente,
                                            fontSize: este.props.optionsPerfil.tags_size,
                                            color: este.props.optionsPerfil.tags_color,
                                            bold: false
                                        },
                                        stem: {
                                            color: este.props.optionsPerfil.tags_stem_color,
                                            length: este.props.optionsPerfil.tags_stem_size
                                        }
                                    }
                                }
                        }
                        }}
                        chartEvents={[
                            {
                                eventName: "select",
                                callback({ chartWrapper }) {
                                    const chart = chartWrapper.getChart();
                                    if(chart.getSelection().length > 0){
                                        consoleLOG(chart.getSelection());
                                        var row = chart.getSelection()[0].row;
                                        consoleLOG("se está lanzando de nuevo el click desde perfil" +row);
                                        este.props.HandleClickChart(row);
                                    }
                                }
                            },
                            {
                                eventName: "ready",
                                callback: ({ chartWrapper, google }) => {
                                    this.imagen();
                                    const chart = chartWrapper.getChart();
                                    globalchart = chart;
                                    consoleLOG("EventosLanzados"+EventosLanzados)
                                    this.imagen();
                                    if(EventosLanzados == 0){
                                        google.visualization.events.addListener(
                                            chart,
                                            "onmouseover",
                                            e => {
                                                //consoleLOG("que pasawss");
                                                const { row, column } = e;
                                                //si hago un mouseout sobre alguno de los annotations que tengo tengo que volver a aplicar el rotado del texto o el pintado de imagen si es imagen
                                                //console.warn("MOUSE OVER ", { row, column });
                                                var locmarker = elevations[row].location;
                                                consoleLOG(locmarker)
                                                this.props.HandleMouseOverChart(locmarker.lat(), locmarker.lng());
                                                EventosLanzados++;
                                                if(this.IsAnnotationRow(row)){
                                                    //consoleLOG("mouse sobre annotation");
                                                    this.imagen();
                                                }
                                                
                                        });
                                        google.visualization.events.addListener(
                                            chart,
                                            "onmouseout",
                                            e => {
                                                
                                                const { row, column } = e;
                                                if(this.IsAnnotationRow(row)){
                                                    consoleLOG("mouse out sobre annotation");
                                                    this.imagen();
                                                }
                                                
                                        });
                                        /*google.visualization.events.addListener(
                                            chart,
                                            "click",
                                            e => {
                                                const { row, column } = e;
                                                console.warn("CLICK ", { e });
                                                
                                        });*/
                                    }
                                }
                            }
                        ]}
                        rootProps={{ 'data-testid': '1' }}
                        />    
                </div>
        );
    }
}
export default Perfil;