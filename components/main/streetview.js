import React, { Component } from "react";
import { useMap } from "react-leaflet";
import L from "leaflet";
import {consoleLOG} from '../../utils/ifelse'

const icon = L.icon({
    iconSize: [30, 30],
    iconAnchor: [15, 15],
    popupAnchor: [0, 15],
    iconUrl: "https://lovelo.app/images/streetview.png"
  });

  let counter =0;
  let marker = null;
  let mapa = null;
  let helpDiv = null;
  var lang = null;
class StreetViewControl extends React.Component {
  helpDiv;
  

  createButtonControl() {
    const MapHelp = L.Control.extend({
      onAdd: (map) => {
        mapa = map;
        helpDiv = L.DomUtil.create("button", "click");
        this.helpDiv = helpDiv;
        helpDiv.innerHTML = '<img width="20px" src="https://lovelo.app/images/streetview.png"/>';

        helpDiv.addEventListener("click", (ev) => {
            const prop = this.props;  
            ev.stopPropagation();
            if(helpDiv.style.backgroundColor != ""){
              consoleLOG("cambimos el estado desde dentro a hidden")
                prop.handeDesactivateStreetView();
                
                //significa que está activo y lo han pulsado, por lo que quieren desactivarlo
                return;
            }
            consoleLOG("se pinta de amarillo)")
            helpDiv.style.backgroundColor = "#fffe02";
            //calculamos el punto central de la mitad de abajo del mapa
            var latLng = map.getCenter();
            var north = map.getBounds().getNorth();
            var Latpoint = north- ((north-latLng.lat)/2);
            var point = latLng;
            point.lat = Latpoint;
            
            marker = new L.marker(latLng, {icon: icon,draggable:'true'});
            
            marker.bindTooltip(lang.drag, 
                {
                    direction: 'right'
                });
            marker.on('mouseover', function (e) {
                this.openTooltip();
            });
            marker.on('mouseout', function (e) {
                this.closeTooltip();
            });

                      
            prop.handeClickStreetView(point);

            
            marker.on('dragend', function (e) {
                prop.handeClickStreetView(e.target.getLatLng());
            });
              
            marker.addTo(map);
        });

        //a bit clueless how to add a click event listener to this button and then
        // open a popup div on the map
        return helpDiv;
      }
    });
    return new MapHelp({ position: "topright" });
  }

  desactivar(){
    consoleLOG("desactivamos street view descactuvar ())")
    mapa.removeLayer(marker);
    counter=0;
    helpDiv.style.backgroundColor = "";
  }

  componentDidMount() {
    lang=this.props.lang;
    const { map } = this.props;
    const control = this.createButtonControl();
    control.addTo(map);
  }

  componentWillUnmount() {
    this.helpDiv.remove();
  }

  componentDidUpdate(prevProps, prevState) {
    //consoleLOG(prevProps)
    //consoleLOG(this.props);
    if((prevProps.streetView  == 'error' || prevProps.streetView  == '') && this.props.streetView == 'hidden'){
        this.desactivar();
        
    }
    else if(this.props.streetView == "error"){
        var color = helpDiv.style.backgroundColor;
        helpDiv.style.backgroundColor = 'red';
        setTimeout(function() {
          helpDiv.style.backgroundColor = color;
     }, 1000);
    }

    consoleLOG("se ha ctualizaaaaado con contador a "+ counter);
  }

  render() {
    consoleLOG("render hijo")
    return null;
  }
}

function withMap(Component) {
  return function WrappedComponent(props) {
    const map = useMap();
    return <Component {...props} map={map} />;
  };
}

export default withMap(StreetViewControl);
