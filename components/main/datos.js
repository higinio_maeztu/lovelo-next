import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown, faArrowsAltH, faArrowUp, faChartArea, faChartLine, faClock, faPercent, faPlayCircle, faStopCircle, faStream, faTachometerAlt, faWindowMaximize, faWindowMinimize} from '@fortawesome/free-solid-svg-icons'
import 'bootstrap/dist/css/bootstrap.min.css';
import Dropdown from 'react-bootstrap/Dropdown'
import {list_directions_prov, list_elevations_prov} from "../../utils/constantes";
import Dato from "./dato"
var lang = null;

var page;
class Datos extends Component{
    constructor(props){
        super(props);
        lang=this.props.lang;
        page = props.page;
        this.state ={
            travelMode:1//se define el travel mode a 1 que es coche-> normal maps. Directo es = 0,
        }
    } 

    
    render(){
        var datosRuta = this.props.estado.datosRuta;

        let distancia;
        console.warn("datosRuta")
        console.warn(datosRuta)
        if(page == "ruta"){
            distancia= datosRuta.distancia;
        }
        else{
            var array = this.props.estado.tramosDistancias;
            //consoleLOG(array)
            distancia = array.reduce((a, b) => a + b, 0);
        }
        var distanciacopy = distancia;

        console.log(distancia);
        
        //consoleLOG(datosRuta.dif_altura +"/"+ distancia +" = "+ (datosRuta.dif_altura / distancia));

        var pendiente = 0;
        if(distancia > 0)
            pendiente = ((datosRuta.dif_altura / distancia) * 100).toFixed(1) ;
        
        distancia = (distancia /1000).toFixed(1);

        var alturainicio = "--";
        var alturafin = "--";
        if(datosRuta.elevations.length > 0){
            alturainicio = parseInt(datosRuta.elevations[0].elevation);
            alturafin = parseInt(datosRuta.elevations[datosRuta.elevations.length - 1].elevation);
        }
        

        var ascenso = datosRuta.ascend;
        var descend = datosRuta.descend;
        var coeficiente = datosRuta.coeficiente;
        var alturamin = datosRuta.alturamin;
        var alturamax = datosRuta.alturamax;

        var tiempo = datosRuta.tiempo;
        var velocidad=distanciacopy/tiempo;
        //consoleLOG("velocidad")
        //consoleLOG(velocidad)
        velocidad=(velocidad/1000)*60;
        velocidad=velocidad.toFixed(1);

        //consoleLOG(velocidad)

        ////////////////////////////////
        var categoria = datosRuta.categoria;
        var categoriaCadena = categoria+"º";
        if(categoria == 0){
            categoriaCadena = "--";
        }
        else if(categoria == 4){
            categoriaCadena = "ES";
        }
        //////////////////////////////////////

        if(!this.props.estado.actualizado){
            ascenso = "--";
            pendiente = "--";
            descend = "--";
            alturainicio = "--";
            alturafin = "--";
            alturamin = "--";
            alturamax = "--";
            coeficiente = "--";
            velocidad = "--";
            tiempo = "--";
        }

        return (            
            <div className="panel panel-container menuDatos mt-3">
            <div className="row nopadding  border-bottom">
				<Dato icono={faArrowsAltH} texto={lang.distancia} unidad="km" valor={distancia} border="border-right" resalta="true"/>
                <Dato icono={faArrowUp} texto={lang.desnivel} unidad="m" valor={ascenso} border=""  resalta="true"/>
                <Dato icono={faClock} texto={lang.tiempo} unidad="" valor={tiempo+"'"} border="border-left"  resalta="true"/>
                <Dato icono={faPercent} texto={lang.pendiente} unidad="%" valor={pendiente} border="border-left"  resalta="true"/>
			</div>
            <div className="row nopadding  border-bottom">
            <Dato icono={faArrowDown} texto={lang.desnivel_neg} unidad="m" valor={descend} border="border-right"/>
                <Dato icono={faTachometerAlt} texto={lang.velocidad} unidad="km/h" valor={velocidad} border=""/>
                <Dato icono={faChartLine} texto={lang.coeficiente} unidad="" valor={coeficiente} border="border-left"/>
                <Dato icono={faStream} texto={lang.categoria} unidad="" valor={categoriaCadena} border="border-left"/>
			</div>
            <div className="row nopadding">
				<Dato icono={faPlayCircle} texto={lang.alturainicio} unidad="m" valor={alturainicio} border="border-right"/>
                <Dato icono={faStopCircle} texto={lang.alturafin} unidad="m" valor={alturafin} border=""/>
                <Dato icono={faWindowMinimize} texto={lang.alturamin} unidad="m" valor={alturamin} border="border-left"/>
                <Dato icono={faWindowMaximize} texto={lang.alturamax} unidad="m" valor={alturamax} border="border-left"/>
                
			</div>            
		</div>
        );
    }
}
export default Datos;