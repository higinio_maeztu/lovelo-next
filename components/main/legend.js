import React from "react"
import {colorBikePath, colorDefault, colorGravel, colorGround, colorPaved, colorRoad, colorSand} from "../../utils/constantes"
var lang;

class Legend extends React.Component{
    constructor(props){
        super(props);
        lang=this.props.lang;
        
    }    
    
    render(){
        return (
            <div className="container">
                <span className="leyenda_titulo">{lang.surface.toUpperCase()}</span>
                
                <div className="row align-items-center" style={{marginTop:"10px"}}>
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:colorRoad}}></div>
                    <div className="col-10">{lang.asphalt}</div>
                </div>
                <div className="row align-items-center">
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:colorPaved}}></div>
                    <div className="col-10">{lang.paved}</div>
                </div>
                <div className="row align-items-center">
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:colorBikePath}}></div>
                    <div className="col-10">{lang.bikepath}</div>
                </div>
                <div className="row align-items-center">
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:colorGravel}}></div>
                    <div className="col-10">{lang.gravel}</div>
                </div>
                <div className="row align-items-center">
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:colorGround}}></div>
                    <div className="col-10">{lang.ground}</div>
                </div>
                <div className="row align-items-center">
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:colorSand}}></div>
                    <div className="col-10">{lang.sand}</div>
                </div>
                <div className="row align-items-center" style={{marginBottom:"20px"}}>
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:colorDefault}}></div>
                    <div className="col-10">{lang.unknown}</div>
                </div>



                <span className="leyenda_titulo" >{lang.waytype.toUpperCase()}</span>
                
                <div className="row align-items-center"  style={{marginTop:"10px"}}>
                <div className="col-2 progress-bar-legend" style={{backgroundColor:'transparent', borderBottom:'2px solid '+colorRoad}}></div>
                    <div className="col-10">{lang.roadorsimilar}</div>
                </div>
                <div className="row align-items-center"  >
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:'transparent', borderBottom:'2px dotted '+colorRoad}}></div>
                    <div className="col-10">{lang.path}</div>
                </div>
                <div className="row align-items-center"  >
                    <div className="col-2 progress-bar-legend" style={{backgroundColor:'transparent', borderBottom:'2px dashed '+colorRoad  }}></div>
                    <div className="col-10">{lang.trackorbike}</div>
                </div>

            </div>
        );
    }
}
export default Legend;