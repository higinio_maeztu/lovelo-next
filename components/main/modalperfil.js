////////////////////////////////////////////////////////////////////////////////////// DEFINE //////////////////////////////////////////////////
/*react / components*/
import { faArrowsAltH, faPaintBrush, faPercent, faTags } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react'
import Modal from 'react-responsive-modal'
import Form from 'react-bootstrap/Form'
import {consoleLOG} from '../../utils/ifelse'



////////////////////////////////////////////////////////////////////////////////////// FIN DEFINE //////////////////////////////////////////////////
let bg = {
    overlay: {
      //lo que queda feura del modal
    },
    modal:{
        background: "white",
        width:'90%'
    },
    closeButton:{
        width:'20px',
        top:'5px',
        rigth:'1px'
    }
  };
let lang = null;
class ModalPerfil extends React.Component {
    
    constructor(props){
        super(props);
        lang=this.props.lang;
        consoleLOG("CONSTRUCTOR MODALPERFIL")
        this.state = this.props.optionsPerfil;
    }

    componentDidUpdate(){
        
    }

    updateParent(){
        this.props.toggleShowModalCallback()
    }
    
    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        //consoleLOG("vamos a actualizar el estado, cambiando la propiedad: " + nam + " al valor "+val+ " y antes el valor era "+this.state[nam]);
        this.setState({[nam]: val});
    }

    setValue (nam, color) {
        this.setState({[nam]: color});
    }

    setDecimales(valor) {
        var num=false;
        if(valor) num=true;
        this.setState({decimales: num});
    }


    Guardar(){
        //tendremos que coger el estado y mandárselo al padre para que lo actualice y cambie el perfil
        //si el segundo parámtro es true, actualizaría pero no cerraría la ventana.
        this.props.ActualizarPerfil(this.state, false);
    }
  

    render(){      
        consoleLOG("render modalperfil")
        consoleLOG(this.state)
        
        consoleLOG("modalperdfil")
        if(this.props.optionsPerfil != null){          
           
            return (             
                
                <Modal open={this.props.abierto} onClose={this.updateParent.bind(this)} center styles={bg}>
                    <div className="modalperfil_title">{lang.custom_perfil}<hr></hr></div>
                    <div className="container-fluid">
                                
                                <div className="well form-horizontal"  id="contact_form">
                                    <fieldset>
                                    
                                    <div className="form-group modalperfil_title2"><b><FontAwesomeIcon size="xs" icon={faArrowsAltH}/>{lang.chart_dimensiones}</b></div>
                                    
                                    <div className="row modalperfil_fila">
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.chart_altura_max}</span> <span className="control-label2">({lang.meters})</span> 
                                            <div className="inputGroupContainer">
                                                <input required name="altura_max" value={this.state.altura_max} onChange={this.myChangeHandler} className="form-control"  type="text"/>
                                            </div>
                                        </div>

                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.chart_width}</span> <span className="control-label2">({lang.percent_screen})</span> 
                                            <div className="inputGroupContainer">
                                                    <input required name="width" value={this.state.width} onChange={this.myChangeHandler} className="form-control"  type="text"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="form-group modalperfil_title2"><b><FontAwesomeIcon size="xs" icon={faPaintBrush}/>{lang.chart_design}</b></div>
                                    <div className="row modalperfil_fila">
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.chart_color}</span>
                                            <div className="inputGroupContainer">
                                                <Form.Control
                                                    type="color"
                                                    id="color"
                                                    defaultValue={this.state.color}
                                                    title={lang.choose_color}
                                                    onChange={e => this.setValue("color" , e.target.value)}
                                                />
                                            </div>
                                        </div>

                                        <div className="form-group col-xs-6 col-lg-3">
                                            <span className="control-label">{lang.lineas_km}</span> 
                                            <div className="inputGroupContainer">
                                                <Form.Check 
                                                    type="checkbox"
                                                    id="lineaskm-checkbox"
                                                    name="lineaskm"
                                                    checked={this.state.lineaskm}
                                                    onClick={e => this.setValue("lineaskm", e.target.checked)}
                                                />
                                            </div>
                                        </div>

                                        <div className="form-group col-xs-6 col-lg-3">
                                            <span className="control-label">{lang.lineashorizontales}</span> 
                                            <div className="inputGroupContainer">
                                                <input required name="lineashorizontales" value={this.state.lineashorizontales} onChange={this.myChangeHandler} className="form-control"  type="text"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="row modalperfil_fila">
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.color_lineashorizontales}</span>
                                            <div className="inputGroupContainer">
                                                <Form.Control
                                                    type="color"
                                                    id="lineashorizontales_color"
                                                    defaultValue={this.state.color_lineashorizontales}
                                                    title={lang.color_lineashorizontales}
                                                    onChange={e => this.setValue("color_lineashorizontales" , e.target.value)}
                                                />
                                            </div>
                                        </div>
                                        
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.fuente}</span> 
                                            <div className="inputGroupContainer">
                                                <select id="fuente" name="fuente" className="form-control" onChange={this.myChangeHandler}>
                                                    <option value="Arial" selected={this.state.fuente == "Arial"}>Arial</option>
                                                    <option value="Times New Roman" selected={this.state.fuente == "Times New Roman"}>Times New Roman</option>
                                                    <option value="Courier" selected={this.state.fuente == "Courier"}>Courier</option>
                                                    <option value="Verdana" selected={this.state.fuente == "Verdana"}>Verdana</option>
                                                    <option value="Tahoma" selected={this.state.fuente == "Tahoma"}>Tahoma</option>
                                                    <option value="sans-serif" selected={this.state.fuente == "sans-serif"}>sans-serif</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>



                                    <div className="form-group modalperfil_title2"><b><FontAwesomeIcon size="xs" icon={faPercent}/>{lang.chart_percent}</b></div>
                                    <div className="row modalperfil_fila">
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.porcentajesmayores}</span>
                                            <div className="inputGroupContainer">
                                            <div className="col-xs-6 selectContainer">
                                                <select id="porcentajesmayores" name="porcentajesmayores" className="form-control" onChange={this.myChangeHandler}>
                                                    <option value="0" selected={this.state.porcentajesmayores == 0}>0 %</option>
                                                    <option value="3" selected={this.state.porcentajesmayores == 3}>3 %</option>
                                                    <option value="5" selected={this.state.porcentajesmayores == 3}>5 %</option>
                                                    <option value="6" selected={this.state.porcentajesmayores == 6}>6 %</option>
                                                    <option value="8" selected={this.state.porcentajesmayores == 8}>8 %</option>
                                                    <option value="10" selected={this.state.porcentajesmayores == 10}>10 %</option>
                                                    <option value="12" selected={this.state.porcentajesmayores == 12}>12 %</option>
                                                    <option value="14" selected={this.state.porcentajesmayores == 14}>14 %</option>
                                                    <option value="16" selected={this.state.porcentajesmayores == 16}>16 %</option>
                                                    <option value="20" selected={this.state.porcentajesmayores == 20}>20 %</option>
                                                </select>
                                            </div>
                                            </div>
                                        </div>

                                        <div className="form-group col-xs-6 col-lg-3">
                                            <span className="control-label">{lang.decimales}</span> <span className="control-label2">(8.5)</span> 
                                            <div className="inputGroupContainer">
                                                <Form.Check 
                                                    type="checkbox"
                                                    id="decimales-checkbox"
                                                    name="decimales"
                                                    checked={this.state.decimales}
                                                    onClick={e => this.setDecimales(e.target.checked)}
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group col-xs-6 col-lg-3">
                                            <span className="control-label">{lang.porc_symbol}</span> <span className="control-label2">(8%)</span> 
                                            <div className="inputGroupContainer">
                                                <Form.Check 
                                                    type="checkbox"
                                                    id="porc_symbol-checkbox"
                                                    name="porc_symbol"
                                                    checked={this.state.porc_symbol}
                                                    onClick={e => this.setValue("porc_symbol",e.target.checked)}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row modalperfil_fila">
                                        <div className="form-group col-xs-12 col-lg-6">
                                                <span className="control-label">{lang.porc_color}</span>
                                                <div className="inputGroupContainer">
                                                    <Form.Control
                                                        type="color"
                                                        id="porc_color"
                                                        defaultValue={this.state.porc_color}
                                                        title={lang.porc_color}
                                                        onChange={e => this.setValue("porc_color" , e.target.value)}
                                                    />
                                                </div>
                                        </div>
                                            
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.porc_pixels}</span> <span className="control-label2">({lang.pixels})</span> 
                                            <div className="inputGroupContainer">
                                                <input required name="porc_pixels" value={this.state.porc_pixels} onChange={this.myChangeHandler} className="form-control"  type="text"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="form-group modalperfil_title2"><b><FontAwesomeIcon size="xs" icon={faTags}/>{lang.tags_profile}</b></div>
                                    <div className="row modalperfil_fila">
                                        <div className="form-group col-xs-12 col-lg-6">
                                                <span className="control-label">{lang.tags_color}</span>
                                                <div className="inputGroupContainer">
                                                    <Form.Control
                                                        type="color"
                                                        id="tags_color"
                                                        defaultValue={this.state.tags_color}
                                                        title={lang.tags_color}
                                                        onChange={e => this.setValue("tags_color" , e.target.value)}
                                                    />
                                                </div>
                                        </div>
                                            
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.tags_size}</span> <span className="control-label2">({lang.pixels})</span> 
                                            <div className="inputGroupContainer">
                                                <input required name="tags_size" value={this.state.tags_size} onChange={this.myChangeHandler} className="form-control"  type="text"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row modalperfil_fila">
                                        <div className="form-group col-xs-12 col-lg-6">
                                                <span className="control-label">{lang.tags_stem_color}</span>
                                                <div className="inputGroupContainer">
                                                    <Form.Control
                                                        type="color"
                                                        id="tags_stem_color"
                                                        defaultValue={this.state.tags_stem_color}
                                                        title={lang.tags_stem_color}
                                                        onChange={e => this.setValue("tags_stem_color" , e.target.value)}
                                                    />
                                                </div>
                                        </div>
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.tags_size_icon}</span> <span className="control-label2">({lang.pixels})</span> 
                                            <div className="inputGroupContainer">
                                                <input required name="tags_size_icon" value={this.state.tags_size_icon} onChange={this.myChangeHandler} className="form-control"  type="text"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row modalperfil_fila">    
                                        <div className="form-group col-xs-12 col-lg-6">
                                            <span className="control-label">{lang.tags_stem_size}</span> <span className="control-label2">({lang.pixels})</span> 
                                            <div className="inputGroupContainer">
                                                <input required name="tags_stem_size" value={this.state.tags_stem_size} onChange={this.myChangeHandler} className="form-control"  type="text"/>
                                            </div>
                                        </div>                                  
                                    </div>


                                    <div className="form-group row modalperfil_botones">
                                        <div className="editor-footer col-lg-3 col offset-lg-2 offset-0">
                                            <button onClick={()=>this.Guardar()} className="btn btn-secondary btn-block w-100"><span>{lang.guardar}</span></button>
                                        </div>
                                        <div className="editor-footer col-lg-3 col offset-1">
                                            <button onClick={this.updateParent.bind(this)} className="btn btn-secondary btn-block w-100"><span>{lang.cerrar}</span></button>    
                                        </div>
                                    </div>

                                    </fieldset>
                                </div>       
                    </div>                    
                </Modal>
            );

                }else{
                    return null;
                }
     }
}
 
export default ModalPerfil;