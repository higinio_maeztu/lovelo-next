import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown, faArrowsAltH, faArrowUp, faDownload, faChartLine, faClock, faPercent, faPlayCircle, faStopCircle, faStream, faTachometerAlt, faWindowMaximize, faWindowMinimize} from '@fortawesome/free-solid-svg-icons'
import 'bootstrap/dist/css/bootstrap.min.css';
import Dato from "./dato"
var lang = null;

class DatosIFrame extends Component{
    constructor(props){
        super(props);
        lang=this.props.lang;

        this.state ={
            travelMode:1//se define el travel mode a 1 que es coche-> normal maps. Directo es = 0,
        }
    } 

    
    render(){
        var datosRuta = this.props.estado.datosRuta;

        var array = this.props.estado.tramosDistancias;
        let distancia = array.reduce((a, b) => a + b, 0);
        var pendiente = 0;
        if(distancia > 0)
            pendiente = ((datosRuta.dif_altura / distancia) * 100).toFixed(1) ;        
        distancia = (distancia /1000).toFixed(1);
        var ascenso = datosRuta.ascend;
        var tiempo = datosRuta.tiempo;

        return (            
            <div className="panel panel-container menuDatos mt-3">
                <div className="row nopadding  border-bottom border-top">
                    <Dato icono={faArrowsAltH} texto={lang.distancia} unidad="km" valor={distancia} border="border-right" resalta="true"/>
                    <Dato icono={faArrowUp} texto={lang.desnivel} unidad="m" valor={ascenso} border=""  resalta="true"/>
                    <Dato icono={faClock} texto={lang.tiempo} unidad="" valor={tiempo+"'"} border="border-left"  resalta="true"/>
                    <Dato icono={faPercent} texto={lang.pendiente} unidad="%" valor={pendiente} border="border-left"  resalta="true"/>
                </div>
                <div className="row nopadding  border-bottom">
                    <div className={"col-3 botonruta nopadding "}  onClick={() => this.Descargar()}> 
                        <div>
                            <span className="dato-icon2"><FontAwesomeIcon icon={faDownload} aria-hidden="true"  size="lg" /></span>
                        </div>
                        
                        <div>                                        
                            <span className="dato-text">GPX</span>
                        </div>
                    </div>
                </div>
            
            </div>
        );
    }
}
export default DatosIFrame;