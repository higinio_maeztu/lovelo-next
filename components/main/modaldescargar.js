import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBrush, faDownload, faEdit, faLayerGroup, faPaintBrush, faPencilAlt, faPlug, faPlus, faPlusCircle, faUpload } from "@fortawesome/free-solid-svg-icons";
import React, { createRef } from "react";
import Modal from 'react-responsive-modal'
import { withRouter } from "react-router-dom";
import logo from '../../public/images/layers.png'

let bg = {
    overlay: {
      //lo que queda feura del modal
    },
    modal:{
        background: "white",
        width:'90%',
        textAlign: "center",
        fontFamily: "Helvetica, Helvetica Neue, Arial"
    },
    closeButton:{
        width:'25px',
        top:'2px',
        rigth:'0px'
    }
  };

let lang = null;
class ModalDescargar extends React.Component {
    
    constructor(props){
        super(props);
        lang=this.props.lang;

        this.state ={
            checkboxradio:"1"
        }

    }

    
    updateParent(){
        this.props.toggleShowModalCallback()
    }


    onChangeValue(event) {
        this.setState({checkboxradio: event.target.value});
    }
    

    render(){      
                    
            return (             
                
                <Modal open={this.props.abierto} onClose={this.updateParent.bind(this)} center styles={bg}>
                    <div className="modalperfil_title">{lang.downloadformat + this.props.formato.toUpperCase()}<hr></hr></div>
                    <div className="container-fluid margintop modalimportar">
                                
                                <div className='checkboxradio' onChange={this.onChangeValue.bind(this)}>
                                    <input checked={this.state.checkboxradio === '1'} type="radio" value="1" name="ele" /> {lang.originalSin}<b style={{color:'green'}}>{lang.recommend}</b><br></br>
                                    <input checked={this.state.checkboxradio === '2'} type="radio" value="2" name="ele" /> {lang.simpleSin}<b style={{color:'green'}}>{lang.fast}</b><br></br>
                                    <input checked={this.state.checkboxradio === '3'} type="radio" value="3" name="ele" /> {lang.simpleEle}<b style={{color:'green'}}>{lang.fast}</b><br></br>
                                    <input checked={this.state.checkboxradio === '4'} type="radio" value="4" name="ele" /> {lang.originalEle}<b style={{color:'red'}}>{lang.slow}</b>
                                </div>
                                <div className="form-group row modalperfil_botones">
                                    <div className="editor-footer col text-center">
                                        <button onClick={()=>this.props.Descargar(this.state.checkboxradio)} className="btn btn-secondary btn-block">
                                            <FontAwesomeIcon size="md" icon={faDownload}/>
                                            <span>{lang.download}</span>
                                        </button>
                                    </div>
                                </div>
                    </div>                         
                </Modal>
            );

     }
}
 
export default ModalDescargar;