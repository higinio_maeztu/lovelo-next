import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {colorGris} from "../../utils/constantes";

class OpcionRuta extends Component{
    constructor(props){
        super(props);
    } 

    
    render(){
        var over = (this.props.valorActual == this.props.valor)? "true" : "";
        return (            
            <div className={"col-4 dato-box opcionruta nopadding " + this.props.border} onClick={()=> this.props.myClickHandler(this)} > 
            	<div>
                    <span className="dato-icon"><img src={this.props.icono.src} color={colorGris} className="dato-icon" style={over ? { color: "#17a2b8" } : {}}/></span>
                </div>
                
                <div>                    
                    <span className="dato-text" style={over ? { color: "#17a2b8" } : {}}>{this.props.texto}</span>
                </div>
			</div>
        );
    }
}
export default OpcionRuta;