import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {GetIcon,GetRoadColor,GetRoadCode, list_directions_prov_siglas} from "../../utils/constantes";
import {distanciaTramo} from '../../utils/matematicas';
import { consoleLOG } from '../../utils/ifelse';
import Image from 'next/image'

var lang = null;


class Tramos extends Component{
    constructor(props){
        super(props);
        lang=this.props.lang;

        this.state ={
            travelMode:1//se define el travel mode a 1 que es coche-> normal maps. Directo es = 0,
        }
    }

    GetSurfacesBar(polyline){
        var distance = polyline.distance;
        var surfaces = polyline.pathSurfaces;

        var arrayDivs = [];
        
        //arrayDivs.push(<div className="progress col-1" style={{paddingLeft:"0px"}}>);

        //console.log(surfaces)
        surfaces.forEach(surface => {
            var surfaceCode = surface.properties.surface;
            var distancia_tramo = surface.distance;
            var percent = (distancia_tramo/distance)*100;
            var roadCode = GetRoadCode(surfaceCode, surface.properties.waytype);
            consoleLOG(roadCode + " ES ESE y el color por TTANASNAS")
            var color = GetRoadColor(roadCode);
            console.log(" la distancia " + distance + " la distancia tramo es " + distancia_tramo +  " y el percent es " + percent + " y el color es " + color)
            arrayDivs.push({"percent":percent,"color":color});
        })
        
        return arrayDivs;
    }

    

    
    render(){
        var polyline = this.props.polyline;
        var idy = this.props.idy;
        var d = polyline.distance/1000;
        if(polyline.distance < 10000)
            d = d.toFixed(1);
        else if(polyline.distance > 1000)
            d = d.toFixed(0);
        else
            d = d.toFixed(1);
        var surfaces = this.GetSurfacesBar(polyline);
        //consoleLOG(surfaces);
        return (            
                        
                            <div id="tramo" className="row">
                                   <div className="col-1 margenes">
                                        {idy+1}.
                                   </div>
                                    
                                    <div className="col-1 click" onClick={() =>this.props.showToastTramo(0,polyline.travelMode)}> 
                                        <Image width="20px" height="20px" className='tramoimg' src={"/images/"+GetIcon(polyline.travelMode)+".png"}/>
                                    </div>


                                    <div className="col-3 margenes click" onClick={() =>this.props.showToastTramo(1,polyline.provider+"["+list_directions_prov_siglas[polyline.provider]+"]")}>
                                        {list_directions_prov_siglas[polyline.provider]}
                                    </div>
                                    
                                    
                                    <div className="progress col-2 align-self-center margenes" style={{padding:"0px"}} onClick={() =>this.props.showToastLegend()}>
                                        {   
                                            surfaces.map((surface, id) => <div key={id} className="progress-bar" style={{width: surface.percent+'%', backgroundColor:surface.color}}></div>)
                                        }
                                    </div>

                                    <div className="col-4 margenes">{d + " km"}</div>
                                
                            </div>
                            )
    }
}
export default Tramos;