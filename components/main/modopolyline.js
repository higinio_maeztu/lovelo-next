import React from "react"
import roadBike from '../../public/images/road.png'
import mtbBike from '../../public/images/mtb.png'
import hiking from '../../public/images/hiking.png'
import direct from '../../public/images/direct.png'
import 'bootstrap/dist/css/bootstrap.min.css';
import Dropdown from 'react-bootstrap/Dropdown'
import {GetIcon,GetTextMode,list_directions_prov, capitalize} from "../../utils/constantes";
import Image from 'next/image'
var lang;

class ModoPolyline extends React.Component{
    constructor(props){
        super(props);
        lang=this.props.lang;
        
    }    
    
    render(){
        //edit será si es posible editar el tramo, y show si sólo es mostrarlo.
        var modoPolyline = this.props.mode;
        
        var polylineID = this.props.id;
        var mode1,mode2,mode3,mode0="";
        if(this.props.travelMode == "DRIVING"){
            mode1 =" active";
        }
        else if(this.props.travelMode == "WALKING"){
            mode2 =" active";
        }
        else if(this.props.travelMode == "BICYCLING"){
            mode3 =" active";
        }
        else if(this.props.travelMode == "DIRECT"){
            mode0 =" active";
        }
        
        var direactionsactive = this.props.directions_prov;
        var este = this;
        var menudirections = [];
        Object.keys(list_directions_prov).map(function(key) {
            var prov = list_directions_prov[key];
            if(direactionsactive == key){
                if(modoPolyline == "edit")
                    menudirections.push(<Dropdown.Item onClick ={()=>este.props.HandledirectionsProvPolyline(polylineID, key)} active>{prov}</Dropdown.Item>);
                else
                    menudirections.push(<Dropdown.Item active style={{cursor:'default'}}>{prov}</Dropdown.Item>);
            }
            else{
                if(modoPolyline == "edit")
                    menudirections.push(<Dropdown.Item onClick ={()=>este.props.HandledirectionsProvPolyline(polylineID, key)}>{prov}</Dropdown.Item>);
                else
                    menudirections.push(<Dropdown.Item style={{cursor:'default'}}>{prov}</Dropdown.Item>);
            }

        });

        if(modoPolyline == "edit")
            var titulo = lang.edit + " " +lang.stretch + " "+(polylineID+1);
        else
            var titulo = lang.stretch + " "+(polylineID+1);

        return (
            <div className="container">
                <div className="row popup_titulo text-center"><div className="col-12">{titulo}</div></div>
                <hr></hr>
               
                    {modoPolyline == "edit" &&
                         <div className="row text-center justify-content-end">
                            <div className="btn-grupo margin0 col-1 popupMode" role="group" aria-label="..." >
                                <button  onClick ={()=>this.props.handeTravelModePolyline(polylineID, "WALKING")} title={lang.mod_walking} name="mode" type="button" className={"btn-light btn-sm mode"+mode2}>
                                    <Image width="20px" height="20px" className='tramoimg' src={"/images/hiking.png"}/>
                                </button>
                                <button onClick ={()=>this.props.handeTravelModePolyline(polylineID, "BICYCLING")} title={lang.mod_bike} name="mode" type="button" className={"btn-light btn-sm mode"+mode3}>
                                    <Image width="20px" height="20px" className='tramoimg' src={"/images/mtb.png"}/>
                                </button>
                                <button onClick ={()=>this.props.handeTravelModePolyline(polylineID, "DRIVING")} title={lang.mod_drive} type="button" name="mode" className={"btn-light btn-sm mode"+mode1}>
                                    <Image width="20px" height="20px" className='tramoimg' src={"/images/road.png"}/>
                                </button>  
                                <button onClick ={()=>this.props.handeTravelModePolyline(polylineID, "DIRECT")} title={lang.mod_eki} name="mode" type="button" className={"btn-light btn-sm mode"+mode0}>
                                    <Image width="20px" height="20px" className='tramoimg' src={"/images/direct.png"}/>
                                </button>    
                            </div>

                            <div className="col-2" role="group" aria-label="..." ></div>
                                                
                            <div className="btn-grupo directions margin0 col-9 float-right popupDirections" role="group" aria-label="..." >
                                {menudirections}
                            </div>
                        </div>
                    }
                    {modoPolyline != "edit" &&
                        <div className="popupMode">
                            <div className="row">
                                <div className="col-12">
                                    {(this.props.distance/1000).toFixed(1)+ " Km"}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                     <span>{capitalize(lang[GetTextMode(this.props.travelMode)])}</span><img width="20px" src={"/images/"+GetIcon(this.props.travelMode)+".png"}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <span>{lang.calculated +" "} <b>{list_directions_prov[this.props.directions_prov]}</b></span>
                                </div>
                            </div>
                        </div>
                    }
                    

                    

                

            </div>
        );
    }
}
export default ModoPolyline;