import { useEffect } from "react";
import { useMap } from "react-leaflet";
import { GeoSearchControl,OpenStreetMapProvider } from "leaflet-geosearch";

import L from "leaflet";
var icon =  L.icon({
    iconSize: [25, 41],
    iconAnchor: [10, 41],
    popupAnchor: [2, -40],
    iconUrl: "https://unpkg.com/leaflet@1.6/dist/images/marker-icon.png",
    shadowUrl: "https://unpkg.com/leaflet@1.6/dist/images/marker-shadow.png"
  });

const SearchControl = props => {
    const map = useMap();
    const provider = new OpenStreetMapProvider();
  
    useEffect(() => {
      const searchControl = new GeoSearchControl({
        provider: provider,
        position: "topright",
        showMarker:"true",
        showPopup:"false",
        maxMarkers:"1",
        retainZoomLevel:"false",
        animateZoom:"true",
        autoClose:"false",
        keepResult:"true",
        marker: {
            icon
          },
        ...props
      });
  
      map.addControl(searchControl);
      return () => map.removeControl(searchControl);
    }, [props]);
  
    return null;
  };

  export default SearchControl;  