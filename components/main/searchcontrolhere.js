import React, { Component } from "react";
import { useMap } from "react-leaflet";
import L from "leaflet";
import {consoleLOG} from '../../utils/ifelse'

const icon = L.icon({
    iconSize: [30, 30],
    iconAnchor: [15, 15],
    popupAnchor: [0, 15],
    iconUrl: "https://ekibike.com/images/streetview.png"
  });

  let counter =0;
  let marker = null;
  let mapa = null;
  let helpDiv = null;
  var lang = null;
class SearchControlHere extends React.Component {
  helpDiv;
  
  createButtonControl() {
    const MapHelp = L.Control.extend({
      onAdd: (map) => {
        mapa = map;
        helpDiv = L.DomUtil.create("button", "click");
        L.DomUtil.addClass(helpDiv, "buttonSearch")
        this.helpDiv = helpDiv;
        helpDiv.innerHTML = '<img width="10px" src="https://lovelo.app/images/lupa.svg"/><span>' +lang.search_here+"</span>";

        helpDiv.addEventListener("click", (ev) => {
            const prop = this.props;  
            ev.stopPropagation();                      
            prop.handeClickSearchHere();
        });

        //a bit clueless how to add a click event listener to this button and then
        // open a popup div on the map
        return helpDiv;
      }
    });
    return new MapHelp({ position: "topleft" });
  }

  desactivar(){
    consoleLOG("desactivamos street view descactuvar ())")
    mapa.removeLayer(marker);
    counter=0;
    helpDiv.style.backgroundColor = "";
  }

  componentDidMount() {
    lang=this.props.lang;
    const { map } = this.props;
    const control = this.createButtonControl();
    control.addTo(map);
  }

  componentWillUnmount() {
    this.helpDiv.remove();
  }

  componentDidUpdate(prevProps, prevState) {
    //consoleLOG(prevProps)
    //consoleLOG(this.props);
    if((prevProps.streetView  == 'error' || prevProps.streetView  == '') && this.props.streetView == 'hidden'){
        this.desactivar();
        
    }
    else if(this.props.streetView == "error"){
        var color = helpDiv.style.backgroundColor;
        helpDiv.style.backgroundColor = 'red';
        setTimeout(function() {
          helpDiv.style.backgroundColor = color;
     }, 1000);
    }

    consoleLOG("se ha ctualizaaaaado con contador a "+ counter);
  }

  render() {
    consoleLOG("render hijo")
    return null;
  }
}

function withMap(Component) {
  return function WrappedComponent(props) {
    const map = useMap();
    return <Component {...props} map={map} />;
  };
}

export default withMap(SearchControlHere);
