import {GetWaytypeTextAndColorByCode, CreateIconLeaflet,capitalize,baseweb,urlWS,FormatPercent, HasSurfaces,centerdefault, apikey_mapbox, GerSurfaceTextAndColorByCode, apiKey_openroute, GeoJsonOptions, perfilOptions, TracksOptions, cadadist,GetIconRouteType, GetIconRouteBicicleta, GetIconRouteActivity, baseurl, IconKm, ntobr} from "../utils/constantes";
import {CalcPolylinesyTramosFromBD, calcularDatosRuta, formarperfil, decodePolyline, marcadoreskms,GetSurfacesChart,calcularEncodedElevationsRuta} from '../utils/matematicas.js'
import {consoleLOG, GetParamsUserAgent, URLamigable  } from '../utils/ifelse.js'
import React from "react";
import { Component } from "react";
import {latLngBounds} from 'leaflet'
import Tramos from './main/tramos'
import ModoPolyline from './main/modopolyline'
import Comment from './main/comment';
import Datos from './main/datos'
import Perfil from './main/perfil'
import ModalCompartir from "./main/modalcompartir";
import ModalDescargar from "./main/modaldescargar";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChartArea, faDownload,  faLandmarkFlag,  faPencilAlt,  faShare, faStar,  faUser} from '@fortawesome/free-solid-svg-icons'
import { MapContainer, TileLayer, WMSTileLayer, LayersControl, Marker, Polyline,ZoomControl, Tooltip, GeoJSON, Popup,LayerGroup} from 'react-leaflet';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'leaflet/dist/leaflet.css';
import L, { polyline } from 'leaflet';
import StreetViewControl from './main/streetview';
import ButterToast, { Cinnamon,POS_TOP, POS_CENTER  } from 'butter-toast';
import Legend from './main/legend'
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css'

import router from 'next/router'
import Link from 'next/link'
import { ConsoleView } from "react-device-detect";
import { loadModules } from 'esri-loader';

let params = null;
var kilometerschecked = true;
var markerschecked = true;
let resultporpartes = [];//array para el cálculo de perfil por partes, en algún sitio hay que acumularlo
var lang;
var userID;
var user;
var clase = "dafault";
var bounds = centerdefault;
let perfil = [];
let path = null;
let markerprofile=null;//posición del marcador que se mueve cuando avanzamos sobre el perfil
var colorhex = "#FFC625";
var clasemapa = "h-700"; //este es el alto para cuando hay imágens, porque si no lo especificamos se queda una línea gris. Esto se podría mejorar calulando el alto del div id=derecha y metiendole el mismo alto al mapa
/*VARIABLES QUE PODRIA IR EN ESTADO PERO  NO CAMBIAN*/
    var tipo_icono,tipo_texto,bicicleta_texto,bicicleta_icono,actividad_texto,actividad_icono = null;
    
/**/
var rutaID,urlRUTA = null;
var opcionActual = null;
const ref = React.createRef();
class Ruta extends Component {
    
    constructor(props){
        super(props);
        var ruta = props.ruta;
        rutaID = ruta.rutaID;
        lang = props.lang;
        user = props.user;

        urlRUTA = URLamigable(ruta.titulo)+"_"+rutaID;

        if(user)
            userID = user.userID;
        else
            userID = -1;

        
        this.handleStateChangeLP  = this.handleStateChangeLP.bind(this);
        
        var isPropietario = false;
        if(ruta.userID == userID){
            isPropietario = true;
        }
        var ararryID = [];
        ruta.images.map((image) => {
            ararryID.push(image.rutaImageID);
        })

        path = decodePolyline(ruta.polyline);
        //console.debug(path.length)
        //Recordar que me inventé
        
        bounds = latLngBounds([path[0].lat, path[0].lng])
        for (let index = 1; index < path.length; index++) {
            bounds.extend(path[index])            
        }

        //PROBLEMÓN, YO SE LAS ELEVACIONES PERO NO SÉ DE QUÉ PUNTOS DEL PATH, PORQUE ELEVACIONES TENGO 20 Y PATH 80
        //DEBO CALCULAR QUÉ PUNTOS SON LOS QUE ESTÁN A 100M, QUE VI A HACER
        var arrayTrans = formarperfil(ruta.perfilLocations, ruta.perfilElevations);
        
        
        //Esto hay que cambiarlo, porque no tendremos los arrays que devuelve ORS tal cuales, tendremos la info como la guardamos, simples coordenadas [ini,fin,waytype,surface]
        
        var arrayResult = CalcPolylinesyTramosFromBD(path, ruta.surfaces);      
        var polylines = arrayResult[0];
        var markersTodos = arrayResult[1];
        //var markers = arrayResult[1];
        var tramosDistancias = arrayResult[2];

        var markers = [markersTodos[0], markersTodos[markersTodos.length-1]]

        var marcadoreskm = marcadoreskms(polylines, tramosDistancias);
        

        
        
        

        ruta.elevations = arrayTrans;
        if(ruta.anns != undefined && ruta.anns != null && !Array.isArray(ruta.anns))
            ruta.annotations = JSON.parse(ruta.anns);
        else
            ruta.annotations = [];
        perfil = arrayTrans;

        //ESTO NO HACE FALTA EN PRINCIPIO, PERO SI QUISIÉRAMOS RECALCULAR LOS DATOS SI QUE HARÍA FALTA, POR EJEMPLO SI QUEREMOS RECALCULAR EL DESNIVEL


        var speedMode="22";
        if(ruta.velocidad)
            speedMode = ruta.velocidad;
        
        //let distancia = tramosDistancias.reduce((a, b) => a + b, 0);
        let distancia = ruta.distancia;
        var datosruta = calcularDatosRuta(arrayTrans, distancia, cadadist, speedMode);
        ruta.distancia = distancia;
        ruta.alturamin = datosruta[0];
        ruta.alturamax = datosruta[1];
        ruta.descend = datosruta[2];
        ruta.ascend = ruta.desnivel;
        ruta.dif_altura = datosruta[4];
        ruta.tiempo = datosruta[5];
        ruta.coeficiente = ruta.coeficiente;
        ruta.categoria = ruta.categoria;


        
        var [html_tramosJOIN, percentSurfaces, percentWaytypes] = GetSurfacesChart(polylines, distancia);

        ruta.percentSurfaces = percentSurfaces;
        ruta.percentWaytypes = percentWaytypes;

        /*///*/
        var tipo_html = GetIconRouteType(ruta.tipo)
        tipo_icono=tipo_html[0];
        tipo_texto=lang[tipo_html[1]];

        var bicicleta_html = GetIconRouteBicicleta(ruta.bicicleta)
        bicicleta_icono = bicicleta_html[0];
        bicicleta_texto = lang[bicicleta_html[1]];

        var actividad_html = GetIconRouteActivity(ruta.actividad)
        actividad_icono = actividad_html[0];
        actividad_texto = lang[actividad_html[1]];

        consoleLOG("ruta CONSTRUCTOR");
        consoleLOG(ruta);

        //IMAGEs*/
        var images = [];
        ruta.images.map((image) => {
            var url = baseurl+image.image;  
                images.push({"original":url, "thumbnail":url}); 
        })
        
        var optionsP = perfilOptions;
        if(ruta.opcionesPerfil)
            optionsP = JSON.parse(ruta.opcionesPerfil);

        this.state ={
            datosRuta:ruta,
            tramosDistancias:tramosDistancias,
            userID:userID,
            rutaID:ruta.rutaID,
            arrayLocalPhotos:[],
            isPropietario:isPropietario,
            loading:false,
            completed:true,
            resultado:true,
            streetView: 'hidden',
            openPerfil:true,
            optionsPerfil:optionsP,
            tracks:[],
            actualizado:true, 
            markers:markers,
            kms:marcadoreskm,
            images:images,
            flag:1,
            repintarPerfil:false,
            mapacargado : false,
            map:null,
            polylines:polylines,
            rating: -1,
            enviado: false,
            comment:'',
            mostrarLayers:false,
            html_tramosJOIN:html_tramosJOIN,
            tramosOterreno:1,
            modalCompartirIsOpen:false,
            modalDescargarIsOpen:false,
            formato:"gpx"
        }

        
    }



    convertToParamas(objeto){ //operations
       return {
        'titulo':objeto.titulo,
        'descripcion':objeto.descripcion,
        'tipo':objeto.tipo,
        'terreno':objeto.terreno,
        'dificultad':objeto.dificultad,
        'ciclabilidad':objeto.ciclabilidad,
        'images':objeto.imagesID,
        'rutaID':objeto.rutaID
       }
    }

      removePhoto(id){
            var imagesID = [];
            var arrayI = [];
            this.state.images.map((image) => {
                if(image.rutaImageID != id){
                    arrayI.push(image);
                    imagesID.push(image.rutaImageID);
                }
            })
            consoleLOG(this.state.images);
            consoleLOG(arrayI);
            this.setState({images:arrayI, imagesID:imagesID});
      }

      shouldComponentUpdate(nextProps, nextState) {
        consoleLOG(this.state);
        consoleLOG(nextState);
        return this.state.flag != nextState.flag;
    }

    componentDidUpdate(prevProps, prevState) {
    
        const { map } = this.state;
        const este = this;
        if (prevState.map !== map && map) {
            map.on('zoomend', function() {
                este.changeZoom()
            });                
        }
    }

    TramoOTerreno(val)
        {
            if(this.state.tramosOterreno != val)
                this.setState({ tramosOterreno:val,   flag: this.state.flag+1});  
        }

    changeZoom(){
        const { map } = this.state;
        var zoom = map.getZoom();
        //si quitamos zoom y estaba activo, lo desactivamos
        if(zoom <= 10 && this.state.mostrarLayers){
            this.setState({ mostrarLayers:false, flag: this.state.flag+1});  
        }
        //si hacemos zoom y estaba desactivo, lo activamos
        if(zoom > 10 && !this.state.mostrarLayers){
            this.setState({ mostrarLayers:true, flag: this.state.flag+1});  
        }          
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
      }

    myClickHandler(object){
        var props = object.props;
        var nam = props.campo;
        var val = props.valor;
        this.setState({[nam]: val});
    }


    handleStateChangeLP(files) {
        this.setState({arrayLocalPhotos:files})
    }

    handeDesactivateStreetView(){
        this.setState({
            streetView:'hidden',flag: this.state.flag+1
        });
    }

    handeClickStreetView(e){
        this.state.map.off("click");
        //al principio del arrastre, deshabilitamos el click, ahora lo habilitamos, dentro de 1 segundo que ya habrá terminado la posibilidad de crear un click al sortar el arrastre
        var este = this;
        var panorama = null;
        var svs = new window.google.maps.StreetViewService();
        svs.getPanorama({location: e, preference: 'nearest'}, function(data, status){
            if(data == null){
                este.setState({
                    streetView:'error',flag: este.state.flag+1
                });
                return;
            }
            consoleLOG('pano1: ' + data.location.pano);
            consoleLOG('pano result: ' + data);
            var pos = data.location.latLng;
            consoleLOG(pos.toString());
            
            if(este.state.streetView != ''){
                este.setState({
                    streetView:'',flag: este.state.flag+1
                });
            }

            panorama = new window.google.maps.StreetViewPanorama(
            document.getElementById('pano'),
            {
              pano: data.location.pano
            });
        });

              
    }

    showToastTramo(tipo, valor){
        class HtmlDiv extends React.Component {
            render() {
              return <div className="toastInfo">{this.props.pre}<b>{this.props.valor}</b>{this.props.post}</div>
            }
        }
        
        valor = valor.toUpperCase();

        if(valor == "BICYCLING"){
            valor = lang.mod_bike;
        }
        if(valor == "DRIVING"){
            valor = lang.mod_drive;
        }

        var mensaje = "";
        if(tipo == 1){
            mensaje = lang.providerInfo ;
        }
        else if(tipo == 0){
            mensaje = lang.travelModeInfo;
        }

        this.OcultarButterToastAndDiv();
        ButterToast.raise({
            timeout: 3500,
            content: <Cinnamon.Crisp scheme={Cinnamon.Crisp.SCHEME_BLUE}
            content={<HtmlDiv pre={mensaje} valor={valor} post={lang.action}/>}/>
            });
    }

    OcultarButterToastAndDiv(){
        ButterToast.dismissAll();
    }

    showToastLegend() {
        this.OcultarButterToastAndDiv();
        ButterToast.raise({
            timeout: 3500,
            content: <Cinnamon.Crisp scheme={Cinnamon.Crisp.SCHEME_BLUE}
            content={<Legend lang={lang}/>}/>
            });
    }

    HandleMouseOutChart(){
        consoleLOG("mouseout")
        if(markerprofile != null){
            this.state.map.removeLayer(markerprofile);
            markerprofile = null;
        }
    }

    HandleMouseOverChart(lat ,lng){
        var newLatLng = new L.LatLng(lat, lng);
        //consoleLOG(newLatLng);
        if(markerprofile== null){
            var iconRueda = CreateIconLeaflet("mtbpin",20);
            markerprofile = L.marker(newLatLng, {icon: iconRueda}).addTo(this.state.map);
        }
        else{
            markerprofile.setLatLng(newLatLng);
        }
    }

    AbrirPefil(){
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {openPerfil:true,repintarPerfil:false, flag: this.state.flag+1}
        });
    }

    CerrarPerfil(){
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {openPerfil:false,repintarPerfil:false, flag: this.state.flag+1}
        });
    }
    
    EditarPerfil(){
        Router.push("./route/"+urlRUTA);
        return null;
    }

    clickpolyline (event, index){
        event.originalEvent.view.L.DomEvent.stopPropagation(event)
        var poly = this.state.polylines[index];
        var travelMod = poly.travelMode;
        //clickpolycount++;        
    }

    HandleClickChart(row){
        /*
        NO PERMITIMOS EDITAR DESDE RUTA, ASÍ QUE NO HACEMOS NADA
        consoleLOG("fila clicada num:" + row)
        var pos = this.PosAnnotation(row);
        var text = '';
        var type = 'texto';
        if(pos >= 0){
            text = this.state.datosRuta.annotations[pos].text;
            type = this.state.datosRuta.annotations[pos].type;
        }
            

        this.setState((state) => {
            return {modalAnnotationIsOpen:true, RowAnnotationclicked:row, TextAnnotationclicked:text,typeAnnotationclicked:type, repintarPerfil:false, flag: this.state.flag+1}
        });*/
    }

    Descargar(opcion){
        opcionActual = opcion;
        var self = this;
        var format = this.state.formato;
        console.warn("La opción para descargar"+opcion + " y el formato es "+format + " y polylineElevatios es " + this.state.datosRuta.polylineElevations)
        if(opcion == 4 && this.state.datosRuta.polylineElevations == null){
            self.setState((state) => {
                // Importante: lee `state` en vez de `this.state` al actualizar.
                return {loading:true, flag: this.state.flag+1}
            });
            //esta es la opción que requiere calcular todas las elevaciones de todo el path. 
            if(path.length > 300){
                resultporpartes = [];
                this.calcularAlturasPorPartes("arcgis", 0, path);              
            }
            else
                this.calcularAlturas("arcgis", path);
        }
        else{
            self.setState((state) => {
                // Importante: lee `state` en vez de `this.state` al actualizar.
                return {modalDescargarIsOpen:false, flag: this.state.flag+1}
            });
            //todas estas opciones se pueden sacar de la base de datos por lo que se le pasa al WS
            window.open(urlWS+"ruta/download.php?id="+rutaID+"&format="+format+"&option="+opcion, "_blank");
        }
    }

    calcularAlturas(proveedor, perfil){
        if (typeof window !== "undefined") {
            consoleLOG("Cálculo de elevación con "+ proveedor);
            var este = this;
            if(proveedor == "google"){
                    var locations = [];            
                    for(var i=0;i<perfil.length;i++){
                        var latlng = new window.google.maps.LatLng(perfil[i][0], perfil[i][1]);
                    }
                    var positionalRequest = {
                        'locations': locations
                    }
                    var elevationService = new window.google.maps.ElevationService();
                    elevationService.getElevationForLocations(positionalRequest, function(results, status) {
                    if (status == window.google.maps.ElevationStatus.OK) {
                        este.calcularDatos(results);
                        //consoleLOG(results);
                    } else {
                        alert("error en el cáculo de elevación.");
                    }
                    });
                

            }
            else if(proveedor == "arcgis"){
                loadModules(['esri/Map', "esri/geometry/Polyline", "esri/geometry/SpatialReference"])
                .then(([Map, Polyline, SpatialReference]) => {
                    const map = new Map({
                        ground: "world-elevation"
                    });
                    

                    var path2 = [];
                    for(var i = 0; i < perfil.length; i++){
                        path2.push([perfil[i][1], perfil[i][0]]);
                    }
                    
                        consoleLOG("vamos a calcular el perfil:")
                        consoleLOG(path2)
                        const elevationPromise = map.ground.queryElevation(
                            new Polyline({
                            paths: path2,
                            spatialReference: SpatialReference.WGS84
                            })
                        );

                        elevationPromise.then((result) => {
                            consoleLOG("resultados:");
                            consoleLOG(result);
                            const path = result.geometry.paths[0];
                            consoleLOG(path);

                            //ahora tengo en path un array de [lng,lat,alt]

                            //este.calcularDatos(results);
                            var arrayTrans = [];
                            for(var i=0;i<path.length;i++){
                                var latlng = new window.google.maps.LatLng(path[i][1], path[i][0]);

                                arrayTrans.push({elevation:path[i][2], location:latlng});
                            }
                            este.calcularDatos(arrayTrans);
                        });
                });

            }else if(proveedor == "openrouteservice"){
                let request = new XMLHttpRequest();
                request.open('POST', "https://api.openrouteservice.org/elevation/line");            
                request.setRequestHeader('Accept', 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8');
                request.setRequestHeader('Content-Type', 'application/json');
                request.setRequestHeader('Authorization', apiKey_openroute);
                request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    let response = JSON.parse(this.response);
                    var path = response.geometry;
                    //consoleLOG("encoded " + g);
                    //var path = decodePolylineElevation(g, true);
                    consoleLOG(path)
                    var arrayTrans = [];
                    for(var i=0;i<path.length;i++){
                        var latlng = new window.google.maps.LatLng(path[i][1], path[i][0]);
                        arrayTrans.push({elevation:path[i][2], location:latlng});
                    }
                    este.calcularDatos(arrayTrans);
                }
                };

                var pathO = "[";
                for(var i = 0; i < perfil.length; i++){
                    if(i== 0)
                        pathO += "["+perfil[i][1]+","+ perfil[i][0]+"]";
                    else
                        pathO += ",["+perfil[i][1]+","+ perfil[i][0]+"]";
                }
                pathO += "]";


                const body2 = '{"format_in":"polyline","format_out":"polyline","dataset":"srtm","geometry":'+pathO+'}';
                consoleLOG(body2);
                request.send(body2);
            }
        }
    }
      
    calcularAlturasPorPartes(proveedor, puntero, perfil){
        if (typeof window !== "undefined") {
            var este = this;
            if(proveedor == "google"){
                var limite = puntero + 300;
                if(limite > perfil.length)
                    limite = perfil.length;
                var locations = [];
                for(var i = puntero;i < limite;i++){
                    var latlng = new window.google.maps.LatLng(perfil[i][0], perfil[i][1]);
                    locations.push(latlng);
                }
                var positionalRequest = {
                    'locations': locations
                }
                var elevationService = new window.google.maps.ElevationService();
                elevationService.getElevationForLocations(positionalRequest, function(results, status) {
                    if (status == window.google.maps.ElevationStatus.OK) {
                        //si es el último pintamos y si no llamamos siguiente
                        for(var j=0;j<results.length;j++){
                            resultporpartes.push(results[j]);
                        }

                        if(limite == perfil.length){
                            este.calcularDatos(resultporpartes);
                        }
                        else{
                            este.calcularAlturasPorPartes(proveedor, limite, perfil);
                        }
                    } else {
                        if(status == "OVER_QUERY_LIMIT" || results == null){
                            setTimeout("calcularAlturasPorPartes("+puntero+")", 50);
                        }
                        else{
                            alert("error en el cáculo de elevación.");
                        }
                    }
                });
            }
            else if(proveedor == "arcgis"){
                loadModules(['esri/Map', "esri/geometry/Polyline", "esri/geometry/SpatialReference"])
                .then(([Map, Polyline, SpatialReference]) => {
                    const map = new Map({
                        ground: "world-elevation"
                    });
                    
                    var limite = puntero + 300;
                    if(limite > perfil.length)
                        limite = perfil.length;

                    var path2 = [];
                    for(var i = puntero; i < limite; i++){
                        path2.push([perfil[i][1], perfil[i][0]]);
                    }
                    

                    const elevationPromise = map.ground.queryElevation(
                        new Polyline({
                            paths: path2,
                            spatialReference: SpatialReference.WGS84
                        })
                    );

                    elevationPromise.then((result) => {
                            consoleLOG("resultados:");
                        const path = result.geometry.paths[0];

                        //ahora tengo en path un array de [lng,lat,alt]

                        for(var i=0;i<path.length;i++){
                            var latlng = new window.google.maps.LatLng(path[i][1], path[i][0]);

                            resultporpartes.push({elevation:path[i][2], location:latlng});
                        }

                        if(limite == perfil.length){
                            this.calcularDatos(resultporpartes);
                        }
                        else{
                            this.calcularAlturasPorPartes(proveedor, limite, perfil);
                        }
                    });
                });
            }
            else if(proveedor == "openrouteservice"){
                let request = new XMLHttpRequest();
                request.open('POST', "https://api.openrouteservice.org/elevation/line");            
                request.setRequestHeader('Accept', 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8');
                request.setRequestHeader('Content-Type', 'application/json');
                request.setRequestHeader('Authorization', apiKey_openroute);
                request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    let response = JSON.parse(this.response);
                    consoleLOG("respuesta")
                    consoleLOG(response);
                    var path = response.geometry;
                    //consoleLOG("encoded " + g);
                    //var path = decodePolylineElevation(g, true);
                    consoleLOG(path)
                    for(var i=0;i<path.length;i++){
                        var latlng = new window.google.maps.LatLng(path[i][1], path[i][0]);
                        resultporpartes.push({elevation:path[i][2], location:latlng});
                    }

                    if(limite == perfil.length){
                        este.calcularDatos(resultporpartes);
                    }
                    else{
                        este.calcularAlturasPorPartes(proveedor, limite, perfil)
                    }
                    
                }
                };

                var limite = puntero + 300;
                if(limite > perfil.length)
                    limite = perfil.length;

                //consoleLOG("vamos a calcular desde " + puntero + " a "+limite)
                var pathO = "[";
                for(var i = puntero; i < limite; i++){
                    if(i== puntero)
                        pathO += "["+perfil[i][1]+","+ perfil[i][0]+"]";
                    else
                        pathO += ",["+perfil[i][1]+","+ perfil[i][0]+"]";
                }
                pathO += "]";


                const body2 = '{"format_in":"polyline","format_out":"polyline","dataset":"srtm","geometry":'+pathO+'}';
                consoleLOG("petición" + body2);
                request.send(body2);
            }
        }
    }

    calcularDatos(elevations){
        //ya están calculadas las elevaciones, hay que enviárselas a la base de datos para que las guarde, y luego llamamos al download.php con la opción 4 y lo descargará ok
        var encodedPerfilElevation = calcularEncodedElevationsRuta(elevations);
        var params2 ={
            'rutaID':rutaID
        }
        //params2.perfilLocations = encodedPerfilElevation[0];
        params2.elevations = encodedPerfilElevation[1];
        consoleLOG("params2")
        consoleLOG(params2)
        const self = this;
        
        (async () => {
            let data3 = GetParamsUserAgent();
            const rawResponse = await fetch(urlWS+'ruta/addelevations.php'+data3, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
              },
              body: JSON.stringify(params2)
            });
            const response = await rawResponse.json();    
            consoleLOG("response");
            consoleLOG(response);
            if(response.id && parseInt(response.id) > 0){
                var id = response.id;
                window.open(urlWS+"ruta/download.php?id="+rutaID+"&format="+this.state.formato+"&option="+opcionActual, "_blank");   
                consoleLOG(urlWS+"ruta/download.php?id="+rutaID+"&format="+this.state.formato+"&option="+opcionActual, "_blank");   
            }
            else{
                alert(lang.error_route_created);
            }
            
            self.setState((state) => {
                // Importante: lee `state` en vez de `this.state` al actualizar.
                return {loading:false, modalDescargarIsOpen:false, flag: this.state.flag+1}
            });
                     
          })();
      
    }

    Editar(){
        window.open(window.location.origin+"/"+lang.lang+"/planner/?id="+rutaID);
    }

    convertToParamasComent(objeto){ //operations
        return {
            'rating':objeto.rating,
            'comment':objeto.comment,
            'userID':user.userID
        }
     }

    

    mySubmitHandlerComment = (event) => {
        event.preventDefault();
        
        if(this.state.rating < 1){
            alert(lang.no_rating);
            return;
        }
        
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {enviado:true, loading:true}
        });
       
        

        var rutaID = this.state.rutaID;
        //aquí debemos de llamar al ws para crear el local y pasar this.state
        var params = this.convertToParamasComent(this.state);
        const self = this;
          
        
        (async () => {
            let data = GetParamsUserAgent();
            const rawResponse = await fetch(urlWS+'ruta/addcomment.php'+data+'&rutaID='+rutaID, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
              },
              body: JSON.stringify(params)
            });
            
            const response = await rawResponse.json();          
            consoleLOG(response)
            var id = response.id;
            
            if(id > 0){
                clase = "success";
                
                var comentarios = self.state.datosRuta.comments;
                consoleLOG(comentarios);
                var date = new Date();
                var dateStr =date.getFullYear() + "-" +("00" + (date.getMonth() + 1)).slice(-2) + "-" +
                    ("00" + date.getDate()).slice(-2) + " " +
                    ("00" + date.getHours()).slice(-2) + ":" +
                    ("00" + date.getMinutes()).slice(-2) + ":" +
                    ("00" + date.getSeconds()).slice(-2);

                comentarios.push({
                    'rating':self.state.rating,
                    'comment':self.state.comment,
                    'userID':user.userID,
                    'username':user.username,
                    'created':dateStr
                });

                consoleLOG(comentarios);
                
                self.setState((state) => {
                    // Importante: lee `state` en vez de `this.state` al actualizar.
                    return {enviado:true,'rating':-1,'comment':'', loading:false, flag:this.state.flag+1}
                });
            }
            else{
                clase = "danger";
                self.setState((state) => {
                    // Importante: lee `state` en vez de `this.state` al actualizar.
                    return {enviado:true, loading:false}
                });
            }
            
        })();
    }

    

    myClickStar(n) {
        var self= this;
        return function () {
            self.setState({rating:n, flag:self.state.flag+1})
         }
    }

    toggleShowCompartirModal(){
        this.setState({modalCompartirIsOpen: !this.state.modalCompartirIsOpen, flag: this.state.flag+1})
    }
    
    share(){
        this.setState({modalCompartirIsOpen: !this.state.modalCompartirIsOpen, flag: this.state.flag+1});
    }

    ShowDescargar(format){
        this.setState({modalDescargarIsOpen: !this.state.modalDescargarIsOpen, formato:format, flag: this.state.flag+1});
    }

    toggleShowDescargarModal(){
        this.setState({modalDescargarIsOpen: !this.state.modalDescargarIsOpen, flag: this.state.flag+1})
    }

    render() {
        
        
        
        
            
        if(this.state.loading == true){
            return(
            <div className="loading_center">
                <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-secondary" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-success" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-danger" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-warning" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-info" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-light" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-dark" role="status">
                <span className="sr-only">Loading...</span>
                </div>
            </div>
            );
        
        }else if(this.state.completed && !this.state.resultado){
            
            return(
                <div className="loading_center">
                    <div className="alert alert-danger">
                        <div>{lang.error_route_created}</div>                 
                        <Link href={"./"}><a>{lang.home}</a></Link>       
                        <Link href={"/ruta/"+params.rutaID}><a>{lang.goruta}</a></Link>
                    </div>
                </div>
                );
                
        }
        else{

            consoleLOG("render main");
            
            const {error, loading } = this.state;
            if (error) {
                return <div>Error: {error.message}</div>;
            } 
            
            
                   
            consoleLOG("se renderiza y el streetview es " + this.state.streetView);

            var urlfondo = "";
            if(this.state.datosRuta.images.length > 0){
                urlfondo=baseurl+this.state.datosRuta.images[0].image;
            }


            var heightmap = "30rem";
            
            
            if(window.innerWidth < 768){
                clasemapa = "h-500";
                heightmap = "200px";
            }else{
                if(ref.current){
                    heightmap =ref.current.clientHeight;
                }
    
                if(this.state.polylines.length < 4){
                    clasemapa = "h-600";
                }
    
                if(this.state.images <= 0){
                    clasemapa = "h-100";
                }
            }

       

            var usernameRuta = this.state.datosRuta.username;
            var titulo = this.state.datosRuta.titulo;
            var numratings = this.state.datosRuta.numRatings;
            var rating = this.state.datosRuta.rating;
            var comments = this.state.datosRuta.comments;

            var percentSurfaces = this.state.datosRuta.percentSurfaces;
            var percentWaytypes = this.state.datosRuta.percentWaytypes;

            var mostrarTerrenoBajoPerfil = true;

            var surfacePrintPerfil  = this.state.html_tramosJOIN && mostrarTerrenoBajoPerfil && HasSurfaces(percentSurfaces);


            var link = "https://lovelo.app/"+lang.lang+"/route/"+ urlRUTA;
            var linkiframe = "https://lovelo.app/"+lang.lang+"/iframe/"+ rutaID;
            var iframe = '<iframe src="'+linkiframe+'" width="100%" height="455" scrolling="auto" transparency frameborder="0"/>'

            return (        
                <div className="h-100">
                    
                    <div className="container" lang={lang.lang} style={{background:"white"}}>                                  
                        
                        
                        {loading &&
                            <div className="loading_center h-100-50">
                                <div className="spinner-grow text-primary" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-secondary" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-success" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-danger" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-warning" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-info" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-light" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-dark" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                            </div>
                        } 
                        <div className="row-fluid">
                            <div className="tituloruta">
                                <div className="nombre text-left">{this.state.datosRuta.titulo}</div>
                                <div className="row subnombre">
                                    {tipo_texto &&
                                        <div className="col-lg-2 col-6 d-flex"><img src={tipo_icono.src}/><span className="d-inline-block">{tipo_texto}</span></div>
                                    }
                                    {bicicleta_icono &&
                                        <div className="col-lg-2 col-6 d-flex"><img src={bicicleta_icono.src}/><span className="d-inline-block">{bicicleta_texto}</span></div>
                                    }
                                    {actividad_icono &&
                                        <div className="col-lg-2 col-6 d-flex"><img src={actividad_icono.src}/><span className="d-inline-block">{actividad_texto}</span></div>
                                    }
                                    <div className="col-lg-3 col-6 d-flex subnombreuser"><FontAwesomeIcon icon={faUser} aria-hidden="true" size="md" className="d-inline-block"/><span className="d-inline-block">{usernameRuta}</span></div>
                                    <div className="col-lg-3 col-6 d-flex">
                                        <div className="row">
                                            <div className="starsComment">
                                                        <span><FontAwesomeIcon color={rating > 0 ? colorhex : '#bbb'} icon={faStar}/></span>
                                                        <span><FontAwesomeIcon color={rating > 1 ? colorhex : '#bbb'} icon={faStar}/></span>
                                                        <span><FontAwesomeIcon color={rating > 2 ? colorhex : '#bbb'} icon={faStar}/></span>
                                                        <span><FontAwesomeIcon color={rating > 3 ? colorhex : '#bbb'} icon={faStar}/></span>
                                                        <span><FontAwesomeIcon color={rating > 4 ? colorhex : '#bbb'} icon={faStar}/></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div className="row">
                        
                            <div className={"col-lg-7 col-12"}>
                                
                                <ButterToast timeout={100000} position={{vertical: POS_TOP,  horizontal: POS_CENTER }} style={{ top: '100px',  right: '100px' }}/>
                                <div className="map" style={{height:heightmap}}>
                                        <div id="pano" className={this.state.streetView + " streetview"} style={{position:'absolute', bottom:0, height: '50%', width:'100%', zIndex:'20000000000000000000000'}}>
                                            
                                        </div>                                
                                        
                                        <ModalCompartir link={link} iframe={iframe} lang={lang} abierto={this.state.modalCompartirIsOpen} toggleShowModalCallback={this.toggleShowCompartirModal.bind(this)}>
                                        </ModalCompartir>

                                        <ModalDescargar formato={this.state.formato} lang={lang} abierto={this.state.modalDescargarIsOpen} toggleShowModalCallback={this.toggleShowDescargarModal.bind(this)} Descargar={this.Descargar.bind(this)}>
                                        </ModalDescargar>
                                  
                                    {consoleLOG("se muestra el mapa")}
                                    <MapContainer bounds={bounds} zoomControl={false} scrollWheelZoom={true} className={clasemapa} whenCreated={(map) => this.setState({ map, flag: this.state.flag+1 })}>
                                        <StreetViewControl
                                                title={"Street View"}
                                                handeDesactivateStreetView= {this.handeDesactivateStreetView.bind(this)} handeClickStreetView={this.handeClickStreetView.bind(this)}
                                                streetView= {this.state.streetView} lang={lang}/>
                                        <LayersControl position="topright">
                                            <LayersControl.BaseLayer checked name="Gmaps">
                                                <TileLayer
                                                attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                                url='https://mt1.google.com/vt/lyrs=r&x={x}&y={y}&z={z}'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="Gmaps Satellite">
                                                <TileLayer
                                                attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                                url='https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="OpenStreetMap">
                                                <TileLayer
                                                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="Esri">
                                                <TileLayer
                                                attribution='Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
                                                url='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="ArcGIS">
                                                <TileLayer
                                                attribution='ArcGIS Streets'
                                                url='http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="MapBox">
                                                <TileLayer       
                                                attribution='© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>'                             
                                                maxZoom='24'
                                                id='mapbox/streets-v11'
                                                accessToken='pk.eyJ1IjoiaGlnaW5pbyIsImEiOiJjanN5bXR1OXAwZ3dlNDRyMnk3Mzc0emY0In0.w19t4Sn9OjLjr9qaXFfkcQ'
                                                url={"https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token="+apikey_mapbox}
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="OpenCycleMap">
                                                <TileLayer
                                                attribution='&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                                url='https://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey={apikey}'
                                                apikey='569b8242922348a2a54db0b454ef67ae'
                                                />
                                            </LayersControl.BaseLayer>
                                            <LayersControl.BaseLayer name="IGN">
                                                <WMSTileLayer
                                                    attribution='&copy; <a href="https://www.ign.es/web/ign/portal">IGN</a>'
                                                    projection= 'EPSG:3857'    
                                                    url="https://tms-mapa-raster.ign.es/1.0.0/mapa-raster/{z}/{x}/{-y}.jpeg"
                                                />
                                            </LayersControl.BaseLayer>

                                           
                                            <LayersControl.Overlay name="routes">
                                                <TileLayer
                                                attribution='Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | Map style: &copy; <a href="https://waymarkedtrails.org">waymarkedtrails.org</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
                                                url='https://tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png'
                                                opacity='0.5'
                                                />
                                            </LayersControl.Overlay>
                                            <LayersControl.Overlay name="googleroads">
                                                <TileLayer
                                                attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                                url='https://mt1.google.com/vt/lyrs=h&x={x}&y={y}&z={z}'
                                                opacity='0.5'
                                                />
                                            </LayersControl.Overlay>
                                            <LayersControl.Overlay name="catastro">
                                                <WMSTileLayer
                                                attribution='<a target="_BLANK" href="https://www.google.es/maps/preview">Google Maps</a>'
                                                url="https://ovc.catastro.meh.es/cartografia/INSPIRE/spadgcwms.aspx"
                                                opacity='0.4'
                                                layers={'CP.CadastralParcel'}
                                                />
                                            </LayersControl.Overlay>
                                            <LayersControl.Overlay name="P.National">
                                                <WMSTileLayer
                                                attribution='Ministerio para la Transici&oacute;n Ecol&oacute;gica y el Reto Demogr&aacute;fico'
                                                url='http://sigred.oapn.es/geoserverOAPN/LimitesParquesNacionalesZPP/ows?'
                                                opacity='0.4'
                                                layers={'view_red_oapn_limite_pn'}
                                                format= {'image/png'}
                                                />
                                            </LayersControl.Overlay>

                                        
                                            {this.state.tracks.map((track, idy) => {
                                                return(      
                                                    <LayersControl.Overlay key={`lc-${idy}`}  checked name={track.properties.name}>
                                                        <LayerGroup>    
                                                        <GeoJSON style={TracksOptions} key={'track-'+track.properties.key} data={track}>
                                                            </GeoJSON>
                                                        </LayerGroup>
                                                    </LayersControl.Overlay>
                                                )
                                            })}
                                        </LayersControl> 
                                        <ZoomControl position="topright" zoomInText="+" zoomOutText="-" />
                                        

                                        {this.state.mostrarLayers &&
                                            this.state.kms.map((km, idx) => {
                                                //consoleLOG(marker)
                                                return (
                                                    <Marker
                                                        key={`km-${idx}`} 
                                                        ref={`km-${idx}`} 
                                                        position={km}
                                                        draggable={false}
                                                        icon={IconKm(idx+1)}>
                                                    </Marker>
                                                )
                                            })
                                            
                                        }
                                        {                                             
                                            this.state.markers.map((marker, idx) => {
                                                //consoleLOG(marker)
                                                var iconi;
                                                if(idx == 0){
                                                    iconi = CreateIconLeaflet("circleini",10);
                                                }
                                                else{
                                                    iconi = CreateIconLeaflet("circlefin",10);
                                                }
                                                return (
                                                    <Marker
                                                        key={`marker2-${idx}`} 
                                                        ref={`marker2-${idx}`} 
                                                        position={marker}
                                                        draggable={false}
                                                        icon={iconi}>
                                                    </Marker>
                                                )
                                            })
                                            
                                        }
                                        

                                        

                                        {this.state.polylines.map((polyl, idy) => {
                                            //consoleLOG("GEOOOO PINTA")
                                            //consoleLOG(polyl);
                                            return(                                         
                                            <GeoJSON ref={this.btnRef} style={GeoJsonOptions} eventHandlers={{
                                                click: (e) => {
                                                this.clickpolyline(e, idy);
                                                } 
                                            }} key={'geojson-'+polyl.key} data={polyl.pathSurfaces}>
                                            
                                            <Popup keepInView={true} className="PopupPolyline">
                                                <ModoPolyline distance={polyl.distance} id={idy} travelMode={polyl.travelMode} lang={lang} directions_prov={polyl.provider} mode="show"></ModoPolyline>    
                                            </Popup>
                                            
                                            </GeoJSON>
                                            )
                                        })}
                                    
                                    </MapContainer>
                                </div>
                        </div>
                        

                        <div id="derecha" className={"col-lg-5 col-12 text-center"} ref={ref}>
                                <div className="container-fluid ">
                                    <hr className="hrnomargin"></hr>
                                    <div className="row mt-3">
                                        <div className={"col-3 botonruta nopadding "}  onClick={() => this.ShowDescargar("gpx")}> 
                                            <div>
                                                <span className="dato-icon2"><FontAwesomeIcon icon={faDownload} aria-hidden="true"  size="lg" /></span>
                                            </div>
                                            
                                            <div>                                        
                                                <span className="dato-text">GPX</span>
                                            </div>
                                        </div>
                                        <div className={"col-3 botonruta nopadding "}  onClick={() => this.ShowDescargar("kml")}> 
                                            <div>
                                                <span className="dato-icon2"><FontAwesomeIcon icon={faDownload} aria-hidden="true"  size="lg" /></span>
                                            </div>
                                            
                                            <div>                                        
                                                <span className="dato-text">KML</span>
                                            </div>
                                        </div>
                                        
                                        <div className={"col-3  botonruta nopadding "}  > 
                                            <Link href={"/planner/?id="+rutaID}>
                                                <a>
                                                <div>
                                                    <span className="dato-icon2"><FontAwesomeIcon icon={faPencilAlt} aria-hidden="true"  size="lg" /></span>
                                                </div>
                                                
                                                <div>                                        
                                                    <span className="dato-text">{lang.edit}</span>
                                                </div>
                                                </a>
                                            </Link>
                                        </div>
                                        <div className={"col-3 botonruta nopadding "} onClick={()=> this.share()}> 
                                            <div>
                                                <span className="dato-icon2"><FontAwesomeIcon icon={faShare} aria-hidden="true"  size="lg" /></span>
                                            </div>
                                            
                                            <div>                                        
                                                <span className="dato-text">{lang.share}</span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <hr></hr>
                                    
                                    
                                    <Datos page="ruta" lang={lang} estado={this.state} reset={()=>this.reset()}/>  
                                    
                                    {(this.state.datosRuta.ciclabilidad || this.state.datosRuta.dificultad) &&
                                        <>
                                            <hr></hr>
                                            
                                            <div className="row dat">
                                                {this.state.datosRuta.dificultad &&
                                                    <div className='col'>
                                                        <span className='dato-text2'>{lang.dificultad}</span>: <span className='dato-valor'>{lang.dificultad_vals[this.state.datosRuta.dificultad]}</span>
                                                    </div>
                                                }
                                                {this.state.datosRuta.ciclabilidad &&
                                                    <div className='col'>
                                                        <span className='dato-text2'>{lang.ciclabilidad}</span>: <span className='dato-valor'>{this.state.datosRuta.ciclabilidad+"%"}</span>
                                                    </div>
                                                }
                                            </div>
                                        </>

                                    }
                                    
                                    {HasSurfaces(percentSurfaces) && <hr></hr>}
                                    
                                    
                                    {this.state.polylines.length > 0 && this.state.tramosOterreno == 0 &&
                                        <div className="form-group tramos" style={{maxHeight:'10rem', overflowY:"scroll" }}>
                                                <div className="btn-group" role="group" aria-label="Basic outlined example">
                                                    <button type="button" onClick={()=>this.TramoOTerreno(1)} className="btn btn-outline-primary">{lang.terreno}</button>
                                                    <button type="button" onClick={()=>this.TramoOTerreno(2)} className="btn btn-outline-primary">{lang.waytypes}</button>
                                                    <button type="button" onClick={()=>this.TramoOTerreno(0)} className="btn btn-outline-primary active">{lang.stretchs}</button>
                                                </div>
                                                <div className="container-fluid text-center justify-content-center">
                                                    {this.state.polylines.map((polyline, idy) => {
                                                        consoleLOG("polyline"+ idy)
                                                        return(
                                                            <Tramos key={"t"+idy} lang={lang} polyline={polyline} idy={idy} showToastLegend={this.showToastLegend.bind(this)} showToastTramo={this.showToastTramo.bind(this)}/>
                                                        )
                                                    })}
                                                </div>
                                        </div>
                                    }
                                    {this.state.html_tramosJOIN && HasSurfaces(percentSurfaces) && this.state.tramosOterreno == 1 &&
                                        
                                        <div className="form-group tramos">
                                            <div className="btn-group" role="group" aria-label="Basic outlined example">
                                                <button type="button" onClick={()=>this.TramoOTerreno(1)} className="btn btn-outline-primary active">{lang.terreno}</button>
                                                <button type="button" onClick={()=>this.TramoOTerreno(2)} className="btn btn-outline-primary">{lang.waytypes}</button>
                                                <button type="button" onClick={()=>this.TramoOTerreno(0)} className="btn btn-outline-primary">{lang.stretchs}</button>
                                            </div>
                                            
                                            <div className='row'>
                                            {
                                                
                                                Object.keys(percentSurfaces).map(key => 
                                                    {
                                                        var val = FormatPercent(percentSurfaces[key][0]);
                                                        //esto elimina que haya datos con 0%
                                                        if(val){
                                                            var [text, color] = GerSurfaceTextAndColorByCode(key);
                                                            if(percentSurfaces[key][0] == 100)
                                                                mostrarTerrenoBajoPerfil = false;
                                                            return <div  className='surfacePercent col-lg-6 col-12' onClick={() =>this.showToastLegend()}> 
                                                                <div style={{backgroundColor:color, cursor:'help'}}>
                                                                    {FormatPercent(percentSurfaces[key][0]) + "%"}
                                                                </div>
                                                                <label >{lang[text]}</label>                                                            
                                                                <span>{(percentSurfaces[key][1]/1000).toFixed(1)+ " km"}</span>
                                                            </div>
                                                            }
                                                        }
                                                    )
                                                }
                                            </div>
                                        </div>                                
                                        
                                    }
                                    {this.state.html_tramosJOIN && HasSurfaces(percentWaytypes) && this.state.tramosOterreno == 2 &&
                                                    <div className="form-group tramos">
                                                        <div className="btn-group" role="group" aria-label="Basic outlined example">
                                                            <button type="button" onClick={()=>this.TramoOTerreno(1)} className="btn btn-outline-primary">{lang.terreno}</button>
                                                            <button type="button" onClick={()=>this.TramoOTerreno(2)} className="btn btn-outline-primary active">{lang.waytypes}</button>
                                                            <button type="button" onClick={()=>this.TramoOTerreno(0)} className="btn btn-outline-primary">{lang.stretchs}</button>
                                                        </div>
                                                        
                                                        <div className='row'>
                                                        {
                                                            
                                                            Object.keys(percentWaytypes).map(key => 
                                                                {
                                                                    var val = FormatPercent(percentWaytypes[key][0]);
                                                                    //esto elimina que haya datos con 0%
                                                                    if(val){
                                                                        var [text, claseBorder] = GetWaytypeTextAndColorByCode(key);
                                                                        

                                                                        return <div onClick={() =>this.showToastLegend()} className='surfacePercent col-lg-6 col-12'>
                                                                            <label className='DatoWaytype' style={{border:"2px "+claseBorder, cursor:'help'}}></label>
                                                                            <label>{val + "%"}</label>
                                                                            <label >{lang[text]}</label>                                                            
                                                                            <span>{(percentWaytypes[key][1]/1000).toFixed(1)+ " km"}</span>
                                                                        </div>
                                                                    }
                                                                }
                                                            )
                                                        }
                                                        </div>
                                                    </div> 
                                    }

                                    

                                    <hr></hr>
                                        
                                    {this.state.polylines.length == 1 && !HasSurfaces(percentWaytypes) &&
                                             <div style={{height:'50px'}}></div>
                                             /*Esto es para cuando no hay tramos ni nada*/
                                    }

                                    
                                    <div className="row">
                                        {this.state.images.length > 0 &&
                                            <ImageGallery items={this.state.images} showPlayButton={false}/>
                                        }
                                    </div>
                                    

                                </div>
                                
                                

                                
                            </div>

                        </div>

                        <div className="row mt-5 cajablanca">
                                { this.state.openPerfil && this.state.datosRuta.elevations.length > 0 &&
                                    <div id="" className="col-12" onMouseLeave={()=>this.HandleMouseOutChart()} >
                                        <Perfil titulo={" - " + titulo} polylines={this.state.polylines} surfacePrintPerfil={surfacePrintPerfil} page="ruta" optionsPerfil={this.state.optionsPerfil} repintar={this.state.repintarPerfil} EditarPerfil={this.EditarPerfil.bind(this)} CerrarPerfil={this.CerrarPerfil.bind(this)} HandleMouseOverChart={this.HandleMouseOverChart.bind(this)} HandleClickChart={this.HandleClickChart.bind(this)} datos = {this.state.datosRuta} lang={lang}/>
                                    </div>
                                }
                                    {consoleLOG(percentSurfaces)}
                                    
                                    {this.state.html_tramosJOIN && mostrarTerrenoBajoPerfil && HasSurfaces(percentSurfaces) &&
                                        <div className="col-12">
                                                <div className="progress col-12 tramoJoin" onClick={() =>this.showToastLegend()}>
                                                {this.state.html_tramosJOIN}
                                                </div>

                                                
                                        </div>

                                    }
                        </div>             
                        
                        <div className="row mt-4">
                            <div className="col-12">
                                <div className="comentarios">
                                    <div className="row">
                                        <div className="commenttitle col-12">{lang.descripcion}</div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="description">                                    
                                                <div className="commentContent">    
                                                    <div className="row">                                
                                                        {ntobr(this.state.datosRuta.descripcion)}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>                                        
                                </div>          
                            </div>

                            <div className="col-12 mt-4">              
                                <div className="comentarios">
                                    <div className="row">
                                        <div className="commenttitle col-12">{numratings + " "+lang.comments}</div>
                                    </div>
                                    
                                    <div className="row">
                                        {comments.map((comment) => {
                                                    return <Comment
                                                    key={comment.rutaCommentID}
                                                    comment={comment}
                                                    lang={lang}
                                                    color={colorhex}>
                                                    </Comment>                                            
                                        })}
                                    
                                    </div>
                                    
                                    {this.state.enviado &&
                                        <div className={"mt-5 alert alert-"+clase}>
                                            <strong>
                                            {clase === "dafault" &&
                                                lang.sending
                                            }
                                            {clase === "danger" &&
                                                lang.comment_error_title
                                            }
                                            {clase === "success" &&
                                                lang.comment_sent_title
                                            }
                                            </strong>
                                            {clase === "danger" &&
                                                lang.comment_error
                                            }
                                            {clase === "success" &&
                                                lang.comment_sent
                                            }
                                        
                                        </div>
                                    }

                                    {user !== null && 
                                        <div className="row">
                                            <form className="well form-horizontal sendrating mt-5 col-12" onSubmit={this.mySubmitHandlerComment}  id="contact_form">
                                                <div className="form-group row">
                                                    <div className="col-12 userComment">{user.username}</div>
                                                    <div className="col-12">
                                                        <FontAwesomeIcon color={this.state.rating > 0 ? colorhex : '#bbb'} icon={faStar} onClick={this.myClickStar(1)}/>
                                                        <FontAwesomeIcon color={this.state.rating > 1 ? colorhex : '#bbb'} icon={faStar} onClick={this.myClickStar(2)}/>
                                                        <FontAwesomeIcon color={this.state.rating > 2 ? colorhex : '#bbb'} icon={faStar} onClick={this.myClickStar(3)}/>
                                                        <FontAwesomeIcon color={this.state.rating > 3 ? colorhex : '#bbb'} icon={faStar} onClick={this.myClickStar(4)}/>
                                                        <FontAwesomeIcon color={this.state.rating > 4 ? colorhex : '#bbb'} icon={faStar} onClick={this.myClickStar(5)}/>
                                                    </div>
                                                </div>

                                                <div className="form-group row">
                                                <div className="col"><textarea rows="4" name="comment" onChange={this.myChangeHandler} className="form-control comentariotextarea" placeholder={lang.comment_placeholder}></textarea>
                                                    </div>
                                                </div>

                                                <div className="form-group row mb-5">
                                                    <div className="col-6"><button type="submit" className="btn btn-secondary btn-sm" >{lang.submit.toUpperCase()}</button>
                                                    </div>
                                                </div>
                                                
                                                
                                            </form>
                                        </div>
                                    }
                                    {!user && 
                                        //añadir url de la cerveza para que no la tenga que volver a buscar
                                        <div className="mt-4 mb-4">
                                            <Link href={"/login?href=route/"+urlRUTA}><a>{capitalize(lang.login_comment)}</a></Link>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                            
                    </div>
                </div>
            );
        }
        
    }
};

export default Ruta;