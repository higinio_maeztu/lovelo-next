import {capitalize,urlWS,GetIconRouteType, GetIconRouteActivity, baseurl, ntobr, brton} from "../utils/constantes";
import {consoleLOG, GetParamsUserAgent, URLamigable } from '../utils/ifelse.js'
import React from "react";
import { Component } from "react";
import ResizableTextarea from './extras/resizetextarea'
import OpcionRuta from './main/opcionruta'
import 'react-gallery-carousel/dist/index.css';
import roadBike from '../public/images/road.png'
import cicloturismo from '../public/images/cicloturismo.png'
import mtbBike from '../public/images/mtb.png'
import MultipleImageUploadComponent from './extras/multiple-image-upload.component';
import Router from 'next/router'
import Link from 'next/router'

var rutaID,urlRUTA,lang,user,userID=null;
class EditRuta extends Component {
    
    constructor(props){
        super(props);
        var ruta = props.ruta;
        rutaID = ruta.rutaID;
        urlRUTA = URLamigable(ruta.titulo)+"_"+rutaID;
        lang = props.lang;
        user = props.user;


        if(user.userID)
            userID = user.userID;
        else
            userID = -1;

        
        this.handleStateChangeLP  = this.handleStateChangeLP.bind(this);
        
        var isPropietario = false;
        if(ruta.userID == userID){
            isPropietario = true;
        }

        var ararryID = [];
        ruta.images.map((image) => {
            ararryID.push(image.rutaImageID);
        })

        this.state ={
            rutaID:ruta.rutaID,
            arrayLocalPhotos:[],
            resultado:true,
            titulo:ruta.titulo,
            descripcion:ruta.descripcion,
            tipo:ruta.tipo,
            bicicleta:ruta.bicicleta,
            actividad:ruta.actividad,
            dificultad:ruta.dificultad,
            ciclabilidad:ruta.ciclabilidad,
            images:ruta.images,
            imagesID:ararryID,
            userID:ruta.userID,
            isPropietario:isPropietario,
            loading: false
        }
       
        
    }

    convertToParamas(objeto){ //operations
        return {
         'titulo':objeto.titulo,
         'descripcion':objeto.descripcion,
         'tipo':objeto.tipo,
         'bicicleta':objeto.bicicleta,
         'actividad':objeto.actividad,
         'dificultad':objeto.dificultad,
         'ciclabilidad':objeto.ciclabilidad,
         'images':objeto.imagesID,
         'rutaID':objeto.rutaID
        }
     }
  
     mySubmitHandler = (event) => {
         event.preventDefault();
         //aquí debemos de llamar al ws para crear el local y pasar this.state
         var params = this.convertToParamas(this.state);
         consoleLOG(params);
         const self = this;
         self.setState({ 
             loading:true
         });  
         
         (async () => {
             let data2 = GetParamsUserAgent();
             const rawResponse = await fetch(urlWS+'ruta/update.php'+data2, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json',
                 'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
               },
               body: JSON.stringify(params)
             });
             const response = await rawResponse.json();          
             var id = response.id;
             var message = lang.error_route_created;
             if(parseInt(id) > 0){
                 message = lang.ruta_created;
                 //////////////////////////////////////////////////////LOCAL IMAGES//////////////////////////////////
                 let data = new FormData();
                 let numimages = 0;
                 for (let i = 0 ; i < this.state.arrayLocalPhotos.length ; i++) {   
                     if(this.state.arrayLocalPhotos[i]){
                         data.append('files[]', this.state.arrayLocalPhotos[i], this.state.arrayLocalPhotos[i].name);
                         numimages++;
                     }
                 }
                 data.append('userID', userID);
                 consoleLOG("añadimos imagenes nuevas")
                 consoleLOG(data);
                 consoleLOG(numimages)
                 if(numimages > 0){
                     (async () => {
                         const rawResponse = await fetch(urlWS+'ruta/addphotos.php'+data2+'&id='+id, {
                           method: 'POST',
                           body: data
                         });
                         const response = await rawResponse.json();
                         self.setState({ 
                             loading:false,
                             completed:true,
                             resultado:true
                         });  
                         
                       })();
                 }
                 else{
                     self.setState({ 
                         loading:false,
                         completed:true,
                         resultado:true
                     });  
                 }
                 //////////////////////////////////////////////////////FIN LOCAL IMAGES//////////////////////////////////
 
             }else{                
                 self.setState({ 
                     loading:false,
                     completed:true,
                     resultado:true
                 });  
             }            
           })();
       }
 
       removePhoto(id){
             var imagesID = [];
             var arrayI = [];
             this.state.images.map((image) => {
                 if(image.rutaImageID != id){
                     arrayI.push(image);
                     imagesID.push(image.rutaImageID);
                 }
             })
             consoleLOG(this.state.images);
             consoleLOG(arrayI);
             this.setState({images:arrayI, imagesID:imagesID});
       }
 
     myChangeHandler = (event) => {
         let nam = event.target.name;
         let val = event.target.value;
         this.setState({[nam]: val});
       }
 
     myClickHandler(object){
         var props = object.props;
         var nam = props.campo;
         var val = props.valor;
         this.setState({[nam]: val});
     }
 
 
     handleStateChangeLP(files) {
         this.setState({arrayLocalPhotos:files})
     }
 
     Disabled(field){
         if(this.state.isPropietario)
             return false;
         else
             return true;
     }
 


    render() {
        
        if(this.state.loading == true){
            return(
            <div className="loading_center">
                <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-secondary" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-success" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-danger" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-warning" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-info" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-light" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-dark" role="status">
                <span className="sr-only">Loading...</span>
                </div>
            </div>
            );
        
        }else if(!this.state.isPropietario){
            return(
                <div className="row nopadding h-100-50 text-center justify-content-center login" >
                    <div className="alert-warning my-auto p-5">
                            <div className="">
                                <div>{capitalize(lang.no_permiso)}</div>                        
                            </div>
                            <div className="text-center margintop">
                                <Link align="center" href="/" className="alert-link"><a>{lang.home}</a></Link>
                                <Link style={{marginLeft:"2rem"}} align="center" href={"/"+lang.lang+"/route/"+urlRUTA} className="alert-link"><a>{lang.goruta}</a></Link>
                            </div>            
                    </div>
                </div>
            );
        }
        else if(this.state.completed){
            if(this.state.resultado){
                Router.push("/"+lang.lang+"/route/"+urlRUTA);
                return null;
            }
            else{
                return(
                    <div className="loading_center">
                        <div className="alert alert-danger">
                            <div>{lang.error_route_created}</div>                 
                            <Link to={"./"}>{lang.home}</Link>       
                            <Link to={"/ruta/"+urlRUTA}>{lang.goruta}</Link>
                        </div>
                    </div>
                    );
                }
        }
        else{
            return (
                <div id="edit" className="col-lg-6 col-12 mx-auto form p-4">
                                    <h1 className="display-5 py-4 text-center">{capitalize(lang.edit_ruta)} - {this.state.titulo}</h1>
                                    
                                    <div className="px-2">
                                        
                                        <form onSubmit={this.mySubmitHandler} className="justify-content-center">
                                            <div className="form-group">
                                                <label>{capitalize(lang.titulo)}</label>
                                                <input required onChange={this.myChangeHandler} value={this.state.titulo} name="titulo" type="text" className="form-control" />
                                            </div>

                                            <div className="form-group">
                                                <label>{capitalize(lang.descripcion)}</label>
                                                <ResizableTextarea required myChangeHandler={this.myChangeHandler} value={this.state.descripcion} name="descripcion" type="text" className="form-control" placeholder={lang.desc_placeholder_edit}/>
                                            </div>

                                            <div className="form-group">
                                                <label>{capitalize(lang.dificultad)}: </label>
                                                {this.state.dificultad &&
                                                    <label className="resalta">{lang.dificultad_vals[this.state.dificultad]}</label>
                                                }
                                                <input required onChange={this.myChangeHandler} value={this.state.dificultad} name="dificultad" min="1" max="5" type="range" className="form-control slider" />
                                            </div>

                                            <div className="form-group">
                                                <label>{capitalize(lang.ciclabilidad)}: </label>
                                                {this.state.ciclabilidad &&
                                                    <label className="resalta">{this.state.ciclabilidad + "%"}</label>
                                                }
                                                <input required onChange={this.myChangeHandler} value={this.state.ciclabilidad} name="ciclabilidad" min="0" max="100" type="range" className="form-control-range" />
                                            </div>

                                            <div className="form-group">
                                                <label>{capitalize(lang.bicicleta_t)}</label>
                                                <div className="panel panel-container menuDatos">
                                                    <div className="row nopadding">
                                                        <OpcionRuta icono={mtbBike} myClickHandler={this.myClickHandler.bind(this)} texto={lang.b_mtb} border="border-left border-right" campo="bicicleta" valorActual={this.state.bicicleta} valor= "1"/>
                                                        <OpcionRuta icono={cicloturismo} myClickHandler={this.myClickHandler.bind(this)} texto={lang.b_gravel} border="border-left" campo="bicicleta" valorActual={this.state.bicicleta} valor= "2"/>
                                                        <OpcionRuta icono={roadBike}  myClickHandler={this.myClickHandler.bind(this)} texto={lang.b_carretera} border="border-left" campo="bicicleta" valorActual={this.state.bicicleta} valor= "3"/>
                                                        
                                                    </div>         
                                                </div>
                                            </div>

                                            <div className="form-group">
                                            <label>{capitalize(lang.tipo_t)}</label>
                                                <div className="panel panel-container menuDatos">
                                                    <div className="row nopadding">
                                                        <OpcionRuta icono={GetIconRouteType(1)[0]} myClickHandler={this.myClickHandler.bind(this)} texto={lang.lineal} border="border-left" campo="tipo" valorActual={this.state.tipo} valor= "1"/>
                                                        <OpcionRuta icono={GetIconRouteType(2)[0]} myClickHandler={this.myClickHandler.bind(this)} texto={lang.circular} border="border-left"  campo="tipo" valorActual={this.state.tipo} valor= "2"/>
                                                        <OpcionRuta icono={GetIconRouteType(3)[0]} myClickHandler={this.myClickHandler.bind(this)} texto={lang.puerto} border="border-left border-right"   campo="tipo" valorActual={this.state.tipo} valor= "3"/>
                                                    </div>         
                                                </div>
                                            </div>

                                            <div className="form-group">
                                            <label>{capitalize(lang.activity_t)}</label>
                                                <div className="panel panel-container menuDatos">
                                                    <div className="row nopadding">
                                                        <OpcionRuta icono={GetIconRouteActivity(1)[0]} myClickHandler={this.myClickHandler.bind(this)} texto={lang.rally} border="border-left"   campo="actividad" valorActual={this.state.actividad} valor= "1"/>
                                                        <OpcionRuta icono={GetIconRouteActivity(2)[0]} myClickHandler={this.myClickHandler.bind(this)} texto={lang.prueba_mtb} border="border-left"   campo="actividad" valorActual={this.state.actividad} valor= "2"/>
                                                        <OpcionRuta icono={GetIconRouteActivity(3)[0]} myClickHandler={this.myClickHandler.bind(this)} texto={lang.prueba_carr} border="border-left border-right"  campo="actividad" valorActual={this.state.actividad} valor= "3"/>
                                                    </div>         
                                                </div>
                                            </div>
                                            
                                            {this.state.images.length > 0 &&
                                            <div className="form-group">
                                                <div className="card">
                                                <div className="card-header">
                                                    {capitalize(lang.photos_route)}
                                                </div>
                                                <div className="card-body">
                                                    <form>
                                                        <div className="container row">
                                                        {
                                                        this.state.images.map((image, idy) => {  
                                                        return <div key={idy} className="col-md-3 col-sm-3 col-xs-3 image">
                                                                <label className="file_upload">
                                                                    <a className="link" href="javascript:void(0)"  onClick={ () => this.removePhoto(image.rutaImageID)}>✖</a>
                                                                <img className="imagen2 img_" src={baseurl+image.image} />
                                                                </label>
                                                                </div>                      
                                                        
                                                        })}
                                                        </div>
                                                    </form>
                                                </div>
                                                </div>
                                            </div>
                                            }
                                        
                                            <div className="form-group">
                                                <div className="card">
                                                <div className="card-header">
                                                    {capitalize(lang.add_photos)}
                                                </div>
                                                <div className="card-body">
                                                    <MultipleImageUploadComponent handleStateChangeLP ={this.handleStateChangeLP }/>
                                                </div>
                                                </div>
                                            </div>

                                            

                                            <div className="form-group text-center">
                                                <button type="submit" className="btn btn-danger margintop mx-auto">{capitalize(lang.guardar)}</button>
                                            </div>
                                            <hr></hr>
                                            <div className="forgot-password text-center">
                                                <a href={"/"+lang.lang+"/route/"+urlRUTA}>{capitalize(lang.saltar)}</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
            );
        }
        
        
    }
};

export default EditRuta;