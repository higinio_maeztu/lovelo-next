import React, { Component } from 'react';
import { MapContainer, TileLayer, WMSTileLayer,LayersControl, Marker, ZoomControl, Tooltip, GeoJSON, Popup,LayerGroup} from 'react-leaflet';
import {isMobile
  } from "react-device-detect";
import 'bootstrap/dist/css/bootstrap.min.css';

import { GetUserPosition, getLang, GetParamsUserAgent, consoleLOG, URLamigable } from '../utils/ifelse.js';
import {GetSurfacesChart,marcadoreskms,descomponArrayTramos, decodePolyline, obtptoskilometricos, obtptosintermedios_direct, distanciaTramo,DivivirRuta,CalcPolyBySurfaces, calcularDatosRuta,convertToParamas,calcularEncodedElevationsRuta,CalcPolylinesyTramosFromBD,  formarperfil } from '../utils/matematicas.js'
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import {CreateIconLeaflet,GetWaytypeTextAndColorByCode,GerSurfaceTextAndColorByCode,FormatPercent,HasSurfaces,urlWS, centerdefault, apikey_mapbox, apiKey_openroute, GeoJsonOptions, perfilOptions, TracksOptions, cadadist,IconKm} from "../utils/constantes";
import Herramientas from './main/herramientas';
import Datos from './main/datos';
import Tramos from './main/tramos';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faChartArea} from '@fortawesome/free-solid-svg-icons'
import Perfil from './main/perfil';
import ModalAnnotation from './main/modalannotation';
import ModalPerfil from './main/modalperfil';
import ModalImportar from './main/modalimportar';
import StreetViewControl from './main/streetview';
import SearchControl from './main/leafletgeosearch';
import GPX from 'gpx-parser-builder';

import ButterToast, { Cinnamon,POS_TOP, POS_CENTER  } from 'butter-toast';
import Legend from './main/legend'

import { loadModules } from 'esri-loader';
import ModoPolyline from './main/modopolyline';
//para importar la función add desde otro archivo: import { add } from './math.js';

//esta variable la tengo que generar dinámicamente con el array PATH y los arrays de suface y waytype. Creando una feature por cada tramo distinto. Pero es una única polylinea.
//el color se le dará en función del valor de surface y waytype

import router from 'next/router'

const { BaseLayer } = LayersControl;
var center = centerdefault;
var url_add, userID, user;
var params, showLegend;
var lang = null;

let scrollplaces = "h-100-scroll";
let userlat,userlng = null;  
let dragmarkercount=0;
let clickpolycount = 0;
let chart2 = null;

let resultporpartes = [];//array para el cálculo de perfil por partes, en algún sitio hay que acumularlo
let perfil = [];
let arrayPolylineaRehacer=[] //este array guarda una polilinea deshecha, a la que se le da la posibilida de rehacerla.
let arrayMarkerRehacer = [];//marcadores desechados
let arrayTramosRehacer =[];//si pulsamos en deshacer guardamos aquí como fue calculada la polylinea desecha
let arrayTramosDistanciasRehacer = [];//si pulsamos en deshacer guardamos aquí la distancia del tramo que hemos borrado
let markerprofile=null;//posición del marcador que se mueve cuando avanzamos sobre el perfil


const datosRutaInicial = {texto:"estado1",elevations: [], annotations:[],  alturamin:0, alturamax:0, ascend:0, distancia:0, descend:0,dif_altura:0,tiempo:0,coeficiente:0, categoria:0};
class Main extends Component {
    

    constructor(props){
        super(props);
        console.log(router.query)

        //var user = CheckLogin(this.props);

        params =router.query;
        
        user = props.user;
        if(user)
            userID = user.userID;

        
        
        [userlat,userlng] = GetUserPosition(props);
        if(userlat!=null && userlng !=null){
            center={
                lat:userlat,
                lng:userlng
            };  
        }

        //tomamos los parámetros de las cookies, y si no hay en cookie, pues el por defecto
        var tm = localStorage.getItem('travelMode') ? localStorage.getItem('travelMode') : "BICYCLING";
        var dp = localStorage.getItem('directions_prov') ? localStorage.getItem('directions_prov') : "openrouteservice";
        var ep = localStorage.getItem('elevations_prov') ? localStorage.getItem('elevations_prov') : "arcgis";
        var sm = localStorage.getItem('speedMode') ? localStorage.getItem('speedMode') : "22";
        
        this.state ={
            flag:0, //vamos a usar esta variable para renderizr la página sólo cuando queramos
            error:null,//{"message" : "vira que pedazo error"},
            isLoading: true,
            loadingRoute: true,//por defecto suponemos que se debe cargar ruta.
            mapacargado : false,
            map:null,
            lang:null,
            currentPosition:{lat:null,lng:null},     
            markers:[],//array de marcadores que se van pintando, arrastrando o borrando
            kms:[],//array de marcadores de kilómetros
            polylines:[],// línes que se pintan por cada tramo
            highways:true,
            travelMode:tm,//se define el travel mode a 1 que es coche-> normal maps. Directo es = 0
            directions_prov:dp,
            elevations_prov:ep,
            speedMode:sm,
            datosRuta: datosRutaInicial,
            tramosDistancias:[],  //este array guarda la distancia de cada tramo.          
            modalAnnotationIsOpen:false,
            modalPerfilIsOpen:false,
            modalImportarIsOpen:false,
            RowAnnotationclicked:null,
            TextAnnotationclicked: '',
            typeAnnotationclicked: '',
            repintarPerfil: false, //usamos esta variable para no repintar el perfil con cambios como añadir marcador o clicar en en el perfil que no afectan al mismo. Magic!
            titulo: '',
            descripcion: '',
            streetView: 'hidden',
            openPerfil:false,
            optionsPerfil:perfilOptions,
            gpxcargado:null,
            tracks:[], //esto son los tracks que se han subido desde el dispositivo para verlos y guiarse
            mostrarLayers:true,
            html_tramosJOIN:null,
            percentSurfaces:null,
            percentWaytypes:null,
            tramosOterreno:1,
            actualizado:false
        }
        
        lang = props.lang;

        if (typeof window !== "undefined") {
            let url  = window.location.href;
            if(url.search('f=1') !== -1)
            showLegend = true;
        }

        this.handeTravelMode  =   this.handeTravelMode.bind(this);
        this.handeDesactivateStreetView= this.handeDesactivateStreetView.bind(this);
        this.handeClickStreetView = this.handeClickStreetView.bind(this);
        
    }

    static getInitialProps({query}) {
        console.warn("GETINITIAs")
        return {query}
      }

    shouldComponentUpdate(nextProps, nextState) {
        consoleLOG(this.state);
        consoleLOG(nextState);
        return this.state.flag != nextState.flag;
    }

    componentDidUpdate(prevProps, prevState) {
        const { map } = this.state;
        const este = this;
        if (prevState.map !== map && map) {
            map.on("click", this.addMarker);
            map.on('zoomend', function() {
                este.changeZoom()
            });
        }
      }

      changeMostrarLayer(e, val){
          if(e.name == "Kilometers"){
                kilometerschecked = val;
          }
      }

      TramoOTerreno(val)
        {
            if(this.state.tramosOterreno != val)
                this.setState({ tramosOterreno:val,   flag: this.state.flag+1});  
        }


      changeZoom(){
            const { map } = this.state;
            var zoom = map.getZoom();
            //si quitamos zoom y estaba activo, lo desactivamos
            if(zoom <= 9 && this.state.mostrarLayers){
                this.setState({ mostrarLayers:false,   flag: this.state.flag+1});  
            }
            //si hacemos zoom y estaba desactivo, lo activamos
            if(zoom > 9 && !this.state.mostrarLayers){
                this.setState({ mostrarLayers:true,   flag: this.state.flag+1});  
            }          
      }


    renewClick(){
        //después de arrastrar un marcador, tenemos que establecer la posibilidad de volver a hacer click para crear uno nuevo
        this.state.map.on("click", this.addMarker);
    }

    
    async componentDidMount(){
        //si hay iniciada sesión pero no tenemos guardadas en cookie su ubicación, la obtenemos
        if(!params.id){
            if(userID != null && (userlat==null || userlng ==null)){
                navigator.geolocation.getCurrentPosition(
                    position => {
                            var lat = position.coords.latitude;
                            var lon = position.coords.longitude;
                            center = [lat, lon];
                            consoleLOG ("centrado" + center);
                            localStorage.setItem('lat', lat);
                            localStorage.setItem('lng', lon);
                            
                            const {map} = this.state;
                            if (map) map.flyTo(center, 10, {
                                animate: true,
                                duration: 0.5 // in seconds
                            });
                    },
                    error => consoleLOG(error.message),
                    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
                );        
            }    
        }
            

        if(params.id){
            var self = this;
            let data = GetParamsUserAgent();
            fetch(urlWS+'ruta/read.php'+data+'&id='+params.id)
                    .then(res => res.json())
                    .then(
                        (ruta) => {
                            //hemos leído la ruta, si es correcta hay que precargar todos los arrays, si es incorrecta mostrar error
                            if(ruta.polyline){
                                //ruta correcta
                                var isPropietario = false;
                                if(ruta.userID == userID){
                                    isPropietario = true;
                                }
                                var ararryID = [];
                                ruta.images.map((image) => {
                                    ararryID.push(image.rutaImageID);
                                })

                                var path = decodePolyline(ruta.polyline);
                                console.debug(path.length)
                                //Recordar que me inventé
                                
                                center={
                                    lat: parseFloat(path[0][0]),
                                    lng: parseFloat(path[0][1])
                                }
                                const {map} = self.state;
                                if (map) map.flyTo(center, 10, {
                                    animate: true,
                                    duration: 0.5 // in seconds
                                });


                                //PROBLEMÓN, YO SE LAS ELEVACIONES PERO NO SÉ DE QUÉ PUNTOS DEL PATH, PORQUE ELEVACIONES TENGO 20 Y PATH 80
                                //DEBO CALCULAR QUÉ PUNTOS SON LOS QUE ESTÁN A 100M, QUE VI A HACER
                                var arrayTrans = formarperfil(ruta.perfilLocations, ruta.perfilElevations);
                                
                                
                                //Esto hay que cambiarlo, porque no tendremos los arrays que devuelve ORS tal cuales, tendremos la info como la guardamos, simples coordenadas [ini,fin,waytype,surface]
                                console.warn(path)
                                var arrayResult = CalcPolylinesyTramosFromBD(path, ruta.surfaces);      
                                var polylines = arrayResult[0];
                                var markers = arrayResult[1];
                                var tramosDistancias = arrayResult[2];
                                

                                ruta.elevations = arrayTrans;
                                ruta.annotations = ruta.annotations ? JSON.parse(ruta.annotations) : [];
                                perfil = arrayTrans;

                                //ESTO NO HACE FALTA EN PRINCIPIO, PERO SI QUISIÉRAMOS RECALCULAR LOS DATOS SI QUE HARÍA FALTA, POR EJEMPLO SI QUEREMOS RECALCULAR EL DESNIVEL
                                
                                //let distancia = tramosDistancias.reduce((a, b) => a + b, 0);
                                let distancia = ruta.distancia;
                                var datosruta = calcularDatosRuta(arrayTrans, distancia, cadadist, this.state.travelMode);
                                ruta.distancia = distancia;
                                ruta.alturamin = datosruta[0];
                                ruta.alturamax = datosruta[1];
                                ruta.descend = datosruta[2];
                                ruta.ascend = ruta.desnivel;
                                ruta.dif_altura = datosruta[4];
                                ruta.tiempo = datosruta[5];
                                ruta.coeficiente = ruta.coeficiente;
                                ruta.categoria = ruta.categoria;


                                //vamos a poner un marcador por cada 5km
                                var marcadoreskm = marcadoreskms(polylines, tramosDistancias);

                                var [html_tramosJOIN, percentSurfaces, percentWaytypes] = GetSurfacesChart(polylines, distancia);
                                
                                /*///*/
                                /*var tipo_html = GetIconRouteType(ruta.tipo)
                                tipo_icono=tipo_html[0];
                                tipo_texto=lang[tipo_html[1]];*/

                                /*var bicicleta_html = GetIconRouteTerreno(ruta.bicicleta)
                                bicicleta_icono = bicicleta_html[0];
                                terreno_texto = lang[bicicleta_html[1]];
                                terreno_rotate = (bicicleta_html[1] == "b_mtb") ? "45" : "0";*/

                                consoleLOG("ruta:");
                                consoleLOG(ruta);

                                //IMAGEs*/
                                /*ruta.images.map((image) => {
                                    var url = baseurl+image.image;  
                                    images.push({"src":url}); 
                                })*/
                                
                                var optionsP = perfilOptions;
                                if(ruta.opcionesPerfil)
                                    optionsP = JSON.parse(ruta.opcionesPerfil);

                                console.warn ( "options perfil va a ser ")
                                consoleLOG(optionsP)
                                    

                                self.setState({ 
                                    loadingRoute:false,
                                    actualizado:true,
                                    datosRuta:ruta,
                                    tramosDistancias:tramosDistancias,
                                    userID:ruta.userID,
                                    isPropietario:isPropietario,
                                    polylines:polylines,
                                    markers:markers,
                                    optionsPerfil:optionsP,
                                    openPerfil:true, 
                                    flag: this.state.flag+1,
                                    percentSurfaces:percentSurfaces, 
                                    percentWaytypes:percentWaytypes,
                                    html_tramosJOIN:html_tramosJOIN, 
                                    kms:marcadoreskm,
                                    isLoading: false});  
                            }
                            else{
                                //ruta no existe o no está en la BD
                                self.setState({ 
                                    isLoading: false,
                                    loadingRoute:false,
                                    flag: this.state.flag+1,
                                    resultado:false
                                }); 
                            }
                        },
                        (error) => {
                            self.setState({ 
                                isLoading: false,
                                loadingRoute:false,
                                flag: this.state.flag+1,
                                resultado:false
                            });  
                            consoleLOG(error);
                        }
                    )
        }else{
            consoleLOG("no se recibe un rutaID")
            this.setState({ 
                isLoading: false,
                resultado:false,
                loadingRoute:false,
                flag: this.state.flag+1
            }); 
        }
    }


    /**************************************************** EVENTOS ******************************************* */  
    markerDragend (event, index){
        dragmarkercount++;
        var markersaux = [...this.state.markers];
        //sustituímos el marcador antiguo por el nuevo en los marcadores auxiliares
        markersaux[index] = event.target.getLatLng();

        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {markers:markersaux,  repintarPerfil:false, flag: this.state.flag+1}
        });
        var este = this;
        //al principio del arrastre, deshabilitamos el click, ahora lo habilitamos, dentro de 1 segundo que ya habrá terminado la posibilidad de crear un click al sortar el arrastre
        setTimeout(function() {
            este.renewClick();
        }, 1000);
        
        //consoleLOG("indice" + index);
        /*this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {markers:markersaux}
        });*/
        //es el primer punto de la ruta, modificaremos arrayPuntos[0]
        if(index == 0){
            this.ReCalcDirections(markersaux,0,1);
        }
        //si es el ultimo punto solo modificaremos arrayPuntos[arrayPuntos.length -1]
        else if(index == markersaux.length -1){
            this.ReCalcDirections(markersaux,markersaux.length -2,markersaux.length -1);
        }
        else{
            //aquí se produce una inconsistencia si hacemos las dos llamadas asíncronas a la vez, y leemos del estado cuando aún no ha sido actualizado el otro proceso.
            this.ReCalcDirections(markersaux,index -1,index);
            this.ReCalcDirections(markersaux,index,index +1);
        }

        /*tramos[index][0] =this.state.travelMode;
        tramos[index][1] =this.state.directions_prov;
        EDITAR ARRAY TRAMO. MEJOR PONERLO en estado y añadirle más info
        */
    }

    clickpolyline (event, index){
        event.originalEvent.view.L.DomEvent.stopPropagation(event)
        var poly = this.state.polylines[index];
        var travelMod = poly.travelMode;
        clickpolycount++;        
    }

    addMarker = (e) => {
        e.originalEvent.view.L.DomEvent.stopPropagation(e)
        e.originalEvent.preventDefault();
        const {markers} = this.state
        markers.push(e.latlng)
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {markers:markers,actualizado:false, streetView:'hidden',  repintarPerfil:false, flag: this.state.flag+1}
        });
        this.anularreahacer();
        this.CalcDirections(markers);
    }

    removeMarker = (e) => {
        var markersaux = [...this.state.markers]; 
        var markereliminado = markersaux.pop();     
        this.setState((state) => {
            return {markers:markersaux,  repintarPerfil:false, flag: this.state.flag+1}
        });
    }

    deshacer(){
        //lo que hacemos es sacar la última polilinea, pero la guardamos en una variable y si se le da a rehacer, la añadimos al array de dónde la quitamos. 
        //si se añaden puntos nuevos ya no se puede rehacer, y tenemos que anular esa posibilidad.
        
        if(this.state.polylines.length > 0){
            var polyaux = [...this.state.polylines];        
            var polyeliminada = polyaux.pop()
            arrayPolylineaRehacer.push(polyeliminada);
            
            var markersaux = [...this.state.markers]; 
            var markereliminado = markersaux.pop();     
            arrayMarkerRehacer.push(markereliminado);
            
            arrayTramosRehacer.push(polyeliminada);

            var tramosDistanciasAux = [...this.state.tramosDistancias];   
            var tramoDistanciaEliminado = tramosDistanciasAux.pop();
            arrayTramosDistanciasRehacer.push(tramoDistanciaEliminado);

            var marcadoreskm = marcadoreskms(polyaux, tramosDistanciasAux);

            let distancia = tramosDistanciasAux.reduce((a, b) => a + b, 0);
            var [html_tramosJOIN, percentSurfaces, percentWaytypes] = GetSurfacesChart(polyaux, distancia);
            
            this.setState((state) => {
                // Importante: lee `state` en vez de `this.state` al actualizar.
                return {polylines:polyaux,percentSurfaces:percentSurfaces, percentWaytypes:percentWaytypes,html_tramosJOIN:html_tramosJOIN, kms:marcadoreskm, markers: markersaux, tramosDistancias:tramosDistanciasAux,  repintarPerfil:false, flag: this.state.flag+1}
            });
        }
    }
    rehacer(){
        //si no hay nada que rehacer no rehacermos
        //si ya hemos rehecho un punto no lo podemos volver a rehacer
        if(arrayPolylineaRehacer.length > 0 && arrayMarkerRehacer.length >0){
            var polyaux = [...this.state.polylines];   
            var polyanadida = arrayPolylineaRehacer[arrayPolylineaRehacer.length - 1];
            /*consoleLOG("indice de la poly "+ polyaux.indexOf(polyanadida));
            consoleLOG(polyaux);
            consoleLOG(polyanadida);*/
            if(polyaux.indexOf(polyanadida) == -1)    { 
                polyaux.push(polyanadida);
                
                var markersaux = [...this.state.markers];      
                var markeranadido =  arrayMarkerRehacer[arrayMarkerRehacer.length - 1];
                if(markersaux.indexOf(markeranadido) == -1){
                    markersaux.push(markeranadido)
                    
                    //la polilinea y el marker que hemos rehecho la tenemos que borrar, ya que si no intentrá luego rehacerla de nuevo
                    arrayPolylineaRehacer.pop();
                    arrayMarkerRehacer.pop();

                    var tramoanadido = arrayTramosRehacer[arrayTramosRehacer.length - 1];
                    if(polyaux.indexOf(tramoanadido) == -1){
                        polyaux.push(tramoanadido);
                    }

                    var tramosDistanciasAux = [...this.state.tramosDistancias];      
                    var tramoDistanciaAniadido =  arrayTramosDistanciasRehacer[arrayTramosDistanciasRehacer.length - 1];
                    tramosDistanciasAux.push(tramoDistanciaAniadido);

                    //vamos a poner un marcador por cada 5km
                    var marcadoreskm = marcadoreskms(polyaux, tramosDistanciasAux);

                    let distancia = tramosDistanciasAux.reduce((a, b) => a + b, 0);
                    var [html_tramosJOIN, percentSurfaces, percentWaytypes] = GetSurfacesChart(polyaux, distancia);

                    this.setState((state) => {
                        // Importante: lee `state` en vez de `this.state` al actualizar.
                        return {polylines:polyaux,percentSurfaces:percentSurfaces, percentWaytypes:percentWaytypes,html_tramosJOIN:html_tramosJOIN, kms:marcadoreskm, markers: markersaux, tramosDistancias:tramosDistanciasAux, flag: this.state.flag+1}
                    });
                }
            }
            
            
        }
        else{
            alert("nada que rehacer");
        }
    }

    reset(){
        arrayPolylineaRehacer = [];
        arrayMarkerRehacer = [];
        var DatosRutaNew = {texto:"estado1",elevations: [], annotations:[],  alturamin:0, alturamax:0, ascend:0, distancia:0, descend:0,dif_altura:0,tiempo:0, coeficiente:0, categoria:0};
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {polylines:[],percentWaytypes:null, percentSurfaces:null,html_tramosJOIN:null, kms:[], markers: [], datosRuta: DatosRutaNew, tramosDistancias: [],  flag: this.state.flag+1}
        });
    }

    anularreahacer(){
        arrayPolylineaRehacer = [];
        arrayMarkerRehacer = [];
    }

    handeTravelMode(mode){
        localStorage.setItem('travelMode', mode);
        this.setState((state) => {
            return {travelMode:mode, flag: this.state.flag+1}
        });
    };

    handeSpeedMode(mode){
        localStorage.setItem('speedMode', mode);
        this.setState((state) => {
            return {speedMode:mode, flag: this.state.flag+1}
        });
    }

    handeTravelModePolyline(index, valor){
        //ahora tenemos que coger y llamar al servicio en cuestión con el nuevo modo
        var markers = this.state.markers;
        var travelMode = valor;

        if (travelMode != 'DIRECT') {
            var origin = markers[index];
            var destination = markers[index+1];
            this.CalcApiDirections(this.state.polylines[index].provider, travelMode, origin, destination, 1,index);//pos1 coincide con el index de la polylinea. Si es el marker0 es poly 0
        } else {
            var latlngs = obtptosintermedios_direct(markers[index],markers[index+1]);
            this.RePaintPoly(latlngs, index, this.state.polylines[index].provider, travelMode); 
        }
    }

    HandledirectionsProvPolyline(index, valor){
        //ahora tenemos que coger y llamar al servicio en cuestión con el nuevo provider
        var markers = this.state.markers;
        var provider = valor;
        var travelMode =this.state.polylines[index].travelMode;

        if (travelMode != 'DIRECT') {
            var origin = markers[index];
            var destination = markers[index+1];
            this.CalcApiDirections(provider, travelMode, origin, destination, 1,index);//pos1 coincide con el index de la polylinea. Si es el marker0 es poly 0
        } else {
            var latlngs = obtptosintermedios_direct(markers[index],markers[index+1]);
            this.RePaintPoly(latlngs, index, provider, travelMode); 
        }
    }


    HandleelevationProv(prov){
        localStorage.setItem('elevations_prov', prov);
        this.setState((state) => {
            return {elevations_prov:prov, flag: this.state.flag+1}
        });
    }

    HandledirectionsProv(prov){
        localStorage.setItem('directions_prov', prov);
        this.setState((state) => {
            return {directions_prov:prov, flag: this.state.flag+1}
        });
    }
    /**************************************************** FIN EVENTOS ******************************************* */  
   

    /***************************************************** MANEJADORES EVENTOS**************** */
    
    //cuando añadimos puntos y hay que calcular el recorrido
    CalcDirections(markers){
        var travelm = this.state.travelMode;
        consoleLOG("calcula tramo"+travelm);
        var l = markers.length;
        if (l > 1) {
            //Aquí se lee el tipo de viaje que queremos hacer (car, person, directo). En tramos se guarda con qué forma se calculó.  
            if (travelm != 'DIRECT') {
                var origin = markers[markers.length - 2];
                var destination = markers[markers.length - 1];
                this.CalcApiDirections(this.state.directions_prov, travelm, origin, destination, 0, null);
                //el resultado lo manejamos en HandlerCalcApiDirections
            } else {
                var latlngs = obtptosintermedios_direct(markers[markers.length - 2],markers[markers.length - 1]);
                this.PaintPoly(latlngs,  this.state.directions_prov, travelm);           
            }    
        }        
    }

    //cuando movemos puntos ya existentes y hay que recalcular el recorrido
    ReCalcDirections(markers,pos1,pos2) {
        consoleLOG("recalculamos línea del "+pos1+ " Al "+ pos2);
        var l = markers.length;
        if (l > 1) {
            //el array tramos guarda el modo de viaje para cada tramo
            var travelMode = this.state.polylines[pos1].travelMode;
            if (travelMode != 'DIRECT') {
                var origin = markers[pos1];
                var destination = markers[pos2];
                this.CalcApiDirections(this.state.polylines[pos1].provider, travelMode, origin, destination, 1,pos1);//pos1 coincide con el index de la polylinea. Si es el marker0 es poly 0
            } else {
                var latlngs = obtptosintermedios_direct(markers[pos1],markers[pos2]);
                this.RePaintPoly(latlngs, pos1, this.state.polylines[pos1].provider, travelMode); 
            }
        }
    }

    HandlerCalcApiDirections(success, proveedor, travelMode, recalc, path, polylineIndex, surfaces = null, waytypes = null){
        if(success){
            if(!recalc){
                //si es un punto nuevo y una polylinea nueva
                this.PaintPoly(path, proveedor, travelMode, waytypes, surfaces);                
            }
            else{
                //si es una polylinea que ya existía y la vamos a sustituir
                this.RePaintPoly(path, polylineIndex, proveedor, travelMode, waytypes, surfaces);                
            }
        }
        else{
            if(recalc){
                alert(proveedor+" "+ lang.no_route + " " + travelMode + ". " + lang.retry_no_route);
            }
            else{
                alert(proveedor+" "+ lang.no_route + " " + travelMode + ". " + lang.retry_no_route);
                //borro el marcador y ni si quiera creo el tramo
                this.removeMarker();
            }          
        }
        
    }

    /***************************************************** MANEJADORES EVENTOS**************** */

    
     /***************************************************  PINTAR LINEAS ****************** */
     PaintPoly(path, proveedor, travelMode, waytypes = false, surfaces = false){
        var polylinePath = [];
        path.map((punto) => {
            polylinePath.push(punto); 
        });

        
        var arrayResult = CalcPolyBySurfaces(path, waytypes, surfaces);   
        //[ini1,fin1][waytype1,surface1]....[iniN,finN][waytypeN,surfaceN]
        
        
        var pathSurfaces = arrayResult[0];
        

        var array_tramos_actual = [... this.state.tramosDistancias];
        consoleLOG("primer calculoDistancia: "+path);
        var distancia_tramo = distanciaTramo(path);
        array_tramos_actual.push(distancia_tramo);

        let distancia = array_tramos_actual.reduce((a, b) => a + b, 0);
        


        //realmente path solo se usa para unir más fácil todas las polylines para mandar un único array al perfil de google.
        //para los tramos y pintar las líneas en el mapa se usa pathSurfaces, que están al revés
        var polyobject = {"key":Math.random(),"pathSurfaces" : pathSurfaces, "path" : polylinePath, "provider": proveedor, "travelMode" : travelMode, "distance":distancia_tramo};
        
        const polylines = this.state.polylines;
        polylines.push(polyobject);
        //guardamos el tramo de puntos en arrayPuntos.

        //vamos a poner un marcador por cada 5km
        var marcadoreskm = marcadoreskms(polylines, array_tramos_actual);

        var [html_tramosJOIN, percentSurfaces, percentWaytypes] = GetSurfacesChart(this.state.polylines, distancia);

        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {polylines:polylines,percentSurfaces:percentSurfaces, percentWaytypes:percentWaytypes,html_tramosJOIN:html_tramosJOIN, tramosDistancias: array_tramos_actual, repintarPerfil:false, kms:marcadoreskm, flag: this.state.flag+1}
        });
    }
    
    RePaintPoly(path, indice, proveedor, travelMode, waytypes = false, surfaces = false){      
        var polylinePath = [];
        path.map((punto) => {
            polylinePath.push(punto); 
        });

        var array_tramos_actual = [... this.state.tramosDistancias];
        var distancia_tramo = distanciaTramo(path);
        array_tramos_actual[indice] = distancia_tramo;

        let distancia = array_tramos_actual.reduce((a, b) => a + b, 0);

        var arrayResult = CalcPolyBySurfaces(path, waytypes, surfaces);      
        //[ini1,fin1][waytype1,surface1]....[iniN,finN][waytypeN,surfaceN]


        var pathSurfaces = arrayResult[0];
        var polyobject = {"key":Math.random(),"pathSurfaces" : pathSurfaces, "path" : polylinePath, "provider": proveedor, "travelMode" : travelMode, "distance":distancia_tramo};
        
        var polyaux = [...this.state.polylines];    
        polyaux[indice] = polyobject;

        //vamos a poner un marcador por cada 5km
        var marcadoreskm = marcadoreskms(polyaux, array_tramos_actual);
        //calculamos la distancia de este tramo que ha sido calculado de nuevo y la guardamos en el índice correspondiente

        var [html_tramosJOIN, percentSurfaces, percentWaytypes] = GetSurfacesChart(this.state.polylines, distancia);
        

        ///tenemos que hacer esto porque si no no se llega a pintar porque los estados se pisan unos a otros. PARCHE . Lo solucionamos poniendo un key diferente a cada geojson, porra
        /*this.setState((newState) => {
            return {polylines: [], tramosDistancias: array_tramos_actual, repintarPerfil:false, flag: this.state.flag+1};            
        });*/
        
        this.setState((newState) => {
            return {actualizado:false,polylines: polyaux,percentSurfaces:percentSurfaces, percentWaytypes:percentWaytypes,html_tramosJOIN:html_tramosJOIN,kms:marcadoreskm, tramosDistancias: array_tramos_actual, repintarPerfil:false, flag: this.state.flag+1};            
        });
    }

    

    /*setPositionUser (e) {
        setPosition(e.latlng)
        consoleLOG('location found:', e)
        map.flyTo(e.latlng, map.getZoom())
    }*/



    

    /***************************************************** CALCULOS ****************************************************/
    HandleMouseOverChart(lat ,lng){
        var newLatLng = new L.LatLng(lat, lng);
        //consoleLOG(newLatLng);
        if(markerprofile== null){
            var iconRueda = CreateIconLeaflet("mtbpin",20);
            markerprofile = L.marker(newLatLng, {icon: iconRueda}).addTo(this.state.map);
        }
        else{
            markerprofile.setLatLng(newLatLng);
        }
    }

    HandleClickChart(row){
        consoleLOG("fila clicada num:" + row)
        var pos = this.PosAnnotation(row);
        var text = '';
        var type = '';
        if(pos >= 0){
            text = this.state.datosRuta.annotations[pos].text;
            type = this.state.datosRuta.annotations[pos].type;
        }
            

        this.setState((state) => {
            return {modalAnnotationIsOpen:true, openPerfil:!this.state.openPerfil, RowAnnotationclicked:row, TextAnnotationclicked:text,typeAnnotationclicked:type, repintarPerfil:false, flag: this.state.flag+1}
        });
    }

    PosAnnotation(row){
        var d = this.state.datosRuta;
        var aux = d.annotations;
        for(var i=0; i < aux.length ; i++){
            if(aux[i].row == row){
                return i;
            }
        }
        return -1;
    }

    CrearAnnotation(row, text, type){
        consoleLOG("crearannotatnos" + text);
        
        var d = this.state.datosRuta;
        var aux = d.annotations;

        //si no existe es un push, pero si existe es un edit
        var pos = this.PosAnnotation(row);

        if(pos >= 0){
            aux[pos].text = text;
            //aux[pos].type = type; del tipo que lo creems debe permanecer. que lo borren
        }
        else
            aux.push({row: row, text: text, type: type});

        consoleLOG(aux)
        d.annotations = aux;

        this.setState((state) => {
            return {datosRuta:d, modalAnnotationIsOpen:false, openPerfil:!this.state.openPerfil, repintarPerfil:true, flag: this.state.flag+1}
        });
    }

    BorrarAnnotation(row){
        var d = this.state.datosRuta;
        var pos = this.PosAnnotation(row);
        //Si no existe no la puedes eliminar
        if(pos > -1){
            var annns =d.annotations;
            annns.splice(pos, 1);
            d.annotations = annns;
            this.setState((state) => {
                return {datosRuta:d, modalAnnotationIsOpen:false,openPerfil:!this.state.openPerfil, repintarPerfil:true, flag: this.state.flag+1}
            });
        }else{
            alert("No la has creado, no la puedes eliminar")
        }
    }

    HandleMouseOutChart(){
        consoleLOG("mouseout")
        if(markerprofile != null){
            this.state.map.removeLayer(markerprofile);
            markerprofile = null;
        }
    }

    AbrirPefil(){
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {openPerfil:true,repintarPerfil:false, flag: this.state.flag+1}
        });
    }

    CerrarPerfil(){
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {openPerfil:false,repintarPerfil:false, flag: this.state.flag+1}
        });
    }
    
    EditarPerfil(){
        this.toggleShowPerfilModal();
    }

    ActualizarPerfil(objetoOpciones, cerrar){
        //consoleLOG(objetoOpciones)
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {modalPerfilIsOpen:cerrar,openPerfil:!this.state.openPerfil, optionsPerfil:objetoOpciones, repintarPerfil:true, flag: this.state.flag+1}
        });
    }

    importar(contentFile){
        //cargar gpx
        var gpx = contentFile;
        this.setState({gpxcargado:gpx,modalImportarIsOpen: !this.state.modalImportarIsOpen, repintarPerfil:false, flag: this.state.flag+1})
    }

    PintarRutaImportada(){
        var gpxcontent = this.state.gpxcargado;
        const gpxLibrary = GPX.parse(gpxcontent);
        consoleLOG(gpxLibrary);
        if(!gpxLibrary){
            alert(lang.error_ruta)
            this.setState({isLoading:true,modalImportarIsOpen: !this.state.modalImportarIsOpen, repintarPerfil:false, flag: this.state.flag+1})
            return;
        }
        var tracksestado = [... this.state.tracks];        
        var tracks = gpxLibrary.trk;
        consoleLOG(tracks)
        var volado = false
        for(var i = 0; i < tracks.length; i++){
            var trk = tracks[i];
            var name = trk.name;
            var pathway = [];
            var trkseg = trk.trkseg;
            for(var j = 0; j < trkseg.length; j++){
                var trkpt = trkseg[j].trkpt;
                for(var z = 0; z < trkpt.length; z++){
                    var loc = trkpt[z];
                    var lat = loc.$.lat;
                    var lon = loc.$.lon;
                    if(!volado){
                        var pos = [lat, lon]
                        const {map} = this.state;
                        if (map) map.flyTo(pos, 10, {
                            animate: true,
                            duration: 0.5 // in seconds
                          });
                        volado = true;
                    }
                    pathway.push([lon,lat]);
                }
            }
            var geojson = { "type": "Feature", "properties": {"key":tracksestado.length, "name":name}, "geometry": { "type": "LineString", "coordinates": pathway } };
            tracksestado.push(geojson);    
        }
        this.setState({isLoading:false,modalImportarIsOpen: !this.state.modalImportarIsOpen, tracks:tracksestado, repintarPerfil:false, flag: this.state.flag+1})
    }

    CargarRuta(){
        this.reset();
        var gpxcontent = this.state.gpxcargado;
        var contentparser = gpxcontent.replace(/(\r\n|\n|\r)/gm, "");//remove those line breaks
        const gpxLibrary = GPX.parse(contentparser);
        consoleLOG(gpxLibrary);
        if(!gpxLibrary){
            alert(lang.error_ruta)
            this.setState({isLoading:true,modalImportarIsOpen: !this.state.modalImportarIsOpen, repintarPerfil:false, flag: this.state.flag+1})
            return;
        }
            
        var tracks = gpxLibrary.trk;
        consoleLOG(tracks)
        var volado = false
        for(var i = 0; i < tracks.length; i++){
            var trk = tracks[i];
            var name = trk.name;
            var pathway = [];
            var trkseg = trk.trkseg;
            for(var j = 0; j < trkseg.length; j++){
                var trkpt = trkseg[j].trkpt;
                for(var z = 0; z < trkpt.length; z++){
                    var loc = trkpt[z];
                    var lat = parseFloat(loc.$.lat);
                    var lon = parseFloat(loc.$.lon);
                    if(!volado){
                        var pos = [lat, lon]
                        const {map} = this.state;
                        if (map) map.flyTo(pos, 10, {
                            animate: true,
                            duration: 0.5 // in seconds
                          });
                        volado = true;
                    }
                    pathway.push([lat,lon]);
                }
            }
            
        }

        //pathway es toda la ruta. La vamos a divivir por partes para así sea más fácil editarla, y mover los puntos
        var partes = DivivirRuta(pathway);
        var markers = [];
        var polylines = [];
        var array_tramos_actual = [];
        partes.forEach(path => {
            var locmarker ={lat: path[0][0], lng: path[0][1]};
            markers.push(locmarker)
            

            var arrayResult = CalcPolyBySurfaces(path, false, false);   
            //[ini1,fin1][waytype1,surface1]....[iniN,finN][waytypeN,surfaceN]
            var pathSurfaces = arrayResult[0]; 
            

            var distancia_tramo = distanciaTramo(path);
            array_tramos_actual.push(distancia_tramo);

            var polyobject = {"key":Math.random(),"pathSurfaces" : pathSurfaces, "path" : path, "provider": "external", "travelMode" : "external", "distance":distancia_tramo};
            polylines.push(polyobject);
        });

        //faltaría añadir el marcador del final de la ruta, ya que hemos cogido el primer punto de cada tramo.
        var locmarkerF ={lat: pathway[pathway.length-1][0], lng: pathway[pathway.length-1][1]};
        markers.push(locmarkerF)
        

        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {isLoading:false,modalImportarIsOpen: !this.state.modalImportarIsOpen, markers:markers, polylines:polylines, tramosDistancias: array_tramos_actual, repintarPerfil:false, flag: this.state.flag+1}
        });
        //this.play(); //no podemos llamar porque aún no ha cambiado el estado y por tanto no valdría.
        return;
    }

    play(){
        if(this.state.polylines.length < 1){
            alert(lang.no_points);
            return;
        }  
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {openPerfil:true,isLoading:true, streetView:'hidden', repintarPerfil:false, flag: this.state.flag+1}
        });

        //waitingDialog.show();
        //polylines es una array de arrays, no se puede mandar tal cual.
        var pathPolylines = [];
        this.state.polylines.forEach(pol => {
            pathPolylines.push(pol.path);
        })
        var path = descomponArrayTramos(pathPolylines);
        consoleLOG("play");
        //dividimos el recorrido en puntos cada 100 metros
        perfil = obtptoskilometricos(path, 100);

        console.warn("perfil tiene " + perfil.length+ " y path tiene "+ path.length)
      
        

        //consoleLOG(perfil);
        //calculamos las alturas de dichos puntos, nunca se puede hacer llamada de más de 512 puntos, las haremos cada 300
        if(perfil.length > 300){
            resultporpartes = [];
            this.calcularAlturasPorPartes(this.state.elevations_prov, 0, perfil);              
        }
        else
            this.calcularAlturas(this.state.elevations_prov, perfil);
    }

    calcularAlturas(proveedor, perfil){
        if (typeof window !== "undefined") {
            consoleLOG("Cálculo de elevación con "+ proveedor);
            var este = this;
            if(proveedor == "google"){
                    var locations = [];            
                    for(var i=0;i<perfil.length;i++){
                        var latlng = new window.google.maps.LatLng(perfil[i][0], perfil[i][1]);
                    }
                    var positionalRequest = {
                        'locations': locations
                    }
                    var elevationService = new window.google.maps.ElevationService();
                    elevationService.getElevationForLocations(positionalRequest, function(results, status) {
                    if (status == window.google.maps.ElevationStatus.OK) {
                        este.calcularDatos(results);
                        //consoleLOG(results);
                    } else {
                        alert("error en el cáculo de elevación.");
                    }
                    });
                

            }
            else if(proveedor == "arcgis"){
                loadModules(['esri/Map', "esri/geometry/Polyline", "esri/geometry/SpatialReference"])
                .then(([Map, Polyline, SpatialReference]) => {
                    const map = new Map({
                        ground: "world-elevation"
                    });
                    

                    var path2 = [];
                    for(var i = 0; i < perfil.length; i++){
                        path2.push([perfil[i][1], perfil[i][0]]);
                    }
                    
                        consoleLOG("vamos a calcular el perfil:")
                        consoleLOG(path2)
                        const elevationPromise = map.ground.queryElevation(
                            new Polyline({
                            paths: path2,
                            spatialReference: SpatialReference.WGS84
                            })
                        );

                        elevationPromise.then((result) => {
                            consoleLOG("resultados:");
                            consoleLOG(result);
                            const path = result.geometry.paths[0];
                            consoleLOG(path);

                            //ahora tengo en path un array de [lng,lat,alt]

                            //este.calcularDatos(results);
                            var arrayTrans = [];
                            for(var i=0;i<path.length;i++){
                                var latlng = new window.google.maps.LatLng(path[i][1], path[i][0]);

                                arrayTrans.push({elevation:path[i][2], location:latlng});
                            }
                            este.calcularDatos(arrayTrans);
                        });
                });

            }else if(proveedor == "openrouteservice"){
                let request = new XMLHttpRequest();
                request.open('POST', "https://api.openrouteservice.org/elevation/line");            
                request.setRequestHeader('Accept', 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8');
                request.setRequestHeader('Content-Type', 'application/json');
                request.setRequestHeader('Authorization', apiKey_openroute);
                request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    let response = JSON.parse(this.response);
                    var path = response.geometry;
                    //consoleLOG("encoded " + g);
                    //var path = decodePolylineElevation(g, true);
                    consoleLOG(path)
                    var arrayTrans = [];
                    for(var i=0;i<path.length;i++){
                        var latlng = new window.google.maps.LatLng(path[i][1], path[i][0]);
                        arrayTrans.push({elevation:path[i][2], location:latlng});
                    }
                    este.calcularDatos(arrayTrans);
                }
                };

                var pathO = "[";
                for(var i = 0; i < perfil.length; i++){
                    if(i== 0)
                        pathO += "["+perfil[i][1]+","+ perfil[i][0]+"]";
                    else
                        pathO += ",["+perfil[i][1]+","+ perfil[i][0]+"]";
                }
                pathO += "]";


                const body2 = '{"format_in":"polyline","format_out":"polyline","dataset":"srtm","geometry":'+pathO+'}';
                consoleLOG(body2);
                request.send(body2);
            }
        }
    }
      
    calcularAlturasPorPartes(proveedor, puntero, perfil){
        if (typeof window !== "undefined") {
            var este = this;
            if(proveedor == "google"){
                var limite = puntero + 300;
                if(limite > perfil.length)
                    limite = perfil.length;
                var locations = [];
                for(var i = puntero;i < limite;i++){
                    var latlng = new window.google.maps.LatLng(perfil[i][0], perfil[i][1]);
                    locations.push(latlng);
                }
                var positionalRequest = {
                    'locations': locations
                }
                var elevationService = new window.google.maps.ElevationService();
                elevationService.getElevationForLocations(positionalRequest, function(results, status) {
                    if (status == window.google.maps.ElevationStatus.OK) {
                        //si es el último pintamos y si no llamamos siguiente
                        for(var j=0;j<results.length;j++){
                            resultporpartes.push(results[j]);
                        }

                        if(limite == perfil.length){
                            este.calcularDatos(resultporpartes);
                        }
                        else{
                            este.calcularAlturasPorPartes(proveedor, limite, perfil);
                        }
                    } else {
                        if(status == "OVER_QUERY_LIMIT" || results == null){
                            setTimeout("calcularAlturasPorPartes("+puntero+")", 50);
                        }
                        else{
                            alert("error en el cáculo de elevación.");
                        }
                    }
                });
            }
            else if(proveedor == "arcgis"){
                loadModules(['esri/Map', "esri/geometry/Polyline", "esri/geometry/SpatialReference"])
                .then(([Map, Polyline, SpatialReference]) => {
                    const map = new Map({
                        ground: "world-elevation"
                    });
                    
                    var limite = puntero + 300;
                    if(limite > perfil.length)
                        limite = perfil.length;

                    var path2 = [];
                    for(var i = puntero; i < limite; i++){
                        path2.push([perfil[i][1], perfil[i][0]]);
                    }
                    

                    const elevationPromise = map.ground.queryElevation(
                        new Polyline({
                            paths: path2,
                            spatialReference: SpatialReference.WGS84
                        })
                    );

                    elevationPromise.then((result) => {
                            consoleLOG("resultados:");
                        const path = result.geometry.paths[0];

                        //ahora tengo en path un array de [lng,lat,alt]

                        for(var i=0;i<path.length;i++){
                            var latlng = new window.google.maps.LatLng(path[i][1], path[i][0]);

                            resultporpartes.push({elevation:path[i][2], location:latlng});
                        }

                        if(limite == perfil.length){
                            this.calcularDatos(resultporpartes);
                        }
                        else{
                            this.calcularAlturasPorPartes(proveedor, limite, perfil);
                        }
                    });
                });
            }
            else if(proveedor == "openrouteservice"){
                let request = new XMLHttpRequest();
                request.open('POST', "https://api.openrouteservice.org/elevation/line");            
                request.setRequestHeader('Accept', 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8');
                request.setRequestHeader('Content-Type', 'application/json');
                request.setRequestHeader('Authorization', apiKey_openroute);
                request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    let response = JSON.parse(this.response);
                    consoleLOG("respuesta")
                    consoleLOG(response);
                    var path = response.geometry;
                    //consoleLOG("encoded " + g);
                    //var path = decodePolylineElevation(g, true);
                    consoleLOG(path)
                    for(var i=0;i<path.length;i++){
                        var latlng = new window.google.maps.LatLng(path[i][1], path[i][0]);
                        resultporpartes.push({elevation:path[i][2], location:latlng});
                    }

                    if(limite == perfil.length){
                        este.calcularDatos(resultporpartes);
                    }
                    else{
                        este.calcularAlturasPorPartes(proveedor, limite, perfil)
                    }
                    
                }
                };

                var limite = puntero + 300;
                if(limite > perfil.length)
                    limite = perfil.length;

                //consoleLOG("vamos a calcular desde " + puntero + " a "+limite)
                var pathO = "[";
                for(var i = puntero; i < limite; i++){
                    if(i== puntero)
                        pathO += "["+perfil[i][1]+","+ perfil[i][0]+"]";
                    else
                        pathO += ",["+perfil[i][1]+","+ perfil[i][0]+"]";
                }
                pathO += "]";


                const body2 = '{"format_in":"polyline","format_out":"polyline","dataset":"srtm","geometry":'+pathO+'}';
                consoleLOG("petición" + body2);
                request.send(body2);
            }
        }
    }

    calcularDatos(results){
        var d = this.state.datosRuta;
        var tramosDistancias = this.state.tramosDistancias;
        let distancia = tramosDistancias.reduce((a, b) => a + b, 0);
        var datosruta = calcularDatosRuta(results, distancia, cadadist, this.state.speedMode);
        d.distancia = distancia;
        d.alturamin = datosruta[0];
        d.alturamax = datosruta[1];
        d.descend = datosruta[2];
        d.ascend = datosruta[3];
        d.dif_altura = datosruta[4];
        d.tiempo = datosruta[5];
        d.coeficiente = datosruta[6];
        d.categoria = datosruta[7];
        d.elevations = results;

        
        
        this.setState((state) => {
            return {datosRuta:d, actualizado:true, isLoading:false,  repintarPerfil:true, flag: this.state.flag+1}
        });
    }

    

      /************************************************************************* FIN CALCULOS **********************/



    /****************************************************************************MODALES */
    toggleShowModal(){
        this.setState({modalAnnotationIsOpen: !this.state.modalAnnotationIsOpen, repintarPerfil:false, flag: this.state.flag+1})
    }

    toggleShowPerfilModal(){
        this.setState({modalPerfilIsOpen: !this.state.modalPerfilIsOpen, openPerfil:!this.state.openPerfil, repintarPerfil:false, flag: this.state.flag+1})
    }

    toggleShowImportarModal(){
        this.setState({modalImportarIsOpen: !this.state.modalImportarIsOpen, repintarPerfil:false, flag: this.state.flag+1})
    }

    
    handleChangeInput(name, value){
        //no cambiamos el flag, para que no actualice toda la página sólo para eso, no hace falta
        this.setState({
                [name]: value
        });
    }

    handeDesactivateStreetView(){
        this.setState({
            streetView:'hidden',flag: this.state.flag+1
        });
    }

    handeClickStreetView(e){
        this.state.map.off("click");
        //al principio del arrastre, deshabilitamos el click, ahora lo habilitamos, dentro de 1 segundo que ya habrá terminado la posibilidad de crear un click al sortar el arrastre
        var este = this;
        setTimeout(function() {
            este.renewClick();
        }, 1000);
        var este = this;
        var panorama = null;
        var svs = new window.google.maps.StreetViewService();
        svs.getPanorama({location: e, preference: 'nearest'}, function(data, status){
            if(data == null){
                este.setState({
                    streetView:'error',flag: este.state.flag+1
                });
                return;
            }
            consoleLOG('pano1: ' + data.location.pano);
            consoleLOG('pano result: ' + data);
            var pos = data.location.latLng;
            consoleLOG(pos.toString());
            
            if(este.state.streetView != ''){
                este.setState({
                    streetView:'',flag: este.state.flag+1
                });
            }

            panorama = new window.google.maps.StreetViewPanorama(
            document.getElementById('pano'),
            {
              pano: data.location.pano
            });
        });

              
    }

    showToastTramo(tipo, valor){
        class HtmlDiv extends React.Component {
            render() {
              return <div className="toastInfo">{this.props.pre}<b>{this.props.valor}</b>{this.props.post}</div>
            }
        }
        
        valor = valor.toUpperCase();

        if(valor == "BICYCLING"){
            valor = lang.mod_bike;
        }
        if(valor == "DRIVING"){
            valor = lang.mod_drive;
        }

        var mensaje = "";
        if(tipo == 1){
            mensaje = lang.providerInfo ;
        }
        else if(tipo == 0){
            mensaje = lang.travelModeInfo;
        }

        this.OcultarButterToastAndDiv();
        ButterToast.raise({
            timeout: 3500,
            content: <Cinnamon.Crisp scheme={Cinnamon.Crisp.SCHEME_BLUE}
            content={<HtmlDiv pre={mensaje} valor={valor} post={lang.action}/>}/>
            });
    }

    OcultarButterToastAndDiv(){
        ButterToast.dismissAll();
    }

    showToastLegend() {
        this.OcultarButterToastAndDiv();
        ButterToast.raise({
            timeout: 30500,
            content: <Cinnamon.Crisp scheme={Cinnamon.Crisp.SCHEME_BLUE}
            content={<Legend lang={lang}/>}/>
            });
    }
    /////////////////////////////////////////////////////////////////////////////* FIN MODALES

    

    showGoToCreate(urlGoToCreate) {
        //document.getElementById("gotocreate").style.display = "block";    
        //this.props.history.replace(urlGoToCreate);
        router.push(urlGoToCreate)
    }

    CreateRuta() {
        var estado = this.state;
        if(estado.titulo.length < 1){
            alert(lang.no_title);
            return;
        }
        if(estado.datosRuta.elevations.length < 1){
            alert(lang.no_route_save)
            return;
        }

        
        //aquí debemos de llamar al ws para crear el local y pasar this.state
        var params2 = convertToParamas(estado, userID);
        var encodedPerfilElevation = calcularEncodedElevationsRuta(estado.datosRuta.elevations);
        params2.perfilLocations = encodedPerfilElevation[0];
        params2.perfilElevations = encodedPerfilElevation[1];
        consoleLOG(params2);
        //return;
        
        
        this.setState({
            isLoading:true,flag: this.state.flag+1
        });
        consoleLOG(params2);
        const self = this;
        
        (async () => {
            let data3 = GetParamsUserAgent();
            const rawResponse = await fetch(urlWS+'ruta/create.php'+data3, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
              },
              body: JSON.stringify(params2)
            });
            const response = await rawResponse.json();    
            consoleLOG(response);
            if(response.id && parseInt(response.id) > 0){
                var id = response.id;
                var ur = URLamigable(estado.titulo)
                url_add = "/edit/"+ur+"_"+id;
                self.showGoToCreate(url_add);                              
            }
            else{
                alert(lang.error_route_created);
            }
            
            self.setState({ 
                isLoading:false
            });  
                     
          })();
      }



      /*/////////////////////////////////////FUNCIONES TOCHOS////////////////////////////////////////*/

      CalcApiDirections(proveedor, travelMode, origin, destination, recalculo = 0, indice = null){
        //Si estamos recalculando un tramo que venía de un archivo cargado el proveedor será external, y ahora lo calcularemos con el que esté elegido
        if(proveedor == "external") proveedor = this.state.directions_prov;
        if(travelMode == "external") travelMode = this.state.travelMode;

        var este = this;
        if(proveedor == "google"){
            const directionsService = new window.google.maps.DirectionsService();
            directionsService.route(
                {
                origin: origin,
                destination: destination,
                travelMode: travelMode
                },
                (result, status) => {
                if (status === window.google.maps.DirectionsStatus.OK) {
                    var path = result.routes[0].overview_polyline ;
                    path = decodePolyline(path);
                    this.HandlerCalcApiDirections(true,proveedor,travelMode, recalculo, path, indice);      
                } else {
                    console.error(`error fetching directions ${result}`);
                    this.HandlerCalcApiDirections(false,proveedor,travelMode, recalculo, null, indice);              
                }
                }
            );
        }
        else if(proveedor == "mapbox"){
            var data = origin.lng+","+origin.lat+";"+destination.lng+","+destination.lat;
            var modelowwer = travelMode.toLowerCase();
            if (modelowwer == "bicycling") modelowwer ="cycling";
            
           
            fetch('https://api.mapbox.com/directions/v5/mapbox/'+modelowwer+'/'+data+'?access_token='+apikey_mapbox+'&geometries=polyline')
            .then(res => res.json())
            .then(
                (result) => {
                if(result && result.code == "Ok"){             
                    if(result.routes.length > 0){
                        var path = decodePolyline(result.routes[0].geometry);
                        this.HandlerCalcApiDirections(true,proveedor,travelMode, recalculo, path, indice);      
                        }                        
                }
                else{
                    consoleLOG(result);
                    this.HandlerCalcApiDirections(false,proveedor,travelMode, recalculo, null, indice);        
                }
                },
                (error) => {
                    consoleLOG(error);
                    this.HandlerCalcApiDirections(false,proveedor,travelMode, recalculo, null, indice);      
                }
            )
            
        }
        else if(proveedor == "openrouteservice"){

            /*
                driving-car",
              "driving-hgv",
              "cycling-regular",
              "cycling-road",
              "cycling-mountain",
              "cycling-electric",
              "foot-walking",
              "foot-hiking",
              "wheelchair
            */

              let request = new XMLHttpRequest();

              var modelowwer = travelMode.toLowerCase();
              if (modelowwer == "bicycling") modelowwer ="cycling-regular";
              if (modelowwer == "driving") modelowwer ="cycling-road";
              if (modelowwer == "walking") modelowwer ="cycling-mountain";
              
  
              request.open('POST', "https://api.openrouteservice.org/v2/directions/"+modelowwer);
  
              request.setRequestHeader('Accept', 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8');
              request.setRequestHeader('Content-Type', 'application/json');
              request.setRequestHeader('Authorization', apiKey_openroute);
  
              request.onreadystatechange = function () {
              if (this.readyState === 4) {
                  //consoleLOG('Status:', this.status);
                  //consoleLOG('Headers:', this.getAllResponseHeaders());
                  consoleLOG('Body:', this.response);
                  if (this.status != 200) {
                      este.HandlerCalcApiDirections(false,proveedor,travelMode, recalculo, null, indice, null);      
                      return;
                  }
                  

                  let response = JSON.parse(this.response);
                  consoleLOG(response)
                  if(!response.routes){
                      este.HandlerCalcApiDirections(false,proveedor,travelMode, recalculo, null, indice, null);      
                  }
                  else{
                      var path = decodePolyline(response.routes[0].geometry);
                      var surfaces = response.routes[0].extras.surface.values;
                      var waytypes = response.routes[0].extras.waytypes.values;
                      este.HandlerCalcApiDirections(true, proveedor, travelMode, recalculo, path, indice, surfaces, waytypes);
                  }
              }
              };
              let latlon = "["+
                              "["+origin.lng+","+origin.lat+"]"+","+
                              "["+destination.lng+","+destination.lat+"]"+"]";
  
              //https://github.com/GIScience/openrouteservice-docs parámetros 
              const body = '{"coordinates":'+latlon+',"options":{"avoid_features":["steps"]},"instructions":"false","elevation":"false","units":"km","extra_info":["surface","waycategory","waytype"]}';
              //const body = '{"coordinates":'+latlon+',"attributes":["avgspeed","detourfactor","percentage"],"elevation":"true","extra_info":["steepness","suitability","surface","waycategory","waytype","traildifficulty","roadaccessrestrictions"],"radiuses":[10000,10000],"units":"mi"}';    
              request.send(body);	
          }
      }

      /*************************************************************************** */
  
    render() {        
        consoleLOG("render main");
        //si tenemos guardada la pos en las cookies
        
        const {error, isLoading, loadingRoute } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } 
        
        let divmap;
        if(isMobile || window.cordova){
            divmap = " h-90 nopadding";
            }
        else{
            divmap = " h-100 nopadding";
        }           
        consoleLOG("se renderiza y el streetview es " + this.state.streetView);

        var percentSurfaces = this.state.percentSurfaces;
        var percentWaytypes = this.state.percentWaytypes;

        consoleLOG(percentWaytypes)

        var mostrarTerrenoBajoPerfil =false;
        if(percentSurfaces && percentSurfaces.length > 0){       
            mostrarTerrenoBajoPerfil=true; 
            Object.keys(percentSurfaces).map(key => 
                {
                    if(percentSurfaces[key][0] == 100)
                        mostrarTerrenoBajoPerfil = false;
                }
            )
        }


        var surfacePrintPerfil  = this.state.html_tramosJOIN && mostrarTerrenoBajoPerfil && HasSurfaces(percentSurfaces) && this.state.actualizado;

        return (        
            <div className="container-fluid h-100" lang={lang.lang}>                                     

                
                {isLoading &&
                    <div className="loading_center h-100">
                        <div className="spinner-grow text-primary" role="status">
                        <span className="sr-only">Loading...</span>
                        </div>
                        <div className="spinner-grow text-secondary" role="status">
                        <span className="sr-only">Loading...</span>
                        </div>
                        <div className="spinner-grow text-success" role="status">
                        <span className="sr-only">Loading...</span>
                        </div>
                        <div className="spinner-grow text-danger" role="status">
                        <span className="sr-only">Loading...</span>
                        </div>
                        <div className="spinner-grow text-warning" role="status">
                        <span className="sr-only">Loading...</span>
                        </div>
                        <div className="spinner-grow text-info" role="status">
                        <span className="sr-only">Loading...</span>
                        </div>
                        <div className="spinner-grow text-light" role="status">
                        <span className="sr-only">Loading...</span>
                        </div>
                        <div className="spinner-grow text-dark" role="status">
                        <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                } 

                {!loadingRoute &&
                
                <div className="row h-100">
                    <div className={"col-lg-9 col-12" + divmap}>
                        <ButterToast timeout={100000} position={{vertical: POS_TOP,  horizontal: POS_CENTER }} style={{ top: '100px',  right: '100px' }}/>
                        <div className="map h-100">
                                <div id="pano" className={this.state.streetView + " streetview"} style={{position:'absolute', bottom:0, height: '50%', width:'100%', zIndex:'20000000000000000000000'}}>
                                    
                                </div>                                
                            
                            <ModalAnnotation CrearAnnotation={this.CrearAnnotation.bind(this)} BorrarAnnotation={this.BorrarAnnotation.bind(this)} lang={lang} type={this.state.typeAnnotationclicked} row={this.state.RowAnnotationclicked} texto={this.state.TextAnnotationclicked} abierto={this.state.modalAnnotationIsOpen} toggleShowModalCallback={this.toggleShowModal.bind(this)}>
                            </ModalAnnotation>

                            <ModalPerfil ActualizarPerfil={this.ActualizarPerfil.bind(this)} optionsPerfil={this.state.optionsPerfil} lang={lang} abierto={this.state.modalPerfilIsOpen} toggleShowModalCallback={this.toggleShowPerfilModal.bind(this)}>
                            </ModalPerfil>

                            <ModalImportar lang={lang} abierto={this.state.modalImportarIsOpen} toggleShowModalCallback={this.toggleShowImportarModal.bind(this)} CargarRuta={()=>this.CargarRuta()} PintarRuta={()=>this.PintarRutaImportada()}>
                            </ModalImportar>

                            <Herramientas estado={this.state} HandleelevationProv={this.HandleelevationProv.bind(this)} HandledirectionsProv={this.HandledirectionsProv.bind(this)} travelMode={this.state.travelMode} handeTravelMode= {this.handeTravelMode.bind(this)} HandleSpeed = {this.handeSpeedMode.bind(this)} lang={lang} deshacer={()=>this.deshacer()} rehacer={()=>this.rehacer()} reset={()=>this.reset()} play={()=>this.play()} importar={this.importar.bind(this)}/>      
                                
                            { !this.state.openPerfil && this.state.datosRuta.elevations.length > 0 &&
                                <div className="btn-grupo abrirPerfil" role="group" aria-label="..." >
                                    <button onClick ={()=>this.AbrirPefil()} title={lang.abrir_perfil} name="mode" type="button" className={"btn-light btn-sm"}>
                                        <FontAwesomeIcon icon={faChartArea} aria-hidden="true" size="lg"/>
                                    </button>
                                </div>
                            }

                                {console.warn("PEFIL"+mostrarTerrenoBajoPerfil)}
                            
                                { this.state.openPerfil && this.state.datosRuta.elevations.length > 0 &&
                                    <div id="menuPerfil" className="container-fluid" onMouseLeave={()=>this.HandleMouseOutChart()} >
                                        <div className="cajablanca h-100" >
                                            <div className="col-12 h-100-10">
                                                <Perfil titulo="" polylines={this.state.polylines} surfacePrintPerfil={surfacePrintPerfil} page="main" optionsPerfil={this.state.optionsPerfil} repintar={this.state.repintarPerfil} EditarPerfil={this.EditarPerfil.bind(this)} CerrarPerfil={this.CerrarPerfil.bind(this)} HandleMouseOverChart={this.HandleMouseOverChart.bind(this)} HandleClickChart={this.HandleClickChart.bind(this)} datos = {this.state.datosRuta} lang={lang}/>
                                            </div>

                                            {this.state.html_tramosJOIN && mostrarTerrenoBajoPerfil && HasSurfaces(percentSurfaces) && this.state.actualizado &&
                                                <div className="col-12">
                                                    <div className="progress col-12 tramoJoin" style={{paddingBottom:'2px'}} onClick={() =>this.showToastLegend()}>
                                                    {this.state.html_tramosJOIN}
                                                    </div>
                                                </div>

                                            }
                                        </div>
                                    
                                        

                                    </div>   
                                }
                                    
                                      
                        
                            
                            <MapContainer center={center} zoom={10} zoomControl={false} scrollWheelZoom={true} className="h-100" whenCreated={(map) => this.setState({ map, flag: this.state.flag+1, isLoading:false })}>
                                <StreetViewControl
                                        title={"Street View"}
                                        handeDesactivateStreetView= {this.handeDesactivateStreetView.bind(this)} handeClickStreetView={this.handeClickStreetView.bind(this)}
                                        streetView= {this.state.streetView} lang={lang}/>
                                <LayersControl position="topright">
                                    <LayersControl.BaseLayer checked name="Gmaps">
                                        <TileLayer
                                        attribution='<a target="_BLANK" href="https://www.google.es/maps/preview">Google Maps</a>'
                                        url='https://mt1.google.com/vt/lyrs=r&x={x}&y={y}&z={z}'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="Gmaps Satellite">
                                        <TileLayer
                                        attribution='<a target="_BLANK" href="https://www.google.es/maps/preview">Google Maps</a>'
                                        url='https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="OpenStreetMap">
                                        <TileLayer
                                        attribution='&copy; <a target="_BLANK" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="Esri">
                                        <TileLayer
                                        attribution='Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
                                        url='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="ArcGIS">
                                        <TileLayer
                                        attribution='ArcGIS Streets'
                                        url='http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="MapBox">
                                        <TileLayer       
                                        attribution='© <a target="_BLANK" href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a target="_BLANK" href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>'                             
                                        maxZoom='24'
                                        id='mapbox/streets-v11'
                                        accessToken='pk.eyJ1IjoiaGlnaW5pbyIsImEiOiJjanN5bXR1OXAwZ3dlNDRyMnk3Mzc0emY0In0.w19t4Sn9OjLjr9qaXFfkcQ'
                                        url={"https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token="+apikey_mapbox}
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="OpenCycleMap">
                                        <TileLayer
                                        attribution='&copy; <a target="_BLANK" href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a target="_BLANK" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                        url='https://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey={apikey}'
                                        apikey='569b8242922348a2a54db0b454ef67ae'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="IGN">
                                        <WMSTileLayer
                                            attribution='&copy; <a href="https://www.ign.es/web/ign/portal">IGN</a>'
                                            projection= 'EPSG:3857'    
                                            url="https://tms-mapa-raster.ign.es/1.0.0/mapa-raster/{z}/{x}/{-y}.jpeg"
                                        />
                                    </LayersControl.BaseLayer>
                                   
                                    
                                    
                                    <LayersControl.Overlay name="routes">
                                        <TileLayer
                                        attribution='Map data: &copy; <a target="_BLANK" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | Map style: &copy; <a href="https://waymarkedtrails.org">waymarkedtrails.org</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
                                        url='https://tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png'
                                        opacity='0.5'
                                        />
                                    </LayersControl.Overlay>
                                    <LayersControl.Overlay name="googleroads">
                                        <TileLayer
                                        attribution='<a target="_BLANK" href="https://www.google.es/maps/preview">Google Maps</a>'
                                        url='https://mt1.google.com/vt/lyrs=h&x={x}&y={y}&z={z}'
                                        opacity='0.5'
                                        />
                                    </LayersControl.Overlay>
                                    <LayersControl.Overlay name="catastro">
                                        <WMSTileLayer
                                        attribution='<a target="_BLANK" href="https://www.google.es/maps/preview">Google Maps</a>'
                                        url="https://ovc.catastro.meh.es/cartografia/INSPIRE/spadgcwms.aspx"
                                        opacity='0.4'
                                        layers={'CP.CadastralParcel'}
                                        />
                                    </LayersControl.Overlay>
                                    <LayersControl.Overlay name="P.National">
                                        <WMSTileLayer
                                        attribution='Ministerio para la Transici&oacute;n Ecol&oacute;gica y el Reto Demogr&aacute;fico'
                                        url='http://sigred.oapn.es/geoserverOAPN/LimitesParquesNacionalesZPP/ows?'
                                        opacity='0.4'
                                        layers={'view_red_oapn_limite_pn'}
                                        format= {'image/png'}
                                        />
                                    </LayersControl.Overlay>



                                    
                                    
                                    {this.state.tracks.map((track, idy) => {
                                        return(      
                                            <LayersControl.Overlay key={idy} checked name={track.properties.name}>
                                                <LayerGroup>    
                                                   <GeoJSON style={TracksOptions} key={'track-'+track.properties.key} data={track}>
                                                    </GeoJSON>
                                                </LayerGroup>
                                            </LayersControl.Overlay>
                                        )
                                    })}
                                </LayersControl> 
                                <SearchControl popupFormat={({ query, result }) => result.label} searchLabel={lang.search}/>
                                <ZoomControl position="topright" zoomInText="+" zoomOutText="-" />
                                
                                
                                
                                

                                {this.state.mostrarLayers &&
                                    this.state.markers.map((marker, idx) => {
                                        var iconi;
                                        if(idx == 0){
                                            iconi = CreateIconLeaflet("moveini",16);
                                        }
                                        else if(idx == this.state.markers.length -1){
                                            iconi = CreateIconLeaflet("movefin",16);
                                        }
                                        else{
                                            iconi=CreateIconLeaflet("move",16);
                                        }
                                        return (
                                            <Marker
                                                key={"marker"+idx}
                                                ref={`marker-${idx}`} 
                                                position={marker}
                                                draggable={true}
                                                eventHandlers={{
                                                    dragend: (e) => {
                                                    this.state.map.off("click");
                                                    this.markerDragend(e, idx);
                                                    },
                                                }}
                                                icon={iconi}>
                                                {dragmarkercount < 1 && <Tooltip>{lang.tooltip_marker}</Tooltip>}
                                            </Marker>
                                        )
                                    })
                                }

                                {this.state.mostrarLayers &&
                                
                                this.state.kms.map((km, idx) => {
                                            //consoleLOG(marker)
                                            return (
                                                <Marker
                                                    key={`marker2-${idx}`} 
                                                    ref={`km-${idx}`} 
                                                    position={km}
                                                    draggable={false}
                                                    icon={IconKm(idx+1)}>
                                                </Marker>
                                            )
                                        })
                                }

                                

                                {this.state.polylines.map((polyl, idy) => {
                                    consoleLOG("GEOOOO PINTA")
                                    consoleLOG(polyl);
                                    return(                                         
                                    <GeoJSON ref={this.btnRef} style={GeoJsonOptions} eventHandlers={{
                                        click: (e) => {
                                          this.clickpolyline(e, idy);
                                        } 
                                      }} key={'geojson-'+polyl.key} data={polyl.pathSurfaces}>
                                          {/*clickpolycount < 3 && <Tooltip>{lang.tooltip_poly}</Tooltip>*/}
                                          
                                            <Popup keepInView={true} className="PopupPolyline">
                                                <ModoPolyline distance={polyl.distance} mode="edit" id={idy} travelMode={polyl.travelMode} lang={lang} directions_prov={polyl.provider} HandledirectionsProvPolyline={this.HandledirectionsProvPolyline.bind(this)} handeTravelModePolyline= {this.handeTravelModePolyline.bind(this)}></ModoPolyline>    
                                            </Popup>
                                        

                                    </GeoJSON>
                                    /*<Polyline 
                                        pathOptions={PolyOptions}
                                        positions={polyline} 
                                        eventHandlers={{
                                        click: (e) => {
                                          this.clickpolyline(e, idy);
                                        } 
                                      }}>
                                        {clickpolycount < 3 && <Tooltip>{lang.tooltip_poly}</Tooltip>}
                                    </Polyline>*/
                                    )
                                })}
                            
                            </MapContainer>
                        </div>
                   </div>

                   <div className={scrollplaces + " col-lg-3 col-12 nopadding text-center"}>
                        <div className="container-fluid nopadding">
                        <Datos  page="main" lang={lang} estado={this.state} reset={()=>this.reset()}/>  
                        <hr></hr>
                        
                        
                        
                        {this.state.polylines.length > 0 && this.state.tramosOterreno == 0 &&
                            <div className="form-group tramos" style={{maxHeight:'10rem', overflowY:"scroll" }}>
                                    <div className="btn-group" role="group" aria-label="Basic outlined example">
                                        <button type="button" onClick={()=>this.TramoOTerreno(1)} className="btn btn-outline-primary">{lang.terreno}</button>
                                        <button type="button" onClick={()=>this.TramoOTerreno(2)} className="btn btn-outline-primary">{lang.waytypes}</button>
                                        <button type="button" onClick={()=>this.TramoOTerreno(0)} className="btn btn-outline-primary active">{lang.stretchs}</button>
                                    </div>
                                    <div className="container-fluid text-center justify-content-center">
                                        {this.state.polylines.map((polyline, idy) => {
                                            consoleLOG("polyline"+ idy)
                                            return(
                                                <Tramos key={"t"+idy} lang={lang} polyline={polyline} idy={idy} showToastLegend={this.showToastLegend.bind(this)} showToastTramo={this.showToastTramo.bind(this)}/>
                                            )
                                        })}
                                    </div>
                            </div>
                        }


                        {this.state.html_tramosJOIN && HasSurfaces(percentSurfaces) && this.state.tramosOterreno == 1 &&
                            
                            <div className="form-group tramos">
                                <div className="btn-group" role="group" aria-label="Basic outlined example">
                                    <button type="button" onClick={()=>this.TramoOTerreno(1)} className="btn btn-outline-primary active">{lang.terreno}</button>
                                    <button type="button" onClick={()=>this.TramoOTerreno(2)} className="btn btn-outline-primary">{lang.waytypes}</button>
                                    <button type="button" onClick={()=>this.TramoOTerreno(0)} className="btn btn-outline-primary">{lang.stretchs}</button>
                                </div>
                                
                                <div className='row'>
                                {
                                    
                                    Object.keys(percentSurfaces).map(key => 
                                        {
                                            var val = FormatPercent(percentSurfaces[key][0]);
                                            //esto elimina que haya datos con 0%
                                            if(val){
                                                var [text, color] = GerSurfaceTextAndColorByCode(key);
                                                if(percentSurfaces[key][0] == 100)
                                                    mostrarTerrenoBajoPerfil = false;
                                                return <div  className='surfacePercent col-lg-6 col-12'>
                                                    <div style={{backgroundColor:color, cursor:'help'}} onClick={() =>this.showToastLegend()}>
                                                        {FormatPercent(percentSurfaces[key][0]) + "%"}
                                                    </div>
                                                    <label >{lang[text]}</label>                                                            
                                                    <span>{(percentSurfaces[key][1]/1000).toFixed(1)+ " km"}</span>
                                                </div>
                                                }
                                            }
                                        )
                                    }
                                </div>
                            </div>
                            
                            
                        }

                        {this.state.html_tramosJOIN && HasSurfaces(percentWaytypes) && this.state.tramosOterreno == 2 &&
                                         <div className="form-group tramos">
                                            <div className="btn-group" role="group" aria-label="Basic outlined example">
                                                <button type="button" onClick={()=>this.TramoOTerreno(1)} className="btn btn-outline-primary">{lang.terreno}</button>
                                                <button type="button" onClick={()=>this.TramoOTerreno(2)} className="btn btn-outline-primary active">{lang.waytypes}</button>
                                                <button type="button" onClick={()=>this.TramoOTerreno(0)} className="btn btn-outline-primary">{lang.stretchs}</button>
                                            </div>
                                            
                                            <div className='row'>
                                            {
                                                
                                                Object.keys(percentWaytypes).map(key => 
                                                    {
                                                        var val = FormatPercent(percentWaytypes[key][0]);
                                                        //esto elimina que haya datos con 0%
                                                        if(val){
                                                            var [text, claseBorder] = GetWaytypeTextAndColorByCode(key);
                                                            

                                                            return <div  className='surfacePercent col-lg-6 col-12' onClick={() =>this.showToastLegend()}>
                                                                <label className='DatoWaytype' style={{border:"2px "+claseBorder, cursor:'help'} }></label>
                                                                <label>{val + "%"}</label>
                                                                <label >{lang[text]}</label>                                                            
                                                                <span>{(percentWaytypes[key][1]/1000).toFixed(1)+ " km"}</span>
                                                            </div>
                                                        }
                                                    }
                                                )
                                            }
                                            </div>
                                        </div> 
                                    }

                                    

                        <div className="form-group mt-4 editor-footer">
                            <div className="container inputsRuta">
                                <input className="form-control input-route-title" onChange={e => this.handleChangeInput("titulo",e.target.value)} formcontrolname="title" type="text" placeholder={lang.titulo_placeholder}/>
                                <textarea className="form-control" rows="4" onChange={e => this.handleChangeInput("descripcion",e.target.value)} formcontrolname="descripción" type="text" placeholder={lang.desc_placeholder}></textarea>
                                
                            </div>

                    
                            <div className="mt-4 text-center editor-footer"><button onClick={()=>this.CreateRuta()} className="btn btn-secondary"><span>{lang.guardar_t}</span></button></div>
                            </div>
                        </div>
                        
                        

                        
                    </div>

                </div>
                }
                    
            </div>
            );
        
        }
}

export default Main;

/*
//AIzaSyAT7gRXBGjF9DdWUkvDdjsQcedMgwg4r3k


*/ 