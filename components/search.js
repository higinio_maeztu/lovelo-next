

import React, { Component } from 'react';
import Pagination from 'react-bootstrap/Pagination';
import { MapContainer, TileLayer, WMSTileLayer, LayersControl, Marker, Polyline,ZoomControl, Tooltip, GeoJSON, Popup,LayerGroup} from 'react-leaflet';
import 'bootstrap/dist/css/bootstrap.min.css';

import { GetUserPosition,  GetParamsUserAgent,consoleLOG, URLamigable } from '../utils/ifelse.js';
import {decodePolyline,  distanciaTramo,CalcPolyBySurfaces } from '../utils/matematicas.js'
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import { urlWS,capitalize,GetIconPinMap, PolyOptions, centerdefault, apikey_mapbox, GetIconRouteType, GetIconRouteBicicleta, GetIconRouteActivity} from "../utils/constantes";

import SearchControlHere from './main/searchcontrolhere';
import StreetViewControl from '../components/main/streetview';
import SearchControl from '../components/main/leafletgeosearch';
import RutaSearch from '../components/main/rutasearch';

import ButterToast, { Cinnamon,POS_TOP, POS_CENTER  } from 'butter-toast';
import Legend from '../components/main/legend'


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilter } from '@fortawesome/free-solid-svg-icons';
//para importar la función add desde otro archivo: import { add } from './math.js';

//esta variable la tengo que generar dinámicamente con el array PATH y los arrays de suface y waytype. Creando una feature por cada tramo distinto. Pero es una única polylinea.
//el color se le dará en función del valor de surface y waytype


const { BaseLayer } = LayersControl;
var center = centerdefault;
var userID, user;
var lang = null;

let scrollplaces = "h-100-scroll";
let userlat,userlng = null;  


class Search extends Component {
    

    constructor(props){
        super(props);

        user = props.user;
        if(user)
            userID = user.userID;

        [userlat,userlng] = GetUserPosition(props);
        if(userlat!=null && userlng !=null){
            center={
                lat:userlat,
                lng:userlng
            };  
        }
        lang = props.lang;

        this.state ={
            flag:0, //vamos a usar esta variable para renderizr la página sólo cuando queramos
            error:null,//{"message" : "vira que pedazo error"},
            isLoading: true,
            mapacargado : false,
            map:null,
            currentPosition:{lat:null,lng:null},            
            rutas:[],
            markers:[],//array de marcadores que se van pintando, arrastrando o borrando
            polylines:{},// línes que se pintan por cada tramo
            tramosDistancias:[],  //este array guarda la distancia de cada tramo.          
            actualizado : false, // =1 indica que el perfil está actualizado según los cambios. Cuando se le da al play está actualizado. Si luego se añade un nuevo marcador o se mueve algo, ya no estará actualizado
            streetView: 'hidden',
            tracks:[], //esto son los tracks que se han subido desde el dispositivo para verlos y guiarse
            pages:0,
            page:1,
            filtroBicicleta:[false,false,false],
            filtroTipo:[false,false,false],
            filtroActividad:[false,false,false],
            order:0,
            numRutas:0,
            filtrosabiertos:false
        }
        
        

        if (typeof window !== "undefined") {
            let url  = window.location.href;
            if(url.search('f=1') !== -1)
            showLegend = true;
        }
       
        this.handeDesactivateStreetView= this.handeDesactivateStreetView.bind(this);
        
    }

    shouldComponentUpdate(nextProps, nextState) {
        consoleLOG(this.state);
        consoleLOG(nextState);
        return this.state.flag != nextState.flag;
    }

    closeStreetView = (e) => {
        e.originalEvent.view.L.DomEvent.stopPropagation(e)
        e.originalEvent.preventDefault();
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {streetView:'hidden', actualizado:false, repintarPerfil:false, flag: this.state.flag+1}
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const { map } = this.state;
        if (prevState.map !== map && map) {
            map.on("click", this.closeStreetView);
            this.ListarRutas();
        }
      }

    renewClick(){
        //después de arrastrar un marcador, tenemos que establecer la posibilidad de volver a hacer click para crear uno nuevo
        this.state.map.on("click", this.closeStreetView);
    }

    
    async componentDidMount(){
        consoleLOG("didmountttt");
        
        //si hay iniciada sesión pero no tenemos guardadas en cookie su ubicación, la obtenemos
        if(userlat==null || userlng ==null){
            navigator.geolocation.getCurrentPosition(
                position => {
                        var lat = position.coords.latitude;
                        var lon = position.coords.longitude;
                        center = [lat, lon];
                        consoleLOG ("centrado" + center);
                        localStorage.setItem('lat', lat);
                        localStorage.setItem('lng', lon);
                        
                        const {map} = this.state;
                        if (map) map.flyTo(center, 10, {
                            animate: true,
                            duration: 0.5 // in seconds
                        });

                        this.ListarRutas();
                },
                error => consoleLOG(error.message),
                { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
            );
    
            
        }           

        
    }

  
    ListarRutas(page2 = 1, filtroTipo = null, filtroBicicleta = null, filtroActividad = null, order = 0){
        this.setState({ 
            isLoading: true,
            flag: this.state.flag+1,
            pages:0,
            rutas:[]
        }); 

        /**FILTROS */
        var tiposEstado = (filtroTipo)? filtroTipo : this.state.filtroTipo;
        let tipos=Number(tiposEstado[0])+""+Number(tiposEstado[1])+""+Number(tiposEstado[2]);

        var bicicletaEstado = (filtroBicicleta)? filtroBicicleta: this.state.filtroBicicleta;
        let bicicleta= Number(bicicletaEstado[0])+""+Number(bicicletaEstado[1])+""+Number(bicicletaEstado[2]);

        var actividadEstado = (filtroActividad)? filtroActividad: this.state.filtroActividad;
        let actividad= Number(actividadEstado[0])+""+Number(actividadEstado[1])+""+Number(actividadEstado[2]);

        var orderEstado = (order)? order : this.state.order;
        /**FILTROS */


        const { map } = this.state;
        var bounds = map.getBounds();
        consoleLOG(bounds);
        var latSW = bounds.getSouthWest().lat;
        var lngSW = bounds.getSouthWest().lng;
        var latNE = bounds.getNorthEast().lat;
        var lngNE = bounds.getNorthEast().lng;
        var self = this;
        let data = GetParamsUserAgent();

        
        console.log("URSWS"+ urlWS)

        fetch(urlWS+'ruta/list.php'+data+'&latSW='+latSW+'&lngSW='+lngSW+'&latNE='+latNE+'&lngNE='+lngNE+'&page='+page2+'&tipos='+tipos+"&bicicleta="+bicicleta+"&actividad="+actividad+"&order="+orderEstado)
            .then(res => res.json())
            .then(
                (respuesta) => {
                    consoleLOG(respuesta);
                    
                    var rutas = [];
                    var totalPages = respuesta.pages;
                    if(respuesta.count > 0){
                        rutas = respuesta.records;
                    }
                    
                    var polylines = {};
                    rutas.map((ruta) => {
                        polylines[ruta.rutaID] = ruta.polyline;
                    });

                    self.setState({ 
                        isLoading: false,
                        resultado:true,
                        flag: self.flag+1,
                        rutas:rutas,
                        pages:totalPages,
                        numRutas:respuesta.count,
                        page:page2,
                        polylines:polylines
                    }); 
                },
                (error) => {
                    consoleLOG("no ha entrado por donde debe")
                consoleLOG(error);
                }
        )
    }

    reset(){
        arrayPolylineaRehacer = [];
        arrayMarkerRehacer = [];
        var DatosRutaNew = {texto:"estado1",elevations: [], annotations:[],  alturamin:0, alturamax:0, ascend:0, distancia:0, descend:0,dif_altura:0,tiempo:0, coeficiente:0, categoria:0};
        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {polylines:[], markers: [], datosRuta: DatosRutaNew, tramosDistancias: [], actualizado : false, flag: this.state.flag+1}
        });
    }

    clickMarker(e, id, titulo){
        consoleLOG(id);
    }

   
     /***************************************************  PINTAR LINEAS ****************** */
     PaintPoly(path, proveedor, travelMode, waytypes = false, surfaces = false){
        var polylinePath = [];
        path.map((punto) => {
            polylinePath.push(punto); 
        });

        
        var arrayResult = CalcPolyBySurfaces(path, waytypes, surfaces);   
        //[ini1,fin1][waytype1,surface1]....[iniN,finN][waytypeN,surfaceN]
        
        
        var pathSurfaces = arrayResult[0];
        

        var array_tramos_actual = [... this.state.tramosDistancias];
        var distancia_tramo = distanciaTramo(path);
        array_tramos_actual.push(distancia_tramo);

        var polyobject = {"key":Math.random(),"pathSurfaces" : pathSurfaces, "path" : polylinePath, "provider": proveedor, "travelMode" : travelMode, "distance":distancia_tramo};
        
        const polylines = this.state.polylines;
        polylines.push(polyobject);
        //guardamos el tramo de puntos en arrayPuntos.

        this.setState((state) => {
            // Importante: lee `state` en vez de `this.state` al actualizar.
            return {polylines:polylines, tramosDistancias: array_tramos_actual, repintarPerfil:false, flag: this.state.flag+1}
        });
    }
    
    
    /****************************************************************************MODALES */
  
    
    handleChangeInput(name, value){
        //no cambiamos el flag, para que no actualice toda la página sólo para eso, no hace falta
        this.setState({
                [name]: value
        });
    }

    handeDesactivateStreetView(){
        this.setState({
            streetView:'hidden',flag: this.state.flag+1
        });
    }

    handeClickStreetView(e){
        this.state.map.off("click");
        //al principio del arrastre, deshabilitamos el click, ahora lo habilitamos, dentro de 1 segundo que ya habrá terminado la posibilidad de crear un click al sortar el arrastre
        var este = this;
        setTimeout(function() {
            este.renewClick();
        }, 1000);
        var este = this;
        var panorama = null;

        if (typeof window !== "undefined") {

            var svs = new window.google.maps.StreetViewService();
            svs.getPanorama({location: e, preference: 'nearest'}, function(data, status){
                if(data == null){
                    este.setState({
                        streetView:'error',flag: este.state.flag+1
                    });
                    return;
                }
                consoleLOG('pano1: ' + data.location.pano);
                consoleLOG('pano result: ' + data);
                var pos = data.location.latLng;
                consoleLOG(pos.toString());
                
                if(este.state.streetView != ''){
                    este.setState({
                        streetView:'',flag: este.state.flag+1
                    });
                }

                panorama = new window.google.maps.StreetViewPanorama(
                document.getElementById('pano'),
                {
                pano: data.location.pano
                });
            });

        }

              
    }

   

    showToastLegend() {
        this.OcultarButterToastAndDiv();
        ButterToast.raise({
            timeout: 3500,
            content: <Cinnamon.Crisp scheme={Cinnamon.Crisp.SCHEME_BLUE}
            content={<Legend lang={lang}/>}/>
            });
    }
    /////////////////////////////////////////////////////////////////////////////* FIN MODALES

    
    PintarRuta(rutaID){
        var tracksestado = [... this.state.tracks];        

        if(tracksestado.indexOf(rutaID) > -1){
            //si ya está pintado, lo borramos
            var index = tracksestado.indexOf(rutaID);
            if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
                tracksestado.splice(index, 1);
            }
        }
        else{
            //si no está pintado, lo pintamos
            tracksestado.push(rutaID);
        }
        
        this.setState({isLoading:false, tracks:tracksestado, flag: this.state.flag+1})
    }


    myClickHandler(object){
        var props = object.props;
        var nam = props.campo;
        var val = props.valor;
        this.setState({[nam]: val});
    }

    ClickFiltroBicicleta(index){
        var copia = [... this.state.filtroBicicleta];
        copia[index] = !this.state.filtroBicicleta[index];
        this.setState({ filtroBicicleta:copia})
        this.ListarRutas(1,null,copia, null);
    }

    ClickFiltroTipo(index){
        var copia = [... this.state.filtroTipo];
        copia[index] = !this.state.filtroTipo[index];
        this.setState({ filtroTipo:copia})
        this.ListarRutas(1, copia, null, null);
    }

    ClickFiltroActividad(index){
        var copia = [... this.state.filtroActividad];
        copia[index] = !this.state.filtroActividad[index];
        this.setState({ filtroActividad:copia})
        this.ListarRutas(1, null, null, copia);
    }

    AbrirFiltros(){
        var newestado = !this.state.filtrosabiertos;
        this.setState({ filtrosabiertos:newestado, flag:this.state.flag+1})
    }

    changeOrder(e){
        var val = e.target.value;
        this.setState({ order:val, flag:this.state.flag+1})
        this.ListarRutas(1, null,null,null, val)
    }

    handleClick(rutaID, name) {
        var url = URLamigable(name+"_"+rutaID)
        const win = window.open("./route/"+url, "_blank");
        win.focus();
    }
  
    render() {        
        consoleLOG("render main");
        //si tenemos guardada la pos en las cookies
        
        const {error, isLoading } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } 
        
        
        let divmap = " h-100 nopadding";
        if(window.innerWidth < 768)
            divmap = " h-60 nopadding";

        consoleLOG("se renderiza y el streetview es " + this.state.streetView);

        
        var finpage = (this.state.page-1)*20 + 20;
        if(this.state.page == this.state.pages){
            //estamos en la última página, hay que sumar sólo las restantes
            finpage = (this.state.numRutas%20) + (this.state.page-1) * 20;
        }
        
        const optionsPages = [];
        if(this.state.page){
            if(this.state.pages > 1 && this.state.pages <= 10){

        
                            //esto no se ve, hay que coger las 5 primeras y 5 últimas
                        Array.from(Array(this.state.pages), (e, index) => {
                            optionsPages.push (
                                <Pagination.Item
                                onClick={() => this.ListarRutas(index+1)}
                                key={index + 1}
                                active={index + 1 === this.state.page}
                                >
                                {index + 1}
                                </Pagination.Item>
                            );
                        })
                
            }
            else if(this.state.pages > 10){
            
                for (let index = 0; index < 5; index++) {                                                
                    optionsPages.push (
                        <Pagination.Item
                        onClick={() => this.ListarRutas(index+1)}
                        key={index + 1}
                        active={index + 1 === this.state.page}
                        >
                        {index + 1}
                    </Pagination.Item>
                    )
                }

                optionsPages.push ( 
                    <Pagination.Item
                    key={9999}
                    active={false}
                    >
                    ...
                </Pagination.Item>)

                for (let index2 = this.state.pages - 5; index2 < this.state.pages; index2++) {                                                
                    optionsPages.push ( 
                        <Pagination.Item
                        onClick={() => this.ListarRutas(index2+1)}
                        key={index2 +1}
                        active={index2 + 1 === this.state.page}
                        >
                        {index2 + 1}
                    </Pagination.Item>
                    )
                }
                
            }
        }


        return (        
            <div className="container-fluid h-100" lang={lang.lang}>                                     

                <div className="row h-100">
                    <div className={"col-lg-8 col-12" + divmap}>
                        <ButterToast timeout={100000} position={{vertical: POS_TOP,  horizontal: POS_CENTER }} style={{ top: '100px',  right: '100px' }}/>
                        <div className="map h-100">
                                <div id="pano" className={this.state.streetView + " streetview"} style={{position:'absolute', bottom:0, height: '50%', width:'100%', zIndex:'20000000000000000000000'}}>
                                    
                                </div>                                
                                                        
                            <MapContainer center={center} zoom={10} zoomControl={false} scrollWheelZoom={true} className="h-100" whenCreated={(map) => this.setState({ map, flag: this.state.flag+1 })}>
                                <SearchControlHere
                                        title={"Street View"} handeClickSearchHere={() =>this.ListarRutas()} lang={lang}/>
                                <StreetViewControl
                                        title={"Street View"}
                                        handeDesactivateStreetView= {this.handeDesactivateStreetView.bind(this)} handeClickStreetView={this.handeClickStreetView.bind(this)}
                                        streetView= {this.state.streetView} lang={lang}/>
                                <LayersControl position="topright">
                                    <LayersControl.BaseLayer checked name="Gmaps">
                                        <TileLayer
                                        attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                        url='https://mt1.google.com/vt/lyrs=r&x={x}&y={y}&z={z}'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="Gmaps Satellite">
                                        <TileLayer
                                        attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                        url='https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="OpenStreetMap">
                                        <TileLayer
                                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="Esri">
                                        <TileLayer
                                        attribution='Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
                                        url='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="ArcGIS">
                                        <TileLayer
                                        attribution='ArcGIS Streets'
                                        url='http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="MapBox">
                                        <TileLayer       
                                        attribution='© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>'                             
                                        maxZoom='24'
                                        id='mapbox/streets-v11'
                                        accessToken='pk.eyJ1IjoiaGlnaW5pbyIsImEiOiJjanN5bXR1OXAwZ3dlNDRyMnk3Mzc0emY0In0.w19t4Sn9OjLjr9qaXFfkcQ'
                                        url={"https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token="+apikey_mapbox}
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="OpenCycleMap">
                                        <TileLayer
                                        attribution='&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                        url='https://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey={apikey}'
                                        apikey='569b8242922348a2a54db0b454ef67ae'
                                        />
                                    </LayersControl.BaseLayer>
                                    <LayersControl.BaseLayer name="IGN">
                                        <WMSTileLayer
                                            attribution='&copy; <a href="https://www.ign.es/web/ign/portal">IGN</a>'
                                            projection= 'EPSG:3857'    
                                            url="https://tms-mapa-raster.ign.es/1.0.0/mapa-raster/{z}/{x}/{-y}.jpeg"
                                        />
                                    </LayersControl.BaseLayer>
                            
                                    
                                    <LayersControl.Overlay name="routes">
                                        <TileLayer
                                        attribution='Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | Map style: &copy; <a href="https://waymarkedtrails.org">waymarkedtrails.org</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
                                        url='https://tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png'
                                        opacity='0.5'
                                        />
                                    </LayersControl.Overlay>
                                    <LayersControl.Overlay name="googleroads">
                                        <TileLayer
                                        attribution='<a href="https://www.google.es/maps/preview">Google Maps</a>'
                                        url='https://mt1.google.com/vt/lyrs=h&x={x}&y={y}&z={z}'
                                        opacity='0.5'
                                        />
                                    </LayersControl.Overlay>
                                    <LayersControl.Overlay name="catastro">
                                        <WMSTileLayer
                                        attribution='<a target="_BLANK" href="https://www.google.es/maps/preview">Google Maps</a>'
                                        url="https://ovc.catastro.meh.es/cartografia/INSPIRE/spadgcwms.aspx"
                                        opacity='0.4'
                                        layers={'CP.CadastralParcel'}
                                        />
                                    </LayersControl.Overlay>
                                    <LayersControl.Overlay name="P.National">
                                        <WMSTileLayer
                                        attribution='Ministerio para la Transici&oacute;n Ecol&oacute;gica y el Reto Demogr&aacute;fico'
                                        url='http://sigred.oapn.es/geoserverOAPN/LimitesParquesNacionalesZPP/ows?'
                                        opacity='0.4'
                                        layers={'view_red_oapn_limite_pn'}
                                        format= {'image/png'}
                                        />
                                    </LayersControl.Overlay>

                                
                                </LayersControl> 
                                <SearchControl popupFormat={({ query, result }) => result.label} searchLabel={lang.search}/>
                                <ZoomControl position="topright" zoomInText="+" zoomOutText="-" />
                                
                                

                                {this.state.rutas.map((ruta, idx) => {
                                    var newLatLng = new L.LatLng(ruta.lat, ruta.lng);
                                    var icon = GetIconPinMap(ruta);
                                    return (
                                        <Marker
                                            key={`marker-${idx}`} 
                                            ref={`marker-${idx}`} 
                                            position={newLatLng}
                                            draggable={false}
                                            eventHandlers={{
                                                click: (e) => {
                                                this.handleClick(ruta.rutaID, ruta.titulo);
                                                },
                                            }}
                                            icon={icon}>
                                        </Marker>
                                    )
                                })
                                }

                                {consoleLOG(this.state.tracks)}

                                {this.state.tracks.map((rutaID, idy) => {
                                    consoleLOG("GEOOOO PINTA")
                                    var poly = this.state.polylines[rutaID];
        
                                    var path = decodePolyline(poly);
                                    
                                    return( 
                                        <Polyline 
                                            key={`ruta3-${idy}`} 
                                            pathOptions={PolyOptions}
                                            positions={path}>
                                        </Polyline>
                                    )
                                })}
                            
                            </MapContainer>
                        </div>
                   </div>

                   <div className={scrollplaces + " col-lg-4 col-12 nopadding"}>
                        <div className="container-fluid">

                                {isLoading &&
                                    <div className="loading_center h-100-50">
                                        <div className="spinner-grow text-primary" role="status">
                                        <span className="sr-only">Loading...</span>
                                        </div>
                                        <div className="spinner-grow text-secondary" role="status">
                                        <span className="sr-only">Loading...</span>
                                        </div>
                                        <div className="spinner-grow text-success" role="status">
                                        <span className="sr-only">Loading...</span>
                                        </div>
                                        <div className="spinner-grow text-danger" role="status">
                                        <span className="sr-only">Loading...</span>
                                        </div>
                                        <div className="spinner-grow text-warning" role="status">
                                        <span className="sr-only">Loading...</span>
                                        </div>
                                        <div className="spinner-grow text-info" role="status">
                                        <span className="sr-only">Loading...</span>
                                        </div>
                                        <div className="spinner-grow text-light" role="status">
                                        <span className="sr-only">Loading...</span>
                                        </div>
                                        <div className="spinner-grow text-dark" role="status">
                                        <span className="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                } 

                                <div className='row cabeceraSearch'>
                                    <div className='col-4 numrutas text-center'>

                                        {!isLoading && (this.state.page-1) * 20+ "-"+ finpage +lang.of +this.state.numRutas + " "+lang.routes}
                                    </div>

                                    <div className='col-4'>
                                        <select className='order form-control' id="order" onChange={(e)=>this.changeOrder(e)} value={this.state.order}>
                                            <option value="0">{lang.popularity}</option>
                                            <option value="1">{lang.distancia}</option>
                                            <option value="2">{lang.desnivel}</option>
                                        </select>
                                        <p></p>
                                        <p>{this.state.value}</p>
                                    </div>

                                    <div className='col-4'>
                                        <button onClick ={()=>this.AbrirFiltros()} title={lang.filtros} name="mode" type="button" className={(this.state.filtrosabiertos) ? "btn-info btn-sm btn-filtro filtro-active" : "btn-info btn-sm btn-filtro"} style={{marginTop:'2px'}}>
                                            <FontAwesomeIcon icon={faFilter} aria-hidden="true" size="lg"/> {lang.filtros}
                                        </button>
                                    </div>
                                </div>

                                    {this.state.filtrosabiertos &&
                                        <div className='row-fluid'>
                                            <div className="form-group">
                                                <label className='Tag3Title'>{capitalize(lang.bicicleta_t)}</label>
                                                <div className="panel panel-container menuDatos">
                                                    <div className="row nopadding">
                                                    {
                                                            Array.from(Array(3), (e, index) => {
                                                                var claseTag = (this.state.filtroBicicleta[index]) ? 'tagactive' : 'tagnoactive';
                                                                return (
                                                                    <div onClick={()=>this.ClickFiltroBicicleta(index)} className="tag3 col-lg-4 col-4 text-center">
                                                                        <div className={claseTag}>
                                                                            <FontAwesomeIcon icon={GetIconRouteBicicleta(index+1)[0]} aria-hidden="true" size="lg" className="dato-icon"/>
                                                                            <span className="d-inline-block">{lang[GetIconRouteBicicleta(index+1)[1]]}</span>
                                                                        </div>
                                                                    </div>
                                                                );
                                                            })
                                                        }
                                                    </div>         
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {this.state.filtrosabiertos && 
                                        <div className='row-fluid'>
                                            <div className="form-group">
                                            <label className='Tag3Title'>{capitalize(lang.tipo_t)}</label>
                                                <div className="panel panel-container menuDatos">
                                                    <div className="row nopadding">
                                                        {
                                                            Array.from(Array(3), (e, index) => {
                                                                var claseTag = (this.state.filtroTipo[index]) ? 'tagactive' : 'tagnoactive';
                                                                return (
                                                                    <div onClick={()=>this.ClickFiltroTipo(index)} className="tag3 col-lg-4 col-4 text-center">
                                                                        <div className={claseTag}>
                                                                            <FontAwesomeIcon icon={GetIconRouteType(index+1)[0]} aria-hidden="true" size="lg" className="dato-icon" />
                                                                            <span className="d-inline-block">{lang[GetIconRouteType(index+1)[1]]}</span>
                                                                        </div>
                                                                    </div>
                                                                );
                                                            })
                                                        }
                                                    </div>         
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {this.state.filtrosabiertos && 
                                        <div className='row-fluid'>
                                            <div className="form-group">
                                            <label className='Tag3Title'>{capitalize(lang.activity_t2)}</label>
                                                <div className="panel panel-container menuDatos">
                                                    <div className="row nopadding">
                                                        {
                                                            Array.from(Array(3), (e, index) => {
                                                                var claseTag = (this.state.filtroActividad[index]) ? 'tagactive' : 'tagnoactive';
                                                                return (
                                                                    <div onClick={()=>this.ClickFiltroActividad(index)} className="tag3 col-lg-4 col-4 text-center">
                                                                        <div className={claseTag}>
                                                                            <FontAwesomeIcon icon={GetIconRouteActivity(index+1)[0]} aria-hidden="true" size="lg" className="dato-icon" />
                                                                            <span className="d-inline-block">{lang[GetIconRouteActivity(index+1)[1]]}</span>
                                                                        </div>
                                                                    </div>
                                                                );
                                                            })
                                                        }
                                                    </div>         
                                                </div>
                                            </div>
                                        </div>
                                    }

                                <hr></hr>
                                {
                                    consoleLOG(this.state.rutas)
                                }
                                
                                {
                                this.state.rutas.map((ruta, idx) => {
                                    return (
                                        <RutaSearch key={"rs"+idx} lang={lang} ruta={ruta} PintarRuta={()=>this.PintarRuta(ruta.rutaID)}/>
                                    )
                                })
                                }
                                
                                    <div className='Pagination' style={{ display: "flex", justifyContent: "center" }}>
                                        <Pagination className="px-4">
                                            {
                                                optionsPages
                                            }
                                        </Pagination>
                                    </div>
                        </div>                        
                    </div>

                </div>
                
                    
            </div>
            );
        
        }
}

export default Search;

/*
//AIzaSyAT7gRXBGjF9DdWUkvDdjsQcedMgwg4r3k


*/ 