export class LatLng{
	constructor(lat,lng){
    	this.lat=lat;
      	this.lng=lng;
    }
	lat(){
    	return this.lat;
    }
    lng(){
    	return this.lng;
    }
}