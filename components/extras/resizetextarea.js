import React, { Component } from "react";

class ResizableTextarea extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: this.props.value,
			rows: 5,
			minRows: 5,
			maxRows: 100,
		};
	}
	
	handleChange = (event) => {
        this.props.myChangeHandler(event);
		const textareaLineHeight = 24;
		const { minRows, maxRows } = this.state;
		
		const previousRows = event.target.rows;
  	    event.target.rows = minRows; // reset number of rows in textarea 
		
		const currentRows = ~~(event.target.scrollHeight / textareaLineHeight);
    
        if (currentRows === previousRows) {
            event.target.rows = currentRows;
        }
            
		if (currentRows >= maxRows) {
			event.target.rows = maxRows;
			event.target.scrollTop = event.target.scrollHeight;
		}
    
        this.setState({
            value: event.target.value,
            rows: currentRows < maxRows ? currentRows : maxRows,
        });
	};
	
	render() {
		return (
			<textarea
				rows={this.state.rows}
				value={this.state.value}
				placeholder={this.props.placeholder}
				className={this.props.className}
				onChange={this.handleChange}
                name={this.props.name}
                style={{overflowY:'hidden'}}
			/>
		);
	}
}

export default ResizableTextarea;