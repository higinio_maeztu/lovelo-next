import React, { Component } from "react";
import { GetParamsUserAgent, Encrypt256, CheckLogin } from '../utils/ifelse.js';
import { urlWS,capitalize } from '../utils/constantes.js';
import router from 'next/router'
import Link from 'next/link'

var lang = null;
var mensaje;
var classCompleted = "alert alert-danger";
var parametros;
var user;

class Login extends Component {
    constructor(props){
        super(props);
        this.state ={
            email:null,
            password:null,
            loading:false,
            completed:false
        }

        lang = props.lang;

        console.log(router.query)
        parametros =router.query;

        user = props.user;
        if(user){
            router.push("/profile/"+user.userID);
        }       

    }

    convertToParamas(objeto){ //operations
        return {
            'email':objeto.email,
            'password':Encrypt256(objeto.password)
        }
    }


    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});   
      }

      mySubmitHandler = (event) => {
        event.preventDefault();


        //aquí debemos de llamar al ws para crear el local y pasar this.state
        var params = this.convertToParamas(this.state);
        const self = this;
        self.setState({ 
            loading:true
        });  
        
        (async () => {
            let data = GetParamsUserAgent();
            const rawResponse = await fetch(urlWS+'user/login.php'+data, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
              },
              body: JSON.stringify(params)
            });
            const response = await rawResponse.json();          
            
            var id = response.id;
            var message = lang.error;
            if(parseInt(id) > 0){
                //login correcto.
                /*self.setState({ 
                    loading:false,
                    completed:true
                });*/  
                localStorage.setItem('user', response.username);
                localStorage.setItem('userID', id);

                if(parametros.href){
                    router.push("/"+parametros.href);
                }
                else{
                    router.push("/planner")
                }
            }else{
                mensaje = message;
                self.setState({ 
                    loading:false,
                    completed:true
                });  
            }            
          })();
      }

    render() {
        if(this.state.loading == true){
            return(
            <div className="loading_center">
                <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-secondary" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-success" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-danger" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-warning" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-info" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-light" role="status">
                <span className="sr-only">Loading...</span>
                </div>
                <div className="spinner-grow text-dark" role="status">
                <span className="sr-only">Loading...</span>
                </div>
            </div>
            );
        }
        else{
        return (

            <section id="cover" className="h-100">
                    <div id="cover-caption">
                        <div className="container">
                            <div className="row text-white">
                                <div className="col-lg-4 col-8 mx-auto text-center form p-4 fondocuadro">
                                    <h1 className="display-5 py-4 text-truncate">{capitalize(lang.login)}</h1>
                                    <div className='infocontact'>{lang.loginekibike}</div>
                                    {this.state.completed === true &&
                                        <div className={classCompleted}>
                                            <div>{capitalize(lang.incorrect_user)}</div>                        
                                        </div>
                                    }
                                    <div className="px-2">
                                        
                                        <form id="loginout" onSubmit={this.mySubmitHandler} className="justify-content-center">
                                            <div className="form-group">
                                            <label>{lang.useroremail}</label>
                                                <input onChange={this.myChangeHandler} name="email" className="form-control" />
                                            </div>
                                            <div className="form-group">
                                                <label>{capitalize(lang.password)}</label>
                                                <input name="password" onChange={this.myChangeHandler}  type="password" className="form-control" />
                                            </div>
                                            <button type="submit" className="btn btn-danger btn-block margintop">{capitalize(lang.submit)}</button>
                                            <hr></hr>
                                            <div className="forgot-password">
                                                <Link href="./forgot"><a>{capitalize(lang.forgot)}</a></Link>
                                            </div>
                                            <div className="forgot-password">
                                                <Link href="./signup"><a>{capitalize(lang.notaccount)}</a></Link>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        );
        }
    }
}

export default Login;