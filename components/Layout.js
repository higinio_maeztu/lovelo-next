import { Navbar,Nav,NavDropdown } from 'react-bootstrap'
import Link from 'next/link'
import Image from 'next/image';
import { useRouter } from "next/router";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileContract, faHeart, faPencilAlt, faQuestion, faSearch, faSignInAlt, faUserCircle, faPlane} from '@fortawesome/free-solid-svg-icons'
import { NavLink } from './NavLink';
import { useCookies } from 'react-cookie';
import React, { useState } from 'react';

import Head  from 'next/head';
import { faFacebook, faInstagram } from '@fortawesome/free-brands-svg-icons';

const Layout = ({children, ...pageProps }) => {
    
    var user = pageProps.user;

    if(user)
        var userID = user.userID
    
    const router = useRouter();
    const [expanded, setExpanded] = useState(false);

    var selected = router.pathname;
    var lang = pageProps.lang;
    var langcode = pageProps.langcode;

    const [ cookie, setCookie ] = useCookies(['NEXT_LOCALE']);
    

    const switchLanguage = (e) => {
        const locale = e;
        console.warn(router.asPath)
        console.warn("vamos a " + "http://localhost:3000/"+locale+router.asPath)

        if(cookie.NEXT_LOCALE !== locale){
            setCookie("NEXT_LOCALE", locale, { path: "/" });
        }

        router.push(router.asPath, router.asPath, { locale: locale })
        //router.replace("http://localhost:3000/"+locale+router.asPath)
        //router.push(router.asPath, undefined, { locale })
        //window.location.reload()
        //router.push('/','/', { locale });
        
    }
    const isLayoutNeeded = router.pathname.includes("/iframe");

    const CerrarSesion = ()=>{
        localStorage.removeItem('user');
        localStorage.removeItem('userID');
        router.replace("/");
    }
    
    return (  
        <div className='h-100'>
            <Head>
                <title>{lang.meta_title}</title>
                <meta key="description" name="description" content={lang.meta_description} />
                <meta name="keywords" content={lang.meta_keys}/>
                <meta name="author" content="Higinio Maeztu" />
                <meta name="copyright" content="LOVELO | Higinio Maeztu" />
                <meta name="viewport" content="width=device-width, initial-scale=1"/>

                <meta name="twitter:card" content="summary" />
                <meta name="twitter:title" content={lang.meta_title}/>
                <meta name="twitter:description" content={lang.meta_description} />
                <meta key="twitter:image" name="twitter:image" content={"https://lovelo.app/images/ico.png"}/>

                 
                
                <meta key="og:title"  property="og:title" content={lang.meta_title} />
                <meta key="og:description" property="og:description" content={lang.meta_description} />
                <meta key="og:url" property="og:url" content="https://lovelo.app" />
                <meta key="og:type" property="og:type" content="activity" />
                <meta key="og:image" property="og:image" content={"https://lovelo.app/images/ico.png"}/>

                {langcode == "es" &&
                    <>
                    <link rel="canonical" href={"https://lovelo.app/es"+router.asPath} />
                    <link rel="alternate" hrefLang="en" href={"https://lovelo.app/en"+router.asPath} />
                    </>
                }
                
                {langcode != "es" &&
                    <>
                    <link rel="canonical" href={"https://lovelo.app/en"+router.asPath} />
                    <link rel="alternate" hrefLang="es" href={"https://lovelo.app/es"+router.asPath} />
                    </>
                }

                <link rel="icon" href="/images/icon.ico" />

            </Head>
            
            {!isLayoutNeeded && <Navbar expanded={expanded} className='m-auto' bg="light" variant="light" expand="lg" sticky="top">

                <Navbar.Brand>
                    <NavLink className="nav-link align-middle ml-3 text-dark" href="/" exact >
                        <div className='brand'>
                        <Image className='' width="35px" height="35px" src="/images/icon.png"/> 
                        <span className="ms-1 logo">LOVELO</span>
                        </div>
                        </NavLink>
                </Navbar.Brand>

                <Navbar.Toggle onClick={() => setExpanded(expanded ? false : "expanded")} aria-controls="basic-navbar-nav" />

                <Navbar.Collapse id="basic-navbar-nav">

                    <Nav activeKey={selected} className="container-fluid text-center">

                        
                        <NavLink className="nav-link" href="/planner"><FontAwesomeIcon size="1x"  icon={faPencilAlt}/> {lang.planner}</NavLink>
                        <NavLink className="nav-link" href="/tutorial"><FontAwesomeIcon size="1x"  icon={faQuestion}/> {lang.help}</NavLink>
                        <NavLink className="nav-link" href="/search"><FontAwesomeIcon size="1x"  icon={faSearch}/> {lang.explore}</NavLink>
                        <NavLink className="nav-link" href="https://tours.lovelo.app" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon size="1x" icon={faPlane} /> {lang.travelBlog}
                        </NavLink>
                        <NavLink className="nav-link" href="/donations"><FontAwesomeIcon size="1x"  icon={faHeart}/> {lang.dona}</NavLink>
                        <NavLink className="nav-link" href="/contact"><FontAwesomeIcon size="1x"  icon={faFileContract}/> {lang.contact}</NavLink>
                        <NavLink className="nav-link" href="https://www.instagram.com/lovelo_cicloturismo/" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon size="1x" icon={faInstagram} /> 
                        </NavLink>
                        <NavLink className="nav-link" href="https://www.facebook.com/loveloapp" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon size="1x" icon={faFacebook} />
                        </NavLink>


                       
                        {langcode == 'es' && <NavLink className="ms-lg-auto nav-link" href="#" onClick={()=>switchLanguage("en")}>ENGLISH</NavLink>}
                        {langcode == 'en' && <NavLink className="ms-lg-auto nav-link" href="#" onClick={()=>switchLanguage("es")}>ESPAÑOL</NavLink>}
                        
                        
                        {userID && 
                            <NavDropdown title={<> {user.username} <FontAwesomeIcon size="1x" icon={faUserCircle} /></>} id="basic-nav-dropdown">
                                <NavDropdown.Item ><NavLink className="dropdown-itemcolor" href={"/profile/"+userID}>{lang.profile}</NavLink></NavDropdown.Item>
                                <NavDropdown.Item ><NavLink className="dropdown-itemcolor" href={"/profile/"+userID}>{lang.your_routes}</NavLink></NavDropdown.Item>
                                <NavDropdown.Item href="#"  onClick={() => {CerrarSesion()}}>{lang.logout}</NavDropdown.Item>
                            </NavDropdown>
                        }
                        {!userID &&
                            <NavLink className="nav-link" href="/login"><FontAwesomeIcon icon={faSignInAlt}/></NavLink>
                        }

                    </Nav>

                    

                </Navbar.Collapse>

            </Navbar>}
        {children}
        </div>
    );
}
 
export default Layout;