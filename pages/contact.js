import { useState } from 'react'
import { capitalize } from '../utils/constantes';

export default function Home(props) {
  var lang = props.lang;
	const [inputs, setInputs] = useState({
		name: '',
		email: '',
		message: '',
	})

	const [form, setForm] = useState('')

	const handleChange = (e) => {
		setInputs((prev) => ({
			...prev,
			[e.target.id]: e.target.value,
		}))
	}

	const onSubmitForm = async (e) => {
		e.preventDefault()

		if (inputs.name && inputs.email && inputs.message) {
			setForm({ state: 'loading' })
			try {
				const res = await fetch("/api/contact", {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(inputs),
				})

				const { error } = await res.json()

				if (error) {
					setForm({
						state: 'error',
						message: error,
					})
					return
				}

				setForm({
					state: 'success',
					message: 'Your message was sent successfully.',
				})
				setInputs({
					name: '',
					email: '',
					message: '',
				})
			} catch (error) {
				setForm({
					state: 'error',
					message: lang.error,
				})
			}
		}
	}
	return (
    <section id="cover" className="h-100-50">
                    <div id="cover-caption">
                        <div className="container">
                            <div className="row text-white">
                                <div className="col-lg-4 col-8 mx-auto text-center form p-4 fondocuadro">
                                    <h1 className="display-5 py-4 text-truncate">{lang.contact}</h1>
                                    {form.state === 'loading' ? (
                                        <div className='infocontact'>{lang.sending}</div>
                                      ) : form.state === 'error' ? (
                                        <div className='infocontact'>{form.message}</div>
                                      ) : (
                                        form.state === 'success' && <div className='infocontact'>{lang.comment_sent}</div>
                                    )}
                                    <div className="px-2">
                                        
                                    <form className="form2" onSubmit={(e) => onSubmitForm(e)}>
                                      <input
                                        id='name'
                                        type='text'
                                        value={inputs.name}
                                        onChange={handleChange}
                                        className="form-group"
                                        placeholder={lang.name}
                                        required
                                      />
                                      <input
                                        id='email'
                                        type='email'
                                        value={inputs.email}
                                        onChange={handleChange}
                                        className="form-group"
                                        placeholder='Email'
                                        required
                                      />
                                      <textarea
                                        id='message'
                                        type='text'
                                        value={inputs.message}
                                        onChange={handleChange}
                                        className="form-group"
                                        placeholder={lang.message}
                                        rows='5'
                                        required
                                      />

                                      <button type="submit" className="btn btn-danger btn-block margintop w-100">{capitalize(lang.submit)}</button>
                                     
                                    </form>
                                    </div>

                                    <hr className='hrlogin'></hr>
                                    <div className="forgot-password2">
                                        <a href='https://www.facebook.com/loveloapp' target="_blank" rel="noreferrer">{lang.sentfacebook}</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
	)
}