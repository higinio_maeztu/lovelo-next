import React from 'react';
import dynamic from 'next/dynamic';


const EditRoute = dynamic(
    () => {
      return import("../../components/editruta");
    },
    { ssr: false }
  );



export default function Edit({ data, ...pageProps }) {
    var lang = pageProps.lang;
        
    var user = pageProps.user;
    

    var ruta = data;
    ruta.anns = ruta.annotations;

    return (        
        <div className="h-100">
            
                <EditRoute ruta={ruta} lang={lang} user={user}/>
                
              
        </div>
            );
  }

  Edit.auth = true
  

  export async function getServerSideProps(context) {
    const { slug } = context.query;
    

    const lastIndex = slug.lastIndexOf('_');
    const id = slug.slice(lastIndex + 1);

    //console.warn("EL edit.ID es "+ id)
    const res = await fetch(process.env.urlWS+'ruta/read.php?id='+id)
    const data = await res.json();

    return {
      props: { data }
    };
  }