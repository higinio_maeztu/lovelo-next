import {consoleLOG, locales, URLamigable} from '../../utils/ifelse'
import React, { useState } from 'react';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { baseurl } from '../../utils/constantes';


const languages = {
  en: require('../../locale/en.json'),
  es: require('../../locale/es.json')
};


const MapRoute = dynamic(
    () => {
      return import("../../components/maproute");
    },
    { ssr: false }
  );



export default function Profile({ data, locale, ...pageProps }) {
    const { asPath, pathname } = useRouter();
    var lang = languages[locale];
   
    //console.warn(locale)
    var user = pageProps.user;
    
    var ruta = data;
    if(!ruta)
      return;

    ruta.anns = ruta.annotations;
    return (        
        <div className="h-100">
                <Head>
                  <title>{ data.titulo }</title>
                  <meta  key="description" name="description" content={data.descripcion} />

                  <meta name="twitter:card" content="summary" />
                  <meta name="twitter:title" content={"Lovelo | "+data.titulo}/>
                  <meta name="twitter:description" content={data.descripcion} />
                  {data.images.length > 0 &&<meta key="twitter:image" name="twitter:image" content={baseurl+data.images[0].image}/>}

                  <meta key="og:title" property="og:title" content={"Lovelo | "+data.titulo} />
                  <meta key="og:description" property="og:description" content={data.descripcion} />
                  <meta key="og:url" property="og:url" content={"https://lovelo.app/"+locale+asPath} />
                  {data.images.length > 0 && <meta key="og:image" property="og:image" content={baseurl+data.images[0].image}/>}
                </Head>
                <MapRoute ruta={ruta} lang={lang} user={user}/>
                
              
        </div>
            );
  }
  

  export async function getStaticPaths() {
    const res = await fetch(process.env.urlWS+'ruta/listall.php');
    const posts = await res.json();
    //console.warn(posts);
    var paths = [];
    /*posts.map((post) => {
      for (const locale of locales) {
        var amigable = URLamigable(post);
        if(typeof variable !== 'object'){
          paths.push({
            params: {
              slug: amigable,
            }
          });
        }
        consoleLOG("El route.id es "+URLamigable(post));
      }
    });   */
  
    return {
      paths,
      fallback: "blocking"
    };
  }
  
  export async function getStaticProps({ params, locale }) {
    const { slug } = params;

    const lastIndex = slug.lastIndexOf('_');
    const id = slug.slice(lastIndex + 1);

    /*if(!Number.isInteger(id)){
      return {props: {}}
    }*/

    console.warn("EL ID es "+ id )
    const res = await fetch(process.env.urlWS+'ruta/read.php?id='+id)
    const data = await res.json();

    if (!data) return null;
    //console.warn(data)
  
    return {
      props: { data , locale},
      revalidate: 1,
    };
  }