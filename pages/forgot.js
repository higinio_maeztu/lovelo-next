import { useRouter } from 'next/router'
import { useState, useRef } from 'react'
import { signOut, useSession } from 'next-auth/react'
import {capitalize,urlWS } from '../utils/constantes'
import {consoleLOG, Encrypt256,GetParamsUserAgent } from '../utils/ifelse'
import Link from 'next/link'
import { convertToParamas } from '../utils/matematicas'
var classCompleted = "alert alert-danger";
var mensaje = '';

const Forgot = (props) => {
    var lang = props.lang;
    var langcode = props.langcode;

    var user = props.user;
    var userID;
    if(user)
        userID = user.userID;

    const router = useRouter()
    const [error, setError] = useState(false)
    const passwordEmail = useRef()
    const [isLoading, setLoading] = useState(false);
    const [completed , setC] = useState(false);
    const [resultado, setR] = useState(false);

    if ((typeof window === undefined)) {
      router.push('/')
    }

  
    const mySubmitHandler = (event) => {
    event.preventDefault();
    
    //var params  = (userID, suit) => { return { userID: userID, suit: suit } }

    
    //aquí debemos de llamar al ws para crear el local y pasar this.state
    
    const params = {
      'usernamemail':passwordEmail.current.value
    };
    
    (async () => {
      consoleLOG(params)
        setLoading(true)
        consoleLOG(urlWS+'user/generatepass.php');
        let data = GetParamsUserAgent(userID);
        const rawResponse = await fetch(urlWS+'user/generatepass.php'+data, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
          },
          body: JSON.stringify(params)
        });
        const response = await rawResponse.json();          
        var id = response.id;
        if(parseInt(id) > 0){
            mensaje = lang.pass_changed;
            classCompleted = "alert alert-success";
            signOut({ redirect: false })
            setLoading(false)
            setC(true)
            setR(true)
        }
        else{
            mensaje = lang.incorrect_useremail;
            setLoading(false)
            setC(true)
            setR(false)
        }            
      })();
  }

  if(isLoading){
    return(
    <div className="loading_center">
        <div className="spinner-grow text-primary" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-secondary" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-success" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-danger" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-warning" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-info" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-light" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-dark" role="status">
        <span className="sr-only">Loading...</span>
        </div>
    </div>
    );
}else if(completed && !isLoading && resultado){
        classCompleted = "alert alert-success";
        return(
          <div className="container h-100">
              <div className='row'>
                  <div className={" ms-auto me-auto mt-4 col-lg-4 col-12 fondocuadro "+classCompleted}>
                      <div className='text-center'>
                          {mensaje}
                      </div>
                      <div className="text-center margintop row">
                          <div className='col-6'><Link  href="/" ><a className="alert-link">{lang.home}</a></Link></div>
                          <div className='col-5 col-offset-1'><Link  href={"/login"}><a className="alert-link">{capitalize(lang.login)}</a></Link></div>
                      </div> 
                  </div>   
              </div>
          </div>
          );
}
else{
  return (
    
         <section id="cover" className="h-100-50">
                    <div id="cover-caption">
                        <div className="container">
                            {completed && !isLoading && !resultado &&
                              <div className='row'>
                                <div className={" ms-auto me-auto mt-4 col-lg-4 col-12  "+classCompleted}>
                                    <div className='text-center'>
                                        {mensaje}
                                    </div>
                                </div>   
                              </div>
                            }
                            <div className="row text-white">
                                <div className="col-lg-4 col-8 mx-auto text-center form p-4 fondocuadro">
                                    <h1 className="forgot py-4">{capitalize(lang.forgotpass)}</h1>
                                    <div className="px-2">
                                        
                                      <form id="loginout" onSubmit={mySubmitHandler} className="justify-content-center">
                                            <div className="form-group">
                                                <input ref={passwordEmail} name="usernamemail" required type="usernamemail" className="form-control" placeholder={lang.enter+" "+lang.usernamemail}/>
                                            </div>
                                            <button type="submit" className="btn btn-danger btn-block margintop">{capitalize(lang.generapass)}</button>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
  )
      }


}

export default Forgot