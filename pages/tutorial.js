import { Accordion , Item } from 'react-bootstrap';

function Tutorial(props) {
  
    
    var lang = props.lang;
    var langcode = props.langcode;
    var userID = null;//tenemos qeu cogerlo de la sesión
    
    return(
        <div className="container home">                       
                        
            <div className="row">
                <div className="col-12 mx-auto text-center form">
                    <h1 className="lovelo_home h1_dona">{lang.faqs}</h1>                                        
                </div>
            </div>
            
            <Accordion defaultActiveKey="-1">
                <Accordion.Item eventKey="0">
                    <Accordion.Header>{lang.faqs1}</Accordion.Header>
                    <Accordion.Body>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/x3FS2xxJUg8" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                    <Accordion.Header>{lang.faqs2}</Accordion.Header>
                    <Accordion.Body>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/9RvyP3bmFA0" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                    <Accordion.Header>{lang.faqs3}</Accordion.Header>
                    <Accordion.Body>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/S0xNOHp9hZc" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="3">
                    <Accordion.Header>{lang.faqs4}</Accordion.Header>
                    <Accordion.Body>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/G66RDrgb8e4" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>

            <div className="row mt-5 news">
                <div className="col-12 mx-auto text-center form">
                    <h1 className="lovelo_home h1_dona">{lang.news}</h1>                                        
                </div>
                <div className='row'>
                    <div className='col'>
                        <ul>
                            <li>{lang.new21}</li>
                            <li>{lang.new3}</li>
                            <li>{lang.new31}</li>
                            <li>{lang.new4}</li>
                            <li>{lang.new5}</li>
                            <li>{lang.new6}</li>
                            <li>{lang.new7}</li>
                            <li>{lang.new8}</li>
                            <li>{lang.new2}</li>
                            <li>{lang.new10}</li>
                            <li>{lang.new9}</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
      );
    
  }
  
  export default Tutorial;


