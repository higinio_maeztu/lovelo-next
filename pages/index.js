import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPlay, faSearch, faFileContract, faQuestion, faEdit } from '@fortawesome/free-solid-svg-icons'
import { useSession } from 'next-auth/react'

function Home(props) {
    if(props.user)
        var userID = props.user.userID;
    
    var lang = props.lang;
    var langcode = props.langcode;
    
    return(
        <div className='h-100-50'>
            <section id="cover" className="lovelo_slogan">
                <div id="cover-caption" className='my-auto'>
                    <div className="container">
                        <div className="row">
                            <div className="col-12 mx-auto text-center form p-4">
                                <h1 className="slogan">{lang.lovelo_slogan}</h1>                                        
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 mx-auto text-center form p-4">
                                <h2 className="slogan">{lang.lovelo_encuentra}</h2>    
                            </div>
                        </div>
                        <div className='row mt-5 '>
                            <div className="col-6 col-lg-2 ms-auto me-auto text-center ">
                                    {userID && <Link href="/planner"><button type="button" className="btn btn-danger btn-block w-100">{lang.start}</button></Link>}
                                    {!userID && <Link href="/login"><button type="button" className="btn btn-danger btn-block w-100">{lang.free}</button></Link>}
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                    <div className="container home">
                        <div className="row">
                            <div className="col-12 mx-auto text-center form">
                                <h1 className="lovelo_home h1_home">{lang.new}</h1>                                        
                            </div>                                
                            <div className="col-lg-12 col-12 mx-auto text-center form p-4">
                                <h2 className="lovelo_home">{lang.new1}</h2> 
                                <Link href={"/tutorial"}><a>{lang.news}</a></Link>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-12 mx-auto text-center form">
                                <h1 className="lovelo_home h1_home">{lang.lovelo_precisionperfiles}</h1>                                        
                            </div>                                
                            <div className="col-lg-12 col-12 mx-auto text-center form p-4">
                                <h2 className="lovelo_home">{lang.lovelo_home}. {lang.lovelo_precision}<a target="_blank" href="https://www.youtube.com/watch?v=FNYU4YX-BpE" rel="noopener noreferrer"><FontAwesomeIcon icon={faPlay} aria-hidden="true" size="sm"/></a></h2>                                        
                            </div>       
                            <div className="col-lg-12 col-12 mx-auto text-center form">
                                <img width="100%" src="/images/palomas.png"/>                                        
                            </div>
                        </div>
                        
                        <div className="row">
                            <div className="col-12 mx-auto text-center form">
                                <h1 className="lovelo_home h1_home">{lang.lovelo_planificador}</h1>                                        
                            </div>
                            <div className="col-12 mx-auto text-center form p-4">
                                <h2 className="lovelo_home">{lang.lovelo_superficies}</h2>                                        
                            </div>         
                            <div className="col-12 mx-auto text-center form pb-4">
                                <Link href="./planner"><a><FontAwesomeIcon size="1x"  icon={faEdit}/> {lang.planner}</a></Link>
                            </div>                         
                            <div className="col-12 mx-auto text-center form">
                                {langcode == 'en' && <img  src="/images/planificador_en.png"/>}
                                {langcode == 'es' && <img  src="/images/planificador.png"/>}
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12 mx-auto text-center form">
                                <h1 className="lovelo_home h1_home">{lang.lovelo_inspiracion}</h1>                                        
                            </div>
                            <div className="col-12 mx-auto text-center form p-4">
                                <h2 className="lovelo_home">{lang.lovelo_inspiracion2}</h2>                                        
                            </div>
                            <div className="col-12 mx-auto text-center form">
                                <Link href="/search"><a><FontAwesomeIcon size="1x"  icon={faSearch}/> {lang.explore}</a></Link>
                            </div>
                            
                        </div>

                        <div className="row">
                            <div className="col-12 mx-auto text-center form">
                                <h1 className="lovelo_home h1_home">{lang.lovelo_ayuda}</h1>                                        
                            </div>
                            <div className="col-12 mx-auto text-center form p-4">
                                <h2 className="lovelo_home">{lang.lovelo_ayuda2}</h2>                                        
                            </div>
                            <div className="col-3 mx-auto text-center form">
                                <Link href="/tutorial"><a><FontAwesomeIcon size="1x"  icon={faQuestion}/> {lang.help}</a></Link>
                            </div>
                            <div className="col-3 mx-auto text-center form">
                                <Link href="/contact"><a><FontAwesomeIcon size="1x"  icon={faFileContract}/> {lang.contact}</a></Link>
                            </div>
                        </div>
                        
                        <br></br>
                        
                    </div>
                </section>
            

        </div>
    );
  
}

export default Home;