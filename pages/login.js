import dynamic from 'next/dynamic'

const DynamicComponentWithCustomLoading = dynamic(
  () => import('../components/login'),
  { ssr: false, loading: () => <div className="loading_center">
  <div className="spinner-grow text-primary" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-secondary" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-success" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-danger" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-warning" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-info" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-light" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-dark" role="status">
  <span className="sr-only">Loading...</span>
  </div>
</div> }
)

function Home(props) {
  var user = props.user;

  return (
    <div className='h-100-50'>
      <DynamicComponentWithCustomLoading   lang={props.lang} langcode={props.langcode} user={user}/>
    </div>
  )
}

export default Home
