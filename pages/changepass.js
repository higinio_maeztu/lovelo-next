import { useState, useRef } from 'react'
import { signOut } from 'next-auth/react'
import {capitalize,urlWS } from '../utils/constantes'
import {consoleLOG, Encrypt256,GetParamsUserAgent } from '../utils/ifelse'
import Link from 'next/link'
import router from 'next/router'
var classCompleted = "alert alert-danger";
var mensaje = '';

const ChangePass = (props) => {
    var lang = props.lang;
    var langcode = props.langcode;

    var user = props.user;
    var userID;
    if(user)
        userID = user.userID;
    
    const [error, setError] = useState(false)
    const passwordRef = useRef()
    const passwordRef1 = useRef()
    const passwordRef2 = useRef()
    const [isLoading, setLoading] = useState(false);
    const [completed , setC] = useState(false);
    const [resultado, setR] = useState(false);

  
    const mySubmitHandler = (event) => {
    event.preventDefault();

    if(passwordRef1.current.value != passwordRef2.current.value){
        alert("Las contraseñas deben ser iguales");
        return;
    }
    
    //var params  = (userID, suit) => { return { userID: userID, suit: suit } }

    
    //aquí debemos de llamar al ws para crear el local y pasar this.state
    
    const params = {
      userID:userID,
      password:Encrypt256( passwordRef.current.value),
      passwordnew:Encrypt256( passwordRef1.current.value)
    };
    
    (async () => {
      consoleLOG(params)
        setLoading(true)
        consoleLOG(urlWS+'user/cahngepass.php');
        let data = GetParamsUserAgent(userID);
        const rawResponse = await fetch(urlWS+'user/changepass.php'+data, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
          },
          body: JSON.stringify(params)
        });
        const response = await rawResponse.json();          
        var id = response.id;
        var message = lang.password_no;
        if(parseInt(id) > 0){
            mensaje = lang.pass_changed2;
            classCompleted = "alert alert-success";
            signOut({ redirect: false })
            setLoading(false)
            setC(true)
            setR(true)
        }
        else{
            mensaje = message;
            setLoading(false)
            setC(true)
            setR(false)
        }            
      })();
  }

  if(isLoading){
    return(
    <div className="loading_center">
        <div className="spinner-grow text-primary" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-secondary" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-success" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-danger" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-warning" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-info" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-light" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-dark" role="status">
        <span className="sr-only">Loading...</span>
        </div>
    </div>
    );
}else if((completed && !isLoading && resultado) ||!user){
        classCompleted = "alert alert-success";
        return(
          <div className="container h-100">
              <div className='row'>
                  <div className={" ms-auto me-auto mt-4 col-lg-4 col-12 "+classCompleted}>
                      <div className='text-center'>
                          {mensaje}
                      </div>
                      <div className="text-center margintop row">
                          <div className='col-6'><Link  href="/" ><a className="alert-link">{lang.home}</a></Link></div>
                          <div className='col-5 col-offset-1'><Link  href={"/login"}><a className="alert-link">{capitalize(lang.login)}</a></Link></div>
                      </div> 
                  </div>   
              </div>
          </div>
          );
}
else{
  return (
    <>
      {user && (
        <>
         <section id="cover" className="h-100-50">
                    <div id="cover-caption">
                        <div className="container">
                            {completed && !isLoading && !resultado &&
                              <div className='row'>
                                <div className={" ms-auto me-auto mt-4 col-lg-4 col-12 "+classCompleted}>
                                    <div className='text-center'>
                                        {mensaje}
                                    </div>
                                </div>   
                              </div>
                            }
                            <div className="row text-white">
                                <div className="col-lg-4 col-8 mx-auto text-center form p-4 fondocuadro">
                                    <h1 className="display-5 py-4 text-truncate">{capitalize(lang.change_pass)}</h1>
                                    <div className="px-2">
                                        
                                        <form id="loginout" onSubmit={mySubmitHandler} className="justify-content-center">
                                            
                                            <div className="form-group">
                                                <label>{capitalize(lang.password)}</label>
                                                <input type="password" ref={passwordRef} required name="password" className="form-control" placeholder={lang.password} />
                                            </div>
                                            <div className="form-group">
                                                <label>{capitalize(lang.new_password)}</label>
                                                <input type="password" pattern=".{6,}" ref={passwordRef1} required name="password1" className="form-control" placeholder={lang.enter+" "+lang.password+" "+lang.six_char} />
                                            </div>
                                            <div className="form-group">
                                                <label>{capitalize(lang.new_password)}</label>
                                                <input type="password" pattern=".{6,}" ref={passwordRef2} required name="password2" className="form-control" placeholder={lang.repeat+" "+lang.password} />
                                            </div>
                                            <button type="submit" className="btn btn-danger btn-block margintop">{capitalize(lang.submit)}</button>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        </>
      )}
    </>
  )
      }


}

export default ChangePass