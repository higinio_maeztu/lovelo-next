import React from 'react';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { baseurl } from '../../utils/constantes';

const MapRoute = dynamic(
    () => {
      return import("../../components/mapiframe");
    },
    { ssr: false}
  );



export default function Profile({ data, ...pageProps }) {
    const { asPath, pathname } = useRouter();
    var lang = pageProps.lang;
    
    var user = pageProps.user;
    var userID = '';
    if(user)    
        userID = pageProps.user.userID;

    var ruta = data;
    ruta.anns = ruta.annotations;
    return (        
        <div className="h-100">
                               
                <MapRoute ruta={ruta} lang={lang}/>
                
              
        </div>
            );
  }
  

  export async function getServerSideProps(context) {
    const { slug } = context.query;
    //console.warn("EL ID es "+ slug)
    const id = slug;

    const res = await fetch(process.env.urlWS+'ruta/read.php?id='+id)
    const data = await res.json();

    return {
      props: { data }
    };
  }