import '../styles/globals.css'
import '../styles/index.css'
import '../styles/search.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'react-responsive-modal/styles.css';
import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'
config.autoAddCss = false
import Layout from '../components/Layout'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import Script from 'next/script'
import { CookieBanner } from "react-cookie-law-customizable";
import {CheckLogin} from '../utils/ifelse'

const languages = {
  en: require('../locale/en.json'),
  es: require('../locale/es.json')
};


export default function MyApp({ Component, pageProps: pageProps }) {
  
  const router = useRouter()
  const { locale, defaultLocale } = router;
  const messages = languages[locale];
  var user = CheckLogin();
  
  return (
    <>
    
    <Script id="gmapsapi" aysnc defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGH5LzLTMLHPP7ICKBBIvYjzQYvZLc5gU&v=3.exp" />
    <Script id="jsapi"  src="https://www.google.com/jsapi"/>

    <Script
        id="gtm-script" 
        strategy="lazyOnload"
        src={`https://www.googletagmanager.com/gtag/js?id=G-RDR4KC0FNN`}
      />

      <Script id="gtm-script2"  strategy="lazyOnload">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-RDR4KC0FNN');
        `}
      </Script>



    {Component.auth ? (
          <Auth>         
            <div className='cookiesdiv'>
              <CookieBanner
                    styles={{
                        dialog: { backgroundColor: "#ddd",bottom:"0",zIndex:"200000" }
                    }}
                    message={messages.cookies}
                    onAccept={() => {}}
                    showStatisticsOption = {false}
                    showMarketingOption = {false}
                    showPreferencesOption ={false}
                    necessaryOptionText = "Necessary cookies"
                    policyLink ="cookies.html"
                    />
          </div> 
            <Layout user={user} lang={messages} langcode={locale}>
              <Component user={user} lang={messages} langcode={locale} {...pageProps} />
            </Layout>
            
          </Auth>
        ) : (
          <Layout user={user} lang={messages} langcode={locale}>
            <Component user={user} lang={messages} langcode={locale} {...pageProps} />
          </Layout>
        )}
    </>
  )
}

function Auth({ children }) {
  const router = useRouter()
  
  var user = CheckLogin();
  
  const isUser = !!user?.userID
  useEffect(() => {
    if (!isUser) router.push('/login') //Redirect to login
  }, [isUser, user])

  
  if (isUser) {
    return children
  }
  return null;
  // Session is being fetched, or no user.
  // If no user, useEffect() will redirect.
  /*return <div className="loading_center mano">
  <div className="spinner-grow text-primary" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-secondary" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-success" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-danger" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-warning" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-info" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-light" role="status">
  <span className="sr-only">Loading...</span>
  </div>
  <div className="spinner-grow text-dark" role="status">
  <span className="sr-only">Loading...</span>
  </div>
</div>*/
}