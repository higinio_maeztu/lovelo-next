function Donations(props) {
  
    
    var lang = props.lang;
    var langcode = props.langcode;
    var userID = null;//tenemos qeu cogerlo de la sesión
    
    return(
                    <div className="container home">                       
                        
                        <div className="row">
                            <div className="col-12 mx-auto text-center form">
                                <h1 className="lovelo_home h1_dona">{lang.dona}</h1>                                        
                            </div>
                            <div className="col-12 mx-auto text-center form p-4">
                                <h3 className="lovelo_home">{lang.textoDona}</h3>                                        
                            </div>

                            <div className="col-12 mx-auto text-center form p-4">
                                <h3 className="lovelo_home"><b>{lang.banktransfer}</b></h3>   
                                <h3>HIGINIO MAEZTU HERRERA<br></br>
                                    BIC: CAIXESBBXXX<br></br>
                                    IBAN ES57 2100 4015 3422 0008 0727
                                </h3>                                     
                            </div>

                            <div className="col-12 mx-auto text-center form p-4">
                                <h3 className="lovelo_home"><b>Bizum</b></h3>   
                                <h3>HIGINIO MAEZTU HERRERA<br></br>
                                    +34659266983
                                </h3>                                     
                            </div>
                            

                            <div className="col-12 mx-auto text-center form p-4">
                                <h3 className="lovelo_home"><b>Paypal</b></h3> 
                                
                                ({lang.comission})
                                <br></br>  <br></br>
                                
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                    <input type="hidden" name="cmd" value="_s-xclick"/>
                                    <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHfwYJKoZIhvcNAQcEoIIHcDCCB2wCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAsZ0qeIZXNRGlE0Oh7YK7OliGVeuWpNTL7HuNrJCeRa9w16vE9X+EX2IxkUrKiALl9xFRLNu3gZtg4Ln7gXn7pxTct7aw5X4ngrvMqlmwJsQU+kQw0uHrss5sQNBQeITBk6xh46xuwC8T0Yx0knvnEC8WJ1O0a15fUtOBspRQj+TELMAkGBSsOAwIaBQAwgfwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIVNTACbojfIGAgdhFmfL4+RpXc203T5i2LfxEhdmji+cXVkCavKF4VDUqRFbEzUuwH8UcmlSbdvdN2JBo/95lV+NDwxmdxeeVMjuJJOorenX3b+QzqgfouLbT3UtXOjVRa4DTU0aXa241pFcCAD5m8EkYfBpS0cNLOvmuYZrL1jOtAtUYCBBh2ydGMAtwjKp2GUVQxkoLZYbR7/YE+lr8okQ4Bz2knS/VqTUVgM4pozVsm+e6bxGBnmnfW13uwUwsYeXmJRof2eh/d7uS7Cv6fE//Bhv/ggcBPJxFeT8Eb1Qc/+GgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xMjA1MDMxODIzMjlaMCMGCSqGSIb3DQEJBDEWBBTdhWZHBk5SOzYSxRsow8HDWqYN4jANBgkqhkiG9w0BAQEFAASBgDC94NRAqCsCR1k0gt7LK5ST9fUrs6idGvlghuSPdShLKvp2CJ3k+zCJCJLhEKhRii0rAtS2W+Ykfn/v2pwsjooohbNpBVPDDVs419v5irVjg9fxcAa9n3U/h69X1jBMKCKYIs4fbzBEmqBEVDgyloO/TbqEPHTGdff7pKcSSaax-----END PKCS7-----"/>
                                    <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal. La forma r�pida y segura de pagar en Internet."/>
                                    <img alt="" border="0" src="https://www.paypalobjects.com/es_ES/i/scr/pixel.gif" width="1" height="1"/>
                                </form>
                            </div>

                            
                        </div>

                        
                        
                    </div>
      );
    
  }
  
  export default Donations;