  import dynamic from 'next/dynamic'

  const DynamicComponentWithCustomLoading2 = dynamic(
    () => import('../components/main'),
    { ssr: false, loading: () => <div className="loading_center">
    <div className="spinner-grow text-primary" role="status">
    <span className="sr-only">Loading...</span>
    </div>
    <div className="spinner-grow text-secondary" role="status">
    <span className="sr-only">Loading...</span>
    </div>
    <div className="spinner-grow text-success" role="status">
    <span className="sr-only">Loading...</span>
    </div>
    <div className="spinner-grow text-danger" role="status">
    <span className="sr-only">Loading...</span>
    </div>
    <div className="spinner-grow text-warning" role="status">
    <span className="sr-only">Loading...</span>
    </div>
    <div className="spinner-grow text-info" role="status">
    <span className="sr-only">Loading...</span>
    </div>
    <div className="spinner-grow text-light" role="status">
    <span className="sr-only">Loading...</span>
    </div>
    <div className="spinner-grow text-dark" role="status">
    <span className="sr-only">Loading...</span>
    </div>
</div> }
  )

  function Home(props) {
    

    return (
      <div className='h-100-50'>
        <DynamicComponentWithCustomLoading2 user={props.user}  lang={props.lang} langcode={props.langcode}/>
      </div>
    )
  }

  export default Home

  Home.auth = true