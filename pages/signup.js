import { useRouter } from 'next/router'
import { useState, useRef } from 'react'
import { signOut, useSession } from 'next-auth/react'
import {capitalize,urlWS } from '../utils/constantes'
import {consoleLOG, Encrypt256,GetParamsUserAgent } from '../utils/ifelse'
import Link from 'next/link'
import { convertToParamas } from '../utils/matematicas'
var classCompleted = "alert alert-danger";
var mensaje = '';

const SignUp = (props) => {
    var lang = props.lang;
    var langcode = props.langcode;

    var user = props.user;
    
    const router = useRouter()
    const [error, setError] = useState(false)
    const usernameRef  = useRef()
    const emailRef  = useRef()
    const passwordRef1 = useRef()
    const passwordRef2 = useRef()
    const [isLoading, setLoading] = useState(false);
    const [completed , setC] = useState(false);
    const [resultado, setR] = useState(false);

    //Si aún no ha cargado el client side o el usuario está conectado
    if ((typeof window === undefined) || user) {
      router.push('/')
    }

  
    const mySubmitHandler = (event) => {
    event.preventDefault();

    if(passwordRef1.current.value != passwordRef2.current.value){
        alert("Las contraseñas deben ser iguales");
        return;
    }
    
    //var params  = (userID, suit) => { return { userID: userID, suit: suit } }

    
    //aquí debemos de llamar al ws para crear el local y pasar state
    
    const params = {
      username:usernameRef.current.value,
      email:emailRef.current.value,
      password:Encrypt256( passwordRef1.current.value)
    };
    
    (async () => {
      consoleLOG(params)
        setLoading(true)
        consoleLOG(urlWS+'user/create.php');
        let data = GetParamsUserAgent('');
        const rawResponse = await fetch(urlWS+'user/create.php'+data, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
          },
          body: JSON.stringify(params)
        });
        const response = await rawResponse.json();          
        var id = response.id;
        if(parseInt(id) > 0){
            mensaje = lang.user_created;
            classCompleted = "alert alert-success";
            setLoading(false)
            setC(true)
            setR(true)
        }
        else if (id == -2){
            mensaje = lang.email_exists;
            setLoading(false)
            setC(true)
            setR(false)
        }
        else if (id == -3){
          mensaje = lang.username_exists;
          setLoading(false)
          setC(true)
          setR(false)
        }
        else{
          mensaje = lang.error;
          setLoading(false)
          setC(true)
          setR(false)
        }          
      })();
  }

  if(isLoading){
    return(
    <div className="loading_center">
        <div className="spinner-grow text-primary" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-secondary" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-success" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-danger" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-warning" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-info" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-light" role="status">
        <span className="sr-only">Loading...</span>
        </div>
        <div className="spinner-grow text-dark" role="status">
        <span className="sr-only">Loading...</span>
        </div>
    </div>
    );
}else if(completed && !isLoading && resultado){
        classCompleted = "alert alert-success";
        return(
          <div className="container h-100">
              <div className='row'>
                  <div className={" ms-auto me-auto mt-4 col-lg-4 col-12 "+classCompleted}>
                      <div className='text-center'>
                          {mensaje}
                      </div>
                      <div className="text-center margintop row">
                          <div className='col-6'><Link  href="/" ><a className="alert-link">{lang.home}</a></Link></div>
                          <div className='col-5 col-offset-1'><Link  href={"/login"}><a className="alert-link">{capitalize(lang.login)}</a></Link></div>
                      </div> 
                  </div>   
              </div>
          </div>
          );
}
else{
  return (
    <>
      {!user && (
        <>
         <section id="cover" className="h-100-50">
                    <div id="cover-caption">
                        <div className="container">
                            
                            <div className="row text-white">
                                <div className="col-lg-4 col-8 mx-auto text-center form p-4 fondocuadro">
                                    <h1 className="display-5 py-1 text-truncate">{capitalize(lang.signup)}</h1>
                                    <div className="px-2">

                                    {completed && !isLoading && !resultado &&
                              
                                        <div className={" ms-auto me-auto mt-0 "+classCompleted}>
                                            <div className='text-center fs-9'>
                                                {mensaje}
                                            </div>
                                      </div>
                                    }
                                        
                                        <form id="loginout" onSubmit={mySubmitHandler} className="justify-content-center">
                                            <div className="form-group">
                                                <label>{capitalize(lang.username)}</label>
                                                <input ref={usernameRef} name="username" required type="username" className="form-control" placeholder={lang.enter+" "+lang.username}/>
                                            </div>
                                            <div className="form-group">
                                                <label>{capitalize(lang.email)}</label>
                                                <input ref={emailRef} name="email" required type="email" className="form-control" placeholder={lang.enter+" "+lang.email}/>
                                            </div>
                                            <div className="form-group">
                                                <label>{capitalize(lang.password)}</label>
                                                <input type="password" pattern=".{6,}" ref={passwordRef1} required name="password1" className="form-control" placeholder={lang.enter+" "+lang.password+" "+lang.six_char} />
                                            </div>
                                            <div className="form-group">
                                                <label>{capitalize(lang.password)}</label>
                                                <input type="password" pattern=".{6,}" ref={passwordRef2} required name="password2" className="form-control" placeholder={lang.repeat+" "+lang.password} />
                                            </div>
                                            <button type="submit" className="btn btn-danger btn-block margintop">{capitalize(lang.submit)}</button>
                                            <hr></hr>
                                            <div className="forgot-password">
                                                <Link href="./login"><a>{capitalize(lang.already_register)}</a></Link>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        </>
      )}
    </>
  )
      }


}

export default SignUp