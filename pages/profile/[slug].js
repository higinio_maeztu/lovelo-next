import { useSession } from 'next-auth/react'
import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faUserCircle,  faBicycle, faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import {  locales, consoleLOG, URLamigable, GetParamsUserAgent   } from '../../utils/ifelse';
import { baseurlUser,urlWS } from '../../utils/constantes.js';
import SimpleImageUploadComponent from '../../components/extras/simple-image-upload.component.js';
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';
import Link from 'next/link'
import Head from 'next/head'
import { useRouter } from 'next/router';
import Router from 'next/router'

let dataAgent = '';
var classCompleted = "alert alert-danger";
var isPropietario = false;
var mensaje=null;

export default function Profile({ data, ...pageProps }) {
    var lang = pageProps.lang;
    
    var userID = '';
    if(pageProps.user)    
        userID = pageProps.user.userID;
    
    
    const { asPath, pathname } = useRouter();


    //console.log("cpmstristo23" +isPropietario)

    const [user, setUser] = useState(data);
    const [isLoading, setL] = useState(false);
    const [completed , setC] = useState(false);
    const [resultado, setR] = useState(false);

    
    const [photo, setphoto] = useState(null);
    const [photoBike, setphotoBike] = useState(null);
    const [photoChanged, setphotoChanged] = useState(false);
    const [photoBikeChanged, setphotoBikeChanged] = useState(false);

    let rutasWS = [];
       


    if(data.rutas){
        rutasWS= data.rutas;
    }
    const [rutas, setRutas] = useState(rutasWS)

    //dataAgent = GetParamsUserAgent(userID);

    var userProfileID = user.userID;
    //console.warn(userID +"=="+ userProfileID)
    if(userID != undefined && userID === userProfileID){
        isPropietario = true;
    }

    var urlfondo=null;
    if(user.photo){
        urlfondo=baseurlUser +"index.php?src="+user.photo;
    }

    var urlfotobici=null;
    if(user.photobike){
        urlfotobici=baseurlUser +"index.php?src="+user.photobike;
    }


    const handleChangePhoto= (file, nameState)  =>{      
      if(nameState == "photo"){
        setUser({...user, photo:file})
        setphoto(file)
        setphotoChanged(true)
      }
      if(nameState == "photoBike"){
        setUser({...user, photobike:file})
        setphotoBike(file)
        setphotoBikeChanged(true)
      }
    }

    const myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        setUser({...user, [nam]:val})
    }


    const ReturnHTMLField=(field)=>{
        var inputC = <input name={field} value={user[field]} disabled="disabled" className="form-control"  type="text"/>
        if(field == "web" && user.web && user.web.length > 0){
            inputC = <a target="_BLANK" rel="noreferrer" href={"//"+user.web}>{user.web}</a>
        }
        //console.warn(isPropietario)
        if(isPropietario){
            inputC = <input name={field} value={user[field]} onChange={myChangeHandler} className="form-control"  type="text"/>
        }

        //if(isPropietario || (user[field] && user[field].length > 0)){
            return (
                <tr>
                    <td>{lang[field]}</td>
                    <td>{inputC}</td>
                </tr>
            )
        //}
    }

    const ReturnDatePicker=(field)=>{
        if(user[field])
            var fechaCal = new Date(user[field]);
        else
            var fechaCal = new Date();


        return (
            <tr>
                <td>{lang[field]}</td>
                <td>
                    <DatePicker
                    className="picker" 
                    selected={fechaCal}
                    onChange={(date) => setStartDate(date, field)}
                    dateFormat="yyyy/MM/dd"/>
                </td>
            </tr>
        )
    }

    const setStartDate=(date, nam)=>{
        setUser({...user, [nam]:date})
    }

    const convertToParamas=(stado)=>{ //operations
        var objeto = stado;
        delete objeto.password;
        delete objeto.rutas;
        delete objeto.modified;
        delete objeto.created;
        
        let data = new Object();
        Object.entries(objeto).map(([key,value])=>{
            return (
                data[key] = value
            );
        })
        return data;
     }

    const GuardarPerfil=()=> {
        //aquí debemos de llamar al ws para
        var userID = user.userID;
        var params = convertToParamas(user);
        consoleLOG(params);
        setL(true);
        
        (async () => {
            let data2 = GetParamsUserAgent(userID);
            const rawResponse = await fetch(urlWS+'user/update.php'+data2, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
              },
              body: JSON.stringify(params)
            });
            const response = await rawResponse.json();    
            consoleLOG(response);      
            var resultadoUpdate = response.id;
            var message = lang.error_route_created;
            message = lang.ruta_created;
            
            
            //////////////////////////////////////////////////////LOCAL IMAGES//////////////////////////////////
            let data3 = new FormData();
            
            if(photo){
                data3.append('files[]', photo, "photo");
            }
            if(photoBike){
                data3.append('files[]', photoBike, "photobike");
            }
            
            data3.append('userID', userID);
            consoleLOG(data);
            if(photo || photoBike){
                (async () => {
                    const rawResponse = await fetch(urlWS+'user/addphotos.php'+data2+'&userID='+userID, {
                        method: 'POST',
                        body: data3
                    });
                    const response = await rawResponse.json();
                    consoleLOG(response)
                    mensaje = lang.perfil_updated;
                    
                    setL(false)
                    setC(true)
                    setR(true)

                        /*completed:true,
                        resultado:true*/
                    
                    
                    })();
            }else{
                if(parseInt(resultadoUpdate) > 0){
                    mensaje = lang.perfil_updated;
                    /*self.setState({ 
                        loading:false,
                        completed:true,
                        resultado:true
                    });  */
                    setL(false)
                    setC(true)
                    setR(true)

                }else{        
                    mensaje = lang.error;        
                    /*self.setState({ 
                        loading:false,
                        completed:true,
                        resultado:false
                    });  */
                    setL(false)
                    setC(true)
                    setR(false)
                }  
            }
            //////////////////////////////////////////////////////FIN LOCAL IMAGES//////////////////////////////////


            
                    
          })();
      }

    const BorrarRuta=(ruta)=>{
        var id = ruta.rutaID;

        var params = {'rutaID':id, 'clave':parseInt(id)*56, 'userID':user.userID};
        (async () => {
            let data2 = GetParamsUserAgent();
            const rawResponse = await fetch(urlWS+'ruta/delete.php'+data2, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Content-Type':'application/x-www-form-urlencoded' //sin esta línea da error
              },
              body: JSON.stringify(params)
            });
            const response = await rawResponse.json();    
            consoleLOG(response);      
            
            var id = response.id;
            if(id > 0){
                alert(lang.ruta_deleted);
                
                Router.push("/"+lang.lang);
                //borramos el elemento
                /*var arr = rutas;
                var i = arr.indexOf( ruta );
                if ( i !== -1 ) {
                    arr.splice( i, 1 );
                }
                //lo hacemos así porque no se actualizaba la página
                setRutas(rutas => ([...rutas, ...arr]));*/

            }                
            else
                alert(lang.error)


            return null;
            
          
          })();
    }

    
    if(completed && !isLoading){

        if(resultado){
            classCompleted = "alert alert-success";
        }
        
        return(
            <div className="container h-100">
                <div className='row'>
                    <div className={" ms-auto me-auto mt-4 col-lg-4 col-12 "+classCompleted}>
                        <div className='text-center'>
                            {mensaje}
                        </div>
                        <div className="text-center margintop row">
                            <div className='col-6'><Link  href="/" ><a className="alert-link">{lang.home}</a></Link></div>
                            <div className='col-5 col-offset-1'><Link  href={"/profile/[key]"} as={"/profile/"+user.userID}><a className="alert-link">{lang.profile}</a></Link></div>
                        </div> 
                    </div>   
                </div>
            </div>
            );
    }
    else{
    return (
      <div className="h-100 col-lg-5 col-12 mx-auto">
                <Head>
                        <title>{ "Lovelo | "+ lang.routes + " " + lang.of+ " " +data.username }</title>
                        <meta  key="description" name="description" content={lang.profile_meta+ data.username} />

                        <meta name="twitter:card" content="summary" />
                        <meta name="twitter:title" content={"Lovelo | "+ lang.routes + " " + lang.of+ " " +data.username}/>
                        <meta name="twitter:description" content={lang.profile_meta + data.username} />
                        {urlfondo > 0 &&<meta key="twitter:image" name="twitter:image" content={urlfondo}/>}

                        <meta key="og:title" property="og:title" content={"Lovelo | "+ lang.routes + " " + lang.of+ " " +data.username} />
                        <meta key="og:description" property="og:description" content={lang.profile_meta + data.username} />
                        <meta key="og:url" property="og:url" content={"https://lovelo.app/"+lang.lang+asPath} />
                        {urlfondo > 0 && <meta key="og:image" property="og:image" content={urlfondo}/>}
                </Head>
                   <div className="container" lang={lang.lang} style={{background:"white"}}>
                        {isLoading &&
                            <div className="loading_center h-100-50">
                                <div className="spinner-grow text-primary" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-secondary" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-success" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-danger" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-warning" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-info" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-light" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                                <div className="spinner-grow text-dark" role="status">
                                <span className="sr-only">Loading...</span>
                                </div>
                            </div>
                        } 

                        {isPropietario && 
                            <div className="text-right mt-4 mb-2 save-profile">
                        
                            <span onClick={() => GuardarPerfil()}>{lang.save_perfil}</span>
                            
                            
                            </div>
                        }  

                        <div className="card card-default">
                        <div className="card-header">
                            <h3 className="card-title text-left">{user.username}</h3>
                        </div>
                        <div className="card-body">
                            <div className="row">
                            <div className="col-3 " align="center"> 
                                {!photoChanged && urlfondo && <img src={urlfondo}/>}
                                {!photoChanged && !urlfondo && <FontAwesomeIcon size="4x" icon={faUserCircle}/>}
                                {isPropietario &&                                  
                                    <div className="mt-2">
                                        <SimpleImageUploadComponent nameState="photo" handleChangePhoto ={handleChangePhoto}/>
                                    </div>
                                }
                            </div>

                            <div className="col-9 "> 
                                {isPropietario && 
                                <div className="save-profile ml-2">
                                    <Link href="/changepass"><a>{lang.change_pass}</a></Link>
                                </div>}
                                <table className="table table-user-information" >
                                <tbody>
                                    {isPropietario && ReturnHTMLField("email")}
                                    {ReturnHTMLField("name")}
                                    {ReturnHTMLField("surname")}

                                    {isPropietario && ReturnDatePicker("datebirth")}
                                    {!isPropietario && ReturnHTMLField("datebirth")}
                                    
                                    {ReturnHTMLField("city")}
                                    {ReturnHTMLField("country")}
                                    {ReturnHTMLField("web")}
                                    {ReturnHTMLField("comment")}
                                       

                                    </tbody>
                                    </table>
                                </div>

                                <div className="col-3" align="center"> 
                                {!photoBikeChanged && urlfotobici && <img src={urlfotobici}/>}
                                {!photoBikeChanged && !urlfotobici && <FontAwesomeIcon size="4x" icon={faBicycle}/>}
                                {isPropietario &&                                  
                                    <div className="mt-2">
                                        <SimpleImageUploadComponent nameState="photoBike" handleChangePhoto ={handleChangePhoto }/>
                                    </div>
                                }
                                </div>
                                <div className="col-9 "> 
                                    <table className="table table-user-information">
                                    <tbody>                                                
                                        {ReturnHTMLField("namebike")}
                                        {ReturnHTMLField("brandbike")}
                                        {ReturnHTMLField("modelbike")}
                                        {isPropietario && ReturnDatePicker("datebike")}
                                        {!isPropietario && ReturnHTMLField("datebike")}
                                        
                                    </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>                                            
                            </div>

                            
                            <div className="card card-default mt-4">
                                <div className="card-header">
                                        <h3 className="card-title text-left">{lang.routes}</h3>
                                    </div>
                                    <div className="card-body rutasprofile">
                                        {rutas.length == 0 &&
                                                <div>{lang.sin_route}</div>
                                        }

                                        {rutas.length > 0 &&
                                               
                                            <table className="table table-user-information">
                                            <tbody>  

                                                {rutas.map((ruta, idy) => {
                                                    var urlruta = URLamigable(ruta.titulo)+"_"+ruta.rutaID;
                                                    //consoleLOG(tipo_icono)
                                                    return(      
                                                        <tr key={idy}>
                                                            <td className="indice">
                                                                <span className='colorGris'>{idy+1}</span>
                                                            </td>                                                       
                                                            <td>
                                                                <Link href={"/route/"+urlruta}>
                                                                    <a>{ruta.titulo}</a>
                                                                </Link>                                                                
                                                            </td>
                                                            {isPropietario &&
                                                                <div>
                                                                    <td>
                                                                        <Link href={"/edit/"+urlruta}>
                                                                            <a>
                                                                                <span ><FontAwesomeIcon className="dato-icon pointer" icon={faPencilAlt} aria-hidden="true" size="md"/></span>
                                                                            </a>
                                                                        </Link>
                                                                    </td>
                                                                    <td>
                                                                        <span ><FontAwesomeIcon className="dato-icon pointer" icon={faTrashAlt} aria-hidden="true" size="md" onClick={() => {if (window.confirm(lang.borrar_ruta+ " -> "+ ruta.titulo)) BorrarRuta(ruta) }}/></span>
                                                                    </td>
                                                                </div>
                                                            }
                                                        </tr>
                                                    )
                                                })}
                                        </tbody>
                                        </table>
                                    }
                                </div>
                            </div>
                        </div>
                    
                </div>
    );
                                }
  }
  
 

  export async function getServerSideProps(context) {
    const { slug } = context.query;
    //.warn("EL ID es "+ slug)
    const res = await fetch(process.env.urlWS+'user/read.php?id='+slug)
    const data = await res.json();
    //console.warn(data)
    if (!data) return null;

    return {
      props: { data }
    };
  }