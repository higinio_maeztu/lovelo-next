import {  consoleLOG } from './ifelse.js';
import {marcadorkmcadax, GetRoadCode,GetWaytypeCode, GetRoadColor, ntobr} from './constantes.js';
import { LatLng } from '../components/extras/latlng.js';
var polyUtil = require('polyline-encoded');

export function decodePolyline(encoded){

    // array that holds the points

    var points=[ ]
    var index = 0, len = encoded.length;
    var lat = 0, lng = 0;
    while (index < len) {
        var b, shift = 0, result = 0;
        do {

    b = encoded.charAt(index++).charCodeAt(0) - 63;//finds ascii                                                                                    //and substract it by 63
              result |= (b & 0x1f) << shift;
              shift += 5;
             } while (b >= 0x20);


       var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
       lat += dlat;
      shift = 0;
      result = 0;
     do {
        b = encoded.charAt(index++).charCodeAt(0) - 63;
        result |= (b & 0x1f) << shift;
       shift += 5;
         } while (b >= 0x20);
     var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
     lng += dlng;
         
    points.push([(lat / 1E5),(lng / 1E5)])
    
  }
  return points
}


export function decodePolylineElevation (encodedPolyline, includeElevation){
    // array that holds the points
    let points = []
    let index = 0
    const len = encodedPolyline.length
    let lat = 0
    let lng = 0
    let ele = 0
    while (index < len) {
      let b
      let shift = 0
      let result = 0
      do {
        b = encodedPolyline.charAt(index++).charCodeAt(0) - 63 // finds ascii
        // and subtract it by 63
        result |= (b & 0x1f) << shift
        shift += 5
      } while (b >= 0x20)

      lat += ((result & 1) !== 0 ? ~(result >> 1) : (result >> 1))
      shift = 0
      result = 0
      do {
        b = encodedPolyline.charAt(index++).charCodeAt(0) - 63
        result |= (b & 0x1f) << shift
        shift += 5
      } while (b >= 0x20)
      lng += ((result & 1) !== 0 ? ~(result >> 1) : (result >> 1))

      if (includeElevation) {
        shift = 0
        result = 0
        do {
          b = encodedPolyline.charAt(index++).charCodeAt(0) - 63
          result |= (b & 0x1f) << shift
          shift += 5
        } while (b >= 0x20)
        ele += ((result & 1) !== 0 ? ~(result >> 1) : (result >> 1))
      }
      try {
        let location = [(lat / 1E5), (lng / 1E5)]
        if (includeElevation) location.push((ele / 100))
        points.push(location)
      } catch (e) {
        consoleLOG(e)
      }
    }
    return points
  }

export function descomponArrayTramos (arrayPuntos){
    var aux = [];
    for(var j=0;j < arrayPuntos.length;j++){
          var temp = arrayPuntos[j];
          if(j== arrayPuntos.length -1){
              //para el caso de ser el último tramo lo meteremos completo
              for(var i=0;i<temp.length;i++){
                  aux.push(temp[i]);
              }
          }else{
              //el -1 es para eliminar el último punto de cada tramo pq coincide con el primero del siguiente tramo.
              for(var i=0;i<temp.length - 1;i++){
                  aux.push(temp[i]);
              }
          }
     }
    return aux;
  }

  export function formarperfil(pathlocationsencode, pathelevationsencoded){
        
            var pathelevations = decodePolyline(pathelevationsencoded);
            var path = decodePolyline(pathlocationsencode);
            var arrayTrans = [];    
            for(var i=0;i<path.length;i++){  
                if(i == 0){
                        var loc2 = path[0];
                        var latlng = 
                        { 
                          lat: loc2[0], 
                          lng:loc2[1],
                          lat: function() { return loc2[0] },
                          lng: function() { return loc2[1] } 
                        } 
                        //var latlng = new LatLng(loc2[0], loc2[1]);
                        //var latlng = new window.google.maps.LatLng(loc2[0], loc2[1]);
                        arrayTrans.push({elevation:pathelevations[0][0], location:latlng});
                }   
                else{
                    var loc2 = path[i];                
                    //var latlng = new window.google.maps.LatLng(loc2[0], loc2[1]);
                    var latlng = 
                        { 
                          lat: loc2[0], 
                          lng:loc2[1],
                          lat: function() { return loc2[0] },
                          lng: function() { return loc2[1] } 
                        } 
                    var ind1 = parseInt(i/2);
                    var ind2 = i%2;
                    //console.warn("indice1 es "+ indice1 + " y indice2 es "+indice2);
                    arrayTrans.push({elevation:pathelevations[ind1][ind2], location:latlng});             
                }
            }

            return arrayTrans;
        
  }

  //únicamente incluye puntos cada 100 metros, olvidándose de los que ya hubiera
  //pero esto no nos vale para poder guardar la ruta con todos los puntos, los del perfil y los del path. Tendríamos que incluir en el path, los del perfil, devolver 2 arrays
  export function obtptoskilometricos(path, cadadist){
    var i=0;
    var indice=0;
    var d=0;
    var dist=0;
    var acum=0;
    var queda=0;
    var x=0;
    //borramos el perfil anterior
    var perfil = [];
    while(i<path.length){
        if(i==0){
            perfil[0]= path[0];
            indice=1;
        }
        else{
            var loc1=path[i-1];
            var loc2=path[i];
            d=distancia3dEarth(loc1,loc2);
            if(acum==0){
                //distancia(puntos[i],perfil[indice]);
                var loc1=path[i];
                var loc2=perfil[indice-1];
                dist=distancia3dEarth(loc1,loc2);
            }
            else{
                dist=d+acum;
            }
            if(dist<=cadadist){
                acum=dist;	
            }
            else{
                //división entera dist/1000
                var m=dist%cadadist;
                var a=dist-m;
                var N=a/cadadist;
                
                //extra es lo que hay que coger desde el puntos[i] palante para q con el acum sume 1000
                var extra=cadadist-acum;
                //nuevoP=puntos[i-1]+extra
                //nuevoP = new Array(3);
                var lat=path[i-1][0]+(path[i][0]-path[i-1][0])*(extra/d);
                var lon=path[i-1][1]+(path[i][1]-path[i-1][1])*(extra/d);
                //nuevoP=	[lat,lon,alt];
                //consoleLOG("lat" + lat);
                perfil[indice]=[parseFloat(lat.toFixed(5)),parseFloat(lon.toFixed(5))];
                indice++;
                
                //resto, es lo que quedarÃ­a suelto al final <1000 si la dis total 2700 resto=700
                //resto=d-x*1000;
                if(N>1)
                {
                    queda=d-extra;
                //x=queda/1000 x son los puntos que se pueden coger dentro del segmento
                    var h=queda%cadadist;
                    var b=queda-h;
                    var x=b/cadadist; //los puntos a poner
                    var j=1;
                    for(j=1;j<=x;j++)
                    {
                    //nuevoP=extra+1000*j
                    var extra1=extra+(cadadist*j);
                    lat=path[i-1][0]+(path[i][0]-path[i-1][0])*(extra1/d);
                    lon=path[i-1][1]+(path[i][1]-path[i-1][1])*(extra1/d);
                    //perfil[indice]=nuevoP
                    perfil[indice]=[parseFloat(lat.toFixed(5)),parseFloat(lon.toFixed(5))];
                    indice++;
                    }
                }
                acum=(d-extra)%cadadist;
            }
        }
        i++;
    }
    i--;
    //añadimos tb el último punto de la ruta
    perfil[indice]=[parseFloat(path[i][0]), parseFloat(path[i][1].toFixed(5))];
    return perfil;
}	

    //antigua función que añade los puntos kms al perfil, pero deja los que ya había.
  export function obtptoskilometricos2(path){
    var i=0;
    var d=0;
    var dist=0;
    var distacum=0;
    var kms = 0;
    //borramos el perfil anterior
    var perfil = [];
    var distancias = [];//este array te dice los índices a los cuáles estarán los puntos kms, por ejemplo [3,78,124] nos dice que en perfil[3] estará el km 1.
    while(i<path.length){
        if(i==0){
            perfil.push([parseFloat(path[0][0].toFixed(5)),parseFloat(path[0][1].toFixed(5))]);
            distancias.push(0);
        }
        else{
            var loc1=path[i-1];
            var loc2=path[i];
            d=distancia3dEarth(loc1,loc2);
            if((dist+d) > 1000){
                //pasamos un punto kilométrico, hay que añadirlo al perfil, crear el punto intermedio y añadirlo
                var extra=1000-dist;//extra es lo que hay que coger desde el path[i] palante para q con el dist sume 1000
                //PuntoKM=puntos[i-1]+extra
                var latPuntoKM=path[i-1][0]+(path[i][0]-path[i-1][0])*(extra/d);
                var lonPuntoKM=path[i-1][1]+(path[i][1]-path[i-1][1])*(extra/d);
                perfil.push([parseFloat(latPuntoKM.toFixed(5)),parseFloat(lonPuntoKM.toFixed(5))]);
                kms++;
                distancias.push(kms*1000);
                //añadimos también el siguiente punto porque ya está a menos de 1km seguro, suponiendo clar que nunca entre dos puntos hay más de 1km.
                //por ello en trazado DIRECT hay que dividir entre puntos cada 100m
                perfil.push([parseFloat(path[i][0].toFixed(5)),parseFloat(path[i][1].toFixed(5))]);
                dist = (dist+d)-1000;//reiniciamos la distancia a lo que llevamos desde el sigueinte punto al km
            }
            else{
                perfil.push([parseFloat(path[i][0].toFixed(5)),parseFloat(path[i][1].toFixed(5))]);
                dist = dist+d;            
            }
            distacum += d;                  
            distancias.push(distacum);
        }
        i++;                      
    }
    return [perfil,distancias];
}	

export function obtptosintermedios_direct(loc1,loc2){
    var a = [parseFloat(loc1.lat.toFixed(5)), parseFloat(loc1.lng.toFixed(5))];
    var b = [parseFloat(loc2.lat.toFixed(5)), parseFloat(loc2.lng.toFixed(5))];
    var i=1;
    var dist=distancia3dEarth(a,b);
    //borramos el perfil anterior
    var ptosintermedios = Math.floor(dist/50);
    var path=[];
    path.push(a);
    while(i<ptosintermedios){      
        var extra = 50;      
        var d = distancia3dEarth(path[i-1],b);
        var latPuntoKM=path[i-1][0]+(b[0]-path[i-1][0])*(extra/d);
        var lonPuntoKM=path[i-1][1]+(b[1]-path[i-1][1])*(extra/d);
        path.push([parseFloat(latPuntoKM.toFixed(5)),parseFloat(lonPuntoKM.toFixed(5))]);
        i++;                      
    }
    path.push(b);
    return path;
}	

export function distanciasArray(array){
    var distancias = [];
    for(var i=0;i<array.length;i++){
        if(i==0)
            distancias.push(0);
        else{
            distancias.push(distancia3dEarth(array[i-1], array[i]));
        }    
    }
    return distancias;
}

export function DivivirRuta(ruta){
    var cadacuanto = 3000;
    //esta función divide la ruta en partes. Por defecto ponemos un punto cada 10 kilómetros, y si la ruta es de menos de 10 kilómetros en dos partes
    var distancia = distanciaTramo(ruta);
    var partes = 2;
    var partes = Math.ceil(distancia/cadacuanto);
    if(partes < 2)
        partes = 2;
    return DividePathEnPartes(ruta, partes);
}

function DividePathEnPartes(ruta, partes){
    var indice = Math.ceil(ruta.length / partes);
    //consoleLOG("partes es " + partes + " y longitud es "+ ruta.length)
    //consoleLOG("indice es " + indice)
    var result = [];
    for(var i=0; i < partes; i++){
        var inicial = i * indice;
        var final = ((i+1) * indice)+1;
        if(final > (ruta.length - 1))
            final = ruta.length - 1;
        
        var trozo = ruta.slice(inicial, final);  
        //consoleLOG("trozo " + i + " es ")
        //consoleLOG(trozo)
        result.push(trozo);
    }
    return result;   
}

export function distanciaTramo(array){
    var distancia = 0;
    for(var i=0;i<array.length;i++){
        if(i>0)
            distancia += distancia3dEarth(array[i-1], array[i]);   
    }
    return distancia;
}

export function marcadoreskms(polylines, tramosDistancias){
    var cadax = marcadorkmcadax;//constante.js
    let distancia = tramosDistancias.reduce((a, b) => a + b, 0);
    var num = parseInt(distancia/cadax);
    //consoleLOG("PATH PLyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
    var kms = [];
    var suma = 0;

    var indices = [];
    for(var i=1;i<num;i++){
        indices[i*cadax]=false;
    }
    
    polylines.map((polyl, idy) => {
        var pathant = [];
        var pathpolyline=polyl.path;
        for(var j=0;j<pathpolyline.length;j++){
            if(pathant.length > 0){
                suma = suma + distancia3dEarth(pathant, pathpolyline[j]);

                var indice = parseInt(suma/cadax);
                if(!indices[indice] && indice > 0){
                    indices[indice] = true;
                    kms.push(pathpolyline[j]);
                }
                //consoleLOG(pathpolyline[j]);
                
            }
            pathant=pathpolyline[j];
        }
        
    })
    return kms;
}

export function distancia3dEarth(loc1, loc2){	
    //Pasamos la lat,lon a coordenadas cartesianas
    var lat1=loc1[0];
    var lon1=loc1[1];
    var lat2=loc2[0];
    var lon2=loc2[1];
    var R = 6371; // km (change this constant to get miles)
    var dLat = (lat2-lat1) * Math.PI / 180;
    var dLon = (lon2-lon1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
    Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return Math.round(d*1000);	
}

//CALCULA COEFICIENTE DE UN PUERTO. VER TABLA ALTIMETRIAS.COM
export function Coeficiente(coef,DistUltimo)
{
    var suma=new Number(0);
    var i=0;
    var entero=0;
    var p=0;
    var res=new Number(0);
    var decimal=0;
    for(i=0;i<coef.length;i++)
    {
    p=coef[i];


    if(p>0 && p<=4)
    res=new Number(p) + 1;

    if(p>4 && p<5)
    {
    entero=parseInt(p);		
    res=entero+1;
    decimal=p-entero;
    res=res+(decimal*2);
    res=res.toFixed(1);
    }
    if(p>=5 && p<6)
    {
    entero=parseInt(p);	
    res=entero+2;
    decimal=p-entero;
    res=res+(decimal*3);
    res=res.toFixed(1);
    }
    if(p>=6 && p<7)
    {
    entero=parseInt(p);	
    res=entero+4;
    decimal=p-entero;
    res=res+(decimal*4);
    res=res.toFixed(1);
    }
    if(p>=7 && p<8)
    {
    entero=parseInt(p);	
    res=entero+7;
    decimal=p-entero;
    res=res+(decimal*5);
    res=res.toFixed(1);
    }
    if(p>=8 && p<9)
    {
    entero=parseInt(p);	
    res=entero+11;
    decimal=p-entero;
    res=res+(decimal*6);
    res=res.toFixed(1);
    }
    if(p>=9 && p<10)
    {
    entero=parseInt(p);	
    res=entero+16;
    decimal=p-entero;
    res=res+(decimal*7);
    res=res.toFixed(1);
    }
    if(p>=10 && p<11)
    {
    entero=parseInt(p);	
    res=entero+22;
    decimal=p-entero;
    res=res+(decimal*8);
    res=res.toFixed(1);
    }
    if(p>=11 && p<12)
    {
    entero=parseInt(p);	
    res=entero+29;
    decimal=p-entero;
    res=res+(decimal*9);
    res=res.toFixed(1);
    }
    if(p>=12 && p<13)
    {
    entero=parseInt(p);	
    res=entero+37;
    decimal=p-entero;
    res=res+(decimal*10);
    res=res.toFixed(1);
    }
    if(p>=13 && p<14)
    {
    entero=parseInt(p);	
    res=entero+46;
    decimal=p-entero;
    res=res+(decimal*11);
    res=res.toFixed(1);
    }
    if(p>=14 && p<15)
    {
    entero=parseInt(p);	
    res=entero+56;
    decimal=p-entero;
    res=res+(decimal*12);
    res=res.toFixed(1);
    }
    if(p>=15 && p<16)
    {
    entero=parseInt(p);	
    res=entero+67;
    decimal=p-entero;
    res=res+(decimal*13);
    res=res.toFixed(1);
    }
    if(p>=16 && p<17)
    {
    entero=parseInt(p);	
    res=entero+79;
    decimal=p-entero;
    res=res+(decimal*14);
    res=res.toFixed(1);
    }
    if(p>=17 && p<18)
    {
    entero=parseInt(p);	
    res=entero+92;
    decimal=p-entero;
    res=res+(decimal*15);
    res=res.toFixed(1);
    }
    if(p>=18 && p<19)
    {
    entero=parseInt(p);	
    res=entero+106;
    decimal=p-entero;
    res=res+(decimal*16);
    res=res.toFixed(1);
    }
    if(p>=19 && p<20)
    {
    entero=parseInt(p);	
    res=entero+121;
    decimal=p-entero;
    res=res+(decimal*17);
    res=res.toFixed(1);
    }
    if(p>=20 && p<21)
    {
    entero=parseInt(p);	
    res=entero+137;
    decimal=p-entero;
    res=res+(decimal*18);
    res=res.toFixed(1);
    }
    if(p>=21 && p<22)
    {
    entero=parseInt(p);	
    res=entero+154;
    decimal=p-entero;
    res=res+(decimal*19);
    res=res.toFixed(1);
    }
    if(p>=22 && p<23)
    {
    entero=parseInt(p);	
    res=entero+172;
    decimal=p-entero;
    res=res+(decimal*20);
    res=res.toFixed(1);
    }
    if(p>=23 && p<24)
    {
    entero=parseInt(p);	
    res=entero+191;
    decimal=p-entero;
    res=res+(decimal*21);
    res=res.toFixed(1);
    }
    if(p>=24 && p<25)
    {
    entero=parseInt(p);	
    res=entero+211;
    decimal=p-entero;
    res=res+(decimal*22);
    res=res.toFixed(1);
    }
    if(p>=25 && p<26)
    {
    entero=parseInt(p);	
    res=entero+232;
    decimal=p-entero;
    res=res+(decimal*23);
    res=res.toFixed(1);
    }
    if(p>=26 && p<27)
    {
    entero=parseInt(p);	
    res=entero+254;
    decimal=p-entero;
    res=res+(decimal*24);
    res=res.toFixed(1);
    }
    if(p>=27 && p<28)
    {
    entero=parseInt(p);	
    res=entero+277;
    decimal=p-entero;
    res=res+(decimal*25);
    res=res.toFixed(1);
    }
    if(p>=28 && p<29)
    {
    entero=parseInt(p);	
    res=entero+301;
    decimal=p-entero;
    res=res+(decimal*26);
    res=res.toFixed(1);
    }
    if(p>=30)
    res=382;

    if(p<=0)
    res=1;

    if(i<coef.length-1)
            suma=new Number(suma)+new Number(res);//solo una de las opciones anteriores rellenara el parámetro res

    else
      {
            res=(res*(DistUltimo/1000)).toFixed(1);
            suma=new Number(suma)+new Number(res);
            }

    }

    return parseInt(suma);
}

export function calcularEncodedElevationsRuta(perfil){
    var arrayelevations = [];
    var arraylocations = [];
    consoleLOG(perfil);
    for (var i = 0; i < perfil.length; i++) {
        arraylocations.push([perfil[i].location.lat(), perfil[i].location.lng()])
        ///////////////////////////////////////////hacemos un encoded de las elevaciones
        //solo añadimos cada 2 veces, ya que añadimos 2 de una vez. Y el último elemento, si no es par, le metemos un 9999
        if(i%2 == 0){
            if(i+1 < perfil.length){
                //si hay un punto siguiente
                arrayelevations.push([parseFloat(perfil[i].elevation.toFixed(3)), parseFloat(perfil[i+1].elevation.toFixed(3))]);
            }
            else{
                arrayelevations.push([parseFloat(perfil[i].elevation.toFixed(3)), 9999]);
            }
        }
    }
    return [polyUtil.encode(arraylocations),polyUtil.encode(arrayelevations)]
}

export function calcularDatosRuta(results, distancia, cadadist, velocidad){
    //console.warn(results)
    var alturamin = results[0].elevation;
    var alturamax = results[0].elevation;
    var dif_altura = results[results.length -1].elevation - results[0].elevation;
    var ascend = 0;
    var descend = 0;
    var coef = [];
    var elevationsTime = [];
    var TE = 0;
    for (var i = 0; i < results.length; i++) {
        ///////////////////////////////////////////hacemos un encoded de las elevaciones
        //solo añadimos cada 2 veces, ya que añadimos 2 de una vez. Y el último elemento, si no es par, le metemos un 9999
        

        elevationsTime.push(SegToTimeZ(TE*60));
        if(i > 0){
            var pm=((results[i].elevation-results[i-1].elevation)/(cadadist/100)).toFixed(1);
            var vel = velocidadEnPm(pm, velocidad);
            TE=TE+cadadist/(vel*1000/60);
        }
        

        if(i%(1000/cadadist) == 0 && i > 0){
            var porc = (results[i].elevation - results[i-(1000/cadadist)].elevation)/10;
            coef.push(porc);
        }
        
        ///////////////////////////////////////////FIN hacemos un encoded de las elevaciones



        if(results[i].elevation > alturamax)
            alturamax = results[i].elevation;

        if(results[i].elevation < alturamin)
            alturamin = results[i].elevation;   
            
        if(i> 0 && results[i].elevation > results[i-1].elevation){
            ascend += results[i].elevation - results[i-1].elevation;
        }

        if(i> 0 && results[i].elevation < results[i-1].elevation){
            descend += results[i-1].elevation - results[i].elevation;
        }
            
    }

    /////////////////////////////////////////////////////////Categoria y Coeficiente
    //x es la posición del final del último km completo . Si la ruta son 2.4 km tendrá 24 puntos y el último km empezará en el punto 20.      
    var x= parseInt(distancia/1000)*(1000/cadadist);
    //console.warn("x es " + x +" y longitud de results es "+ results.length + " y distancia es "+ distancia)
    var porc = (results[results.length-1].elevation - results[x].elevation)/(distancia%1000);
    porc = porc *100;
    coef.push(porc);
    var coeficienteR=Coeficiente(coef,distancia%1000);  
      
   
    var categoria = GetIntCategory(coeficienteR);
    /////////////////////////////////////////////////////////Categoria y Coeficiente


    ///////////////////////////////////////////////////Tiempo y Velocidad
    //no calculamos el último punto, poco va a añadir
    elevationsTime.push(SegToTimeZ(TE*60));
    TE=TE.toFixed(0);           
    ///////////////////////////////////////////////////Tiempo y Velocidad

    return [Math.round(alturamin), Math.round(alturamax), descend.toFixed(0), ascend.toFixed(0), dif_altura.toFixed(1), TE, coeficienteR, categoria]
}

export function GetIntCategory(c){
    //0 es que no tiene categoría, muy suave
    //1 es primera
    //2 es segunda
    //3 es tercera
    //4 categoria especial
    var d = 0;
    if(c>=75 && c<125)
        d=3;
    else if(c>=125 && c<=200)
        d=2;
    else if(c>200 && c<=300)
        d=1;
    else if(c>300)
        d=4;
    
    return d;
}

export function CalculaTime(velocidad, distancia){
    //distancia en metros
    //velocidad en km/h
    var km = distancia/1000;
    var vel = velocidad/3600;
    
    //calculo tiempo en segundos
    var tiempo = km/velocidad;
    return tiempo;
}

export function SegToTimeZ(time){
    var hours = Math.floor( time / 3600 );  
    if(hours < 10)
        hours = "0"+hours;
    var minutes = Math.floor( (time % 3600) / 60 );
    if(minutes < 10)
        minutes = "0"+minutes;
    var seconds = time % 60;
    seconds = seconds.toFixed(0);
    if(seconds < 10)
        seconds = "0"+seconds;
    
    return hours+":"+minutes+":"+seconds;
}

//Calcula la velocidad estimada para un % concreto
export function velocidadEnPm(pm, velocidadUsuario = "22"){
    //var velocidadUsuario = "22";//Esto antes lo elegían los usuarios, ahora lo vamos a definir nosotros.
    var y=0;
    if (pm>20)
        pm=20;
    if (pm<-20)
        pm=-20;
    //para el tiempo estimado y las calorías. ecuaciones sacadas por estadísticas.ver excel
    if(velocidadUsuario == "17")
    {
        if(pm>=0){
            y = 0.063*(pm*pm) - 1.924*pm + 17.35;
        }
        else{
            y = -0.129*(pm*pm) - 6.296*pm + 12.48;
        }
    }									
    if(velocidadUsuario == "22"){
        if(pm>=0){
            y = 0.053*(pm*pm) - 2.023*pm + 22.11;
        }
        else{
            y = -0.130*(pm*pm) - 5.972*pm + 18.51;
        }
    }												
    if(velocidadUsuario == "27"){
        if(pm>=0){
            y = 0.054*(pm*pm) - 2.205*pm + 27.29;
        }
        else{
            y = -0.107*(pm*pm) - 4.832*pm + 31.32;
        }
    }
    return y;
}


export function CalcPolyBySurfaces(path, waytypes, surfaces){
    //devuelve 2 elementos. El primero es el  array de GEOjson, y el segundo es un array sólo de indices por si se guarda la ruta en la bd, no perder esa info
    //si el cálculo se ha hecho con un proveeedor que no devuelve el tipo de carretera o de terreno devolvemos una única línea con surface y waytype desconocido [valor = 0]

    
    if(!waytypes || !surfaces){
        var distancia = distanciaTramo(path)
        var pathreverse = [];
        path.map((punto) => {
            pathreverse.push([punto[1], punto[0]]); 
        });
        return [[{ "type": "Feature", "distance":distancia,"properties": {"waytype":0, "surface": 0 }, "geometry": { "type": "LineString", "coordinates": pathreverse } }], null];
    }
    else{
        var arrayLineal = [];
        waytypes.forEach(way => {
            arrayLineal.push(parseInt(way[0]));
            arrayLineal.push(parseInt(way[1]));
        })
        surfaces.forEach(sur => {
            arrayLineal.push(parseInt(sur[0]));
            arrayLineal.push(parseInt(sur[1]));
        })

        /*if (!Array.isArray(arrayLineal)) {
            return null;
        }*/

        var uniqueArray = arrayLineal.filter(function(item, pos) {
            return arrayLineal.indexOf(item) == pos;
        })

        //sort
        uniqueArray = uniqueArray.sort(function(a, b) {return a - b;});
        
        var surfacesIndices = [];
        var GeoJSON = [];
        for(var j = 0; j < uniqueArray.length - 1; j++){
            var pathway = [];
            var ini = uniqueArray[j];
            var fin = uniqueArray[j+1];
            var distance = 0;
            //cogemos la geometry
            for(var i = ini ; i <= fin; i++){
                pathway.push([path[i][1], path[i][0]]);
                //en el último punto que no entre que ya no hay un +1
                if(i < fin)
                    distance += distancia3dEarth(path[i], path[i+1]);   
            }
            //var distance = distanciaTramo(pathway)
            //calculamos waytype y surface
            var surface = GetSurfaceOrWaytype(ini,fin, surfaces);
            var waytype = GetSurfaceOrWaytype(ini,fin, waytypes);
            var feature = { "type": "Feature", "distance":distance,"properties": {"waytype":waytype, "surface": surface }, "geometry": { "type": "LineString", "coordinates": pathway } };
            GeoJSON.push(feature);
            surfacesIndices.push(ini,fin);
            surfacesIndices.push(waytype,surface);
        }
        return [GeoJSON, surfacesIndices];
    }        
}

export function GetSurfaceOrWaytype(ini,fin,arraySW){
    //array será el array de waytypes o de surfaces
    var enc = false;
    var result = 0;
    var i = 0;
    while(i < arraySW.length && !enc){
        var element = arraySW[i];
        if(ini >= element[0] && fin <= element[1]){
            enc = true;
            result = element[2];
        }
        i++;
    }
    return parseInt(result);
}

export function CalcSurfacesTramosWaytypesForSaveRoute(polylines){
    //[ini1,fin1][waytype1,surface1]....[iniN,finN][waytypeN,surfaceN]
    var retornar = [];
    var indiceTramos = 0;
    polylines.map((polyl, idy) => {
        /*consoleLOG("CalcSurfacesTramosWaytypesForSaveRoute")
        consoleLOG(polyl);*/
        
        var surfaces = polyl.pathSurfaces;
        var arraySurfacesPoly = [];
        var indiceprev = 0;
        surfaces.map((elem, idx) => {
            var way = elem.properties.waytype;
            var surf = elem.properties.surface;
            var number = elem.geometry.coordinates.length + indiceprev - 1;//el -1 es porque el último de una poly, coincide con el primero  de la siguiente, pero el path no los tiene
            arraySurfacesPoly.push([indiceprev,number]);
            arraySurfacesPoly.push([way,surf]);
            indiceprev = number;
        })
        //consoleLOG(arraySurfacesPoly);
        indiceTramos += polyl.path.length-1;
        retornar.push({"tM":polyl.travelMode,"p":polyl.provider,"l":polyUtil.encode(arraySurfacesPoly),"t":indiceTramos})
        
    })

    return retornar;
}

function DecodePolylineSurfacesOwn(cadenajson, path){
    var arraysEncoded = decodePolyline(cadenajson);
    consoleLOG(arraysEncoded)
    var GeoJSON = [];
    for (let index = 0; index < arraysEncoded.length; index++) {
        const indices = arraysEncoded[index];
        index++;
        const waysurf = arraysEncoded[index];

        var ini = indices[0];
        var fin = indices[1];//porque slice no incluye al índice de fin
        var pathway = path.slice(ini,fin+1);
        /*consoleLOG("pathway")
        consoleLOG(pathway)*/

        var distancia = distanciaTramo(pathway);

        var pathreverse = [];
        pathway.map((punto) => {
            pathreverse.push([punto[1], punto[0]]); 
        });

        var waytype = waysurf[0];
        var surface = waysurf[1];


        var feature = { "type": "Feature","distance":distancia, "properties": {"waytype":waytype, "surface": surface }, "geometry": { "type": "LineString", "coordinates": pathreverse } };   
        GeoJSON.push(feature); 
    }
    return GeoJSON;    
}

export function CalcPolylinesyTramosFromBD(path, infobd = false){
    //devuelve 2 elementos. El primero es el  array de GEOjson, y el segundo es un array sólo de indices por si se guarda la ruta en la bd, no perder esa info
    //si el cálculo se ha hecho con un proveeedor que no devuelve el tipo de carretera o de terreno devolvemos una única línea con surface y waytype desconocido [valor = 0]
    //consoleLOG(infobd);
    
    if(!infobd){
        var pathreverse = [];
        path.map((punto) => {
            pathreverse.push([punto[1], punto[0]]); 
        });
        var markers = [{lat: path[0][0], lng: path[0][1]}, {lat: path[path.length-1][0], lng: path[path.length-1][1]}]
        var distancia_tramo = distanciaTramo(path);
        var array_tramos_actual= [distancia_tramo];

        var pathSurfaces = [{ "type": "Feature","distance":distancia_tramo, "properties": {"waytype":0, "surface": 0 }, "geometry": { "type": "LineString", "coordinates": pathreverse } }];
        
        var polylines = [{"key":Math.random(),"pathSurfaces" : pathSurfaces, "path" : path, "provider": null, "travelMode" : null, "distance":distancia_tramo}];
        
    }
    else{
        var polylines= [];//este array es el array de polylines que se meterá en el estado
        var markers = [];//este array es el array de markers que se meterá en el estado
        var array_tramos_actual = [];

        var arrayJson = JSON.parse(infobd);
        var indiceprev = 0;

        var sumatramos = 0;
        for (let index = 0; index < arrayJson.length; index++) {
            const elementJSON = arrayJson[index];
            //cada vuelta es una polyline y un marcador
            markers.push({lat: path[indiceprev][0], lng: path[indiceprev][1]});
            

            var travelMode = elementJSON.tM;
            var provider = elementJSON.p;  
            var indice = elementJSON.t;  
            
            var polylinePath = [];
            let i = 0;
            for (i = indiceprev; i <= indice; i++) {
                polylinePath.push(path[i]);  
            }
            
            //consoleLOG("polylinePath")
            //consoleLOG(polylinePath)
            var pathSurfaces = DecodePolylineSurfacesOwn(elementJSON.l, polylinePath);
            indiceprev=indice;  
            
            
            //indiceprev en la siguiente vuelta será el indice que ahora era el fin
            

            var distancia_tramo = distanciaTramo(polylinePath);
            array_tramos_actual.push(distancia_tramo);

            sumatramos += distancia_tramo;
            //consoleLOG("SUMA TRAMOS :"+sumatramos)

            var polyobject = {"key":Math.random(),"pathSurfaces" : pathSurfaces, "path" : polylinePath, "provider": provider, "travelMode" : travelMode, "distance":distancia_tramo};
            polylines.push(polyobject);
        }

        //ultimo marcador
        markers.push({lat: path[path.length-1][0], lng: path[path.length-1][1]});

        
    }   
    
    //console.warn("polylines")
    //console.warn(polylines)    
    return [polylines, markers, array_tramos_actual];
}

export function GetSurfaceChartSVG(polylines, width,posIni, distance){
    var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svg.setAttribute('xlink','http://www.w3.org/1999/xlink');
    svg.setAttribute('width',width);
    svg.setAttribute('height','10');

    var indice = posIni;
    width = width - posIni; //tenemos que restarle el marginLeft para que cuadren los porcentajes
    
    polylines.map((polyline, idy) => {
        polyline.pathSurfaces.forEach(surface => {
            /*var id = "percetnt"+ids;
            ids++;*/
            var surfaceCode = surface.properties.surface;
            var distancia_tramo = surface.distance;
            var percent = (distancia_tramo/distance);
            var codesurf = GetRoadCode(surfaceCode, surface.properties.waytype);
            var color = GetRoadColor(codesurf);
            
            var w = width * percent;

            console.log("=> porcentaje: "+ percent + " color: "+ color)
            console.log("el width es " + w + " y el width total es " + width);

            var rect = document.createElementNS( 'http://www.w3.org/2000/svg','rect' );
            rect.setAttributeNS( null,'x',indice );
            rect.setAttributeNS( null,'y',0 );
            rect.setAttributeNS( null,'width',w );
            rect.setAttributeNS( null,'height','10' );
            rect.setAttributeNS( null,'fill',color);
            svg.appendChild( rect );

            indice = indice + w;
        })
    })
    

    return svg;
}

export function GetSurfacesChart(polylines, distance){
    
    var percentSurface=[];
    var percentWaytype=[];
    var arrayDivs = [];
    var ids = 0;
    polylines.map((polyline, idy) => {
        polyline.pathSurfaces.forEach(surface => {
            var id = "percetnt"+ids;
            ids++;
            var surfaceCode = surface.properties.surface;
            var distancia_tramo = surface.distance;
            var percent = (distancia_tramo/distance)*100;
            var codesurf = GetRoadCode(surfaceCode, surface.properties.waytype);
            var color = GetRoadColor(codesurf);
            consoleLOG( " la distanciatota es"+distance+" y la del tramo es " + distancia_tramo +  " y el percent es " + percent + " y el color es " + color)
            arrayDivs.push(<div key={id} className="progress-bar" style={{width: percent+'%', backgroundColor:color}}></div>)

            var codeway = GetWaytypeCode(surface.properties.waytype);


            if(percentSurface[codesurf]){
                percentSurface[codesurf] = [percentSurface[codesurf][0]+percent,percentSurface[codesurf][1]+distancia_tramo];
            }
            else{
                percentSurface[codesurf] = [percent,distancia_tramo];
            }

            if(percentWaytype[codeway]){
                percentWaytype[codeway] = [percentWaytype[codeway][0]+ percent,percentWaytype[codeway][1]+ distancia_tramo];
            }
            else{
                percentWaytype[codeway] = [percent,distancia_tramo];
            }            
        })
    })

    

    return [arrayDivs, percentSurface, percentWaytype]
}

function encodedPathComplete(polylines){
    var pathPolylines = [];
    polylines.forEach(pol => {
        pathPolylines.push(pol.path);
    })
    return descomponArrayTramos(pathPolylines);
}

export function convertToParamas(objeto, userID){ //operations
    var inicio = objeto.datosRuta.elevations[0];
    var jsonsurfaces = JSON.stringify(CalcSurfacesTramosWaytypesForSaveRoute(objeto.polylines));
    var encodedPolylineRuta = polyUtil.encode(encodedPathComplete(objeto.polylines));
    return {
        'userID':userID ,
        'titulo': objeto.titulo,
        'descripcion':objeto.descripcion,
        'polyline':encodedPolylineRuta,
        'lat':inicio.location.lat() ,
        'lng':inicio.location.lng() ,
        'alt':inicio.elevation ,
        'distancia':objeto.datosRuta.distancia ,
        'coeficiente':objeto.datosRuta.coeficiente ,
        'desnivel':objeto.datosRuta.ascend,
        'categoria':objeto.datosRuta.categoria ,
        //'tipo':"a" ,
        //'terreno':"a" ,
        'velocidad':parseInt(objeto.speedMode),
        'annotations': (objeto.datosRuta.annotations.length > 0) ? JSON.stringify(objeto.datosRuta.annotations): null,
        'surfaces': jsonsurfaces,
        'opcionesPerfil':JSON.stringify(objeto.optionsPerfil)
    }
 }

 