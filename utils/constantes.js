import roadBike from '../public/images/road.png'
import rally from '../public/images/rally.png'
import mtbBike from '../public/images/mtb.png'
import hike from '../public/images/hiking.png'
import direct from '../public/images/direct.png'
import circular from '../public/images/circular.png'
import pruebaroad from '../public/images/pruebaroad.png'
import cicloturismo from '../public/images/cicloturismo.png'
import puerto from '../public/images/puerto.png'
import pruebamtb from '../public/images/pruebamtb.png'
import React from 'react'

import { consoleLOG } from './ifelse'

/*export const baseurl = "http://localhost/lovelo/ws/photos/";
export const baseurlUser = "http://localhost/lovelo/ws/usersphotos/";
export const urlWS = "http://localhost/lovelo/ws/";
export const baseweb = "http://localhost:3000/";*/
//export const baseicon = "http://localhost/lovelo/ekibike/src/icons/";
//export const baseimages = "http://localhost/lovelo/ekibike/src/images/";

export const baseurlUser = "https://himae.com/lovelo/ws/usersphotos/";
export const baseurl = "https://himae.com/lovelo/ws/photos/";
export const urlWS = "https://himae.com/lovelo/ws/";
export const baseweb = "https://lovelo.app/";
export const baseicon = "https://lovelo.app/icons/";
export const baseimages = "https://lovelo.app/images/";

export const marcadorkmcadax = 5000;


const imagesArray = importAll(require.context('../public/icons', false, /\.(png)$/));

export function capitalize (s) {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

export function IconKm(km){
  var L = require('leaflet');
  var classe = 'icon-km';
  if(km ==1)
    classe = 'icon-km1';
  var icon = new L.DivIcon({
    iconSize: [15, 15],
    className: classe,
    html: '<div class="my-div-span">'+(km*marcadorkmcadax/1000)+'</div>'
  });

  return icon;
}

export function CreateIconLeaflet(rutapng, size){
  var L = require('leaflet');
  var pin = L.icon({
    iconSize: [size, size],
    iconUrl: baseimages+rutapng+".png"
  });
  return pin;
}



export const perfilOptions = { 'color':'#24B8AA', 'width':'100' ,'altura_max':'0', 'lineaskm':true, 'lineashorizontales':'2', 'color_lineashorizontales':'#d4d4d4', "porcentajesmayores":"0",
"decimales":false, "porc_symbol":false, "porc_color":"#111111","porc_pixels":"9", "tags_color":"#100000", "tags_stem_color":"#100000", "tags_stem_size":"20","tags_size":"12","fuente":"Tahoma", "tags_size_icon":"20"};

//export const perfilOptions = {'annotation_font_size':"9", 'annotation_color':'#000000', 'porc_font','stem_color','stem_long',
//'chart_color','lines_color','lines_count','chart_font_family','porc_symbol','lineaskm','altura_max','porcentajesmayores','decimales'};

export const PolyOptions = { color: '#f74a3d', weight:"4" }
export const centerdefault = [40.3370985,-5.3576908];
export const apikey_mapbox = "pk.eyJ1IjoiaGlnaW5pbyIsImEiOiJjanN5bXR1OXAwZ3dlNDRyMnk3Mzc0emY0In0.w19t4Sn9OjLjr9qaXFfkcQ";
export const apiKey_arcgis = "AAPKef7e91441e7344228b601a99052941c6kJCdStMwyRaAhOFWnVYN7yWzfHMV7Knse-qCfeFuXcaDe77Rlwa8Vj-pQwOSvzNl";
//export const apiKey_openroute = "5b3ce3597851110001cf6248f18806b5e72d4b348a8889653ad2e53a"; //PRODUCCION
export const apiKey_openroute = "5b3ce3597851110001cf62480a258257d24447f58287de32546b30c9"; //DESARROLLO

//export const list_directions_prov = {"google":"Google Maps", "mapbox" :"MapBox", "arcgis":"ArcGis", "openrouteservice":"Open Route Service"};
export const list_directions_prov = {"google":"Google Maps [GM]", "mapbox" :"MapBox [MB]", "openrouteservice":"Open Route Service [ORS]"};
export const list_directions_prov_siglas = {"google":"GM", "mapbox" :"MB", "openrouteservice":"ORS", "external":"EXT"};
export const list_elevations_prov = {"google":"Google Maps", "arcgis":"ArcGis", "openrouteservice":"Open Route Service"};

export const colorGris = "#999";
export const colorStars = "#FFC625";
export const colorGravel = "#ff9900";
//export const colorDefault = "#f74a3d";
export const colorDefault = "#24B8AA";
export const colorRoad = "#111";
//export const colorPaved = "#7a7a7a";
export const colorPaved = "#aaaaaa";
export const colorFerry = "blue";
export const colorGround= "#804000";
export const colorSand = "#F0C929";
export const colorBikePath = "#FF0000";

const colorsTracks = ["#BC5DE3", "#E3BC68", "#6A52E3","#15E33B","#4680E3"];

export const TracksOptions = function (feature) {
  return {
    "color": GetTrackColor(feature.properties.key),
    "opacity": 1,
    "dashOffset": 0
  }};

export const GeoJsonOptions = function (feature) {
  var [text,color]=GerSurfaceTextAndColorByCode(GetRoadCode(feature.properties.surface, feature.properties.waytype));
  console.log(color)
  return {
    "color": color,
    "opacity": 1,
    "fill-opacity": 0.6,
    "dashArray": GetRoadDash(feature.properties.waytype), //x,y donde x es la longitud del trazo, e y es cada cuántos puntos se pinta
    "dashOffset": 0
    //"weight": GetRoadWeight(feature.properties.waytype)
  }};

  export function GetRoadCode(surface, waytype) {    
    var surfacecode;
    /*/
    0 desconocido
    1 carretera asfaltada = pista asfaltada
    2 hormigón adoquines empedrado
    3 PISTA = pista gravilla = carretera gravilla
    4 sendero tierra = pista tierra 
    5 carril bici
    6 sand
    10 ferry, water
    11 tunel

    El waytype suele venir relleno siempre y el surface no, entonces, si el surface es 0, intentamos averiguar lo que es según el waytype
    */
    if(surface == 0){
        if(waytype == 1 || waytype == 2){
          //es una carretera debería ser asfalto
          surfacecode = "1";
        }
        else if(waytype == 3){
          //es una calle debería ser hormigón o adoquines
          surfacecode = "2"
        }else if(waytype == "5"){
          //es una pista debería ser gravilla
          surfacecode = "3"
        }else if(waytype == "4" || waytype == "7"){
          //es un sendero debería ser campo
          surfacecode = "4"
        }else if(waytype == "6"){
          //es un carril bici
          surfacecode = "6"
        }else if(waytype == "9"){
          //es un carril bici
          surfacecode = "10"
        }
    }
    else if (surface == 1 && waytype == 2) {
      //si es carretera y paved es que es asfalto. Si no, todo sale en gris
      surfacecode = "1";
    }
    else if (surface == 1 && waytype == 6) {
      surfacecode = "6";
    }
    else if (surface == 3 )  
      surfacecode = "1";
    else if([1,4,5,14,6,7].includes(surface)){
      surfacecode = "2"
    }
    else if([8,9, 10].includes(surface)){
      surfacecode = "3"
    }
    else if([2,12,17,11,18].includes(surface)){
      surfacecode = "4"
    } 
    else if (surface == 15)  {
      surfacecode = "5";
    }
    else{
      surfacecode = "0";
    }

    return surfacecode;
  }

  export function GetRoadColor(surfacecode) { 
    surfacecode = parseInt(surfacecode)
    /*0 desconocido
    1 asfalto
    2 paved
    3 gravel
    4 ground
    5 sand
    6 carril bici*/   

    switch (surfacecode) {
      case 1:
        return colorRoad;
        break;
      case 2:
        return colorPaved;
        break;
      case 3:
        return colorGravel;
        break;
      case 4:
        return colorGround;
        break;
      case 5:
        return colorSand;
        break;
      case 6:
        return colorBikePath;
        break;
      case 10:
        return colorFerry;
        break;
      default:
        return colorDefault;
        break;
    }
  }


  function GetTrackColor(x){
    var indice = x%5;
    return colorsTracks[indice];
  }

  function GetRoadWeight(x) {    
    if ([1,2,3].includes(x))  return "6";
    if ([5].includes(x))  return "4";
    if([4,7].includes(x)) return "2";
    if([6].includes(x)) return "4";
    return "0";
  }

  function GetRoadDash(x) {    
    if ([1,2,3].includes(x))  return "0,0";
    if([4,7].includes(x)) return "1, 5";
    if([5,6].includes(x)) return "5, 5";
    return "0,0";
  }

  

  export function HasSurfaces(percentSurfaces){
    //si tiene más de una superficie o tiene una pero no es la 0 que es la desconocida.
    //console.warn(percentSurfaces[0][0])
    if(percentSurfaces.length > 1 || (percentSurfaces.length == 1 && !percentSurfaces[0][0]))
      return true;
    else
      return false;
  }


  export function GetWaytypeTextAndColorByCode(val){
    switch (parseInt(val)) {
      case 0:
        return ["unknown", "solid "+colorRoad]
        break;
      case 1:
        return ["road", "solid "+colorRoad];
        break;
      case 2:
        return ["track", "dashed "+colorRoad]
        break;
      case 3:
        return ["path","dotted "+colorRoad]
        break;
      case 4:
        return ["cycleway", "dashed "+colorRoad]
        break;
      case 5:
        return ["footway", "solid "+colorRoad];
        break;
      case 6:
        return ["steps", "solid "+colorRoad];
        break; 
      case 7:
        return ["ferry", "solid "+colorRoad];
        break;  
      case 7:
        return ["ferry", "solid "+colorRoad];
        break;  
      default:
        return ["unknown", "solid "+colorRoad]
        break;
  }
  }

  export function GetWaytypeCode(val){
    /*
    0 desconocido
    1 carretera
    2 pista
    3 sendero
    4 carrilbici
    5 acera
    6 escalones
    7 ferry

    */
    switch (parseInt(val)) {
      case 0:
        return 0;
        break;
      case 1:
        return 1;
        break;
      case 2:
        return 1;
        break;
      case 3:
        return 1;
        break;
      case 4:
        return 3;
        break;
      case 5:
        return 2;
        break;
      case 6:
        return 4;
        break;  
      case 7:
        return 5;
        break;    
      case 8:
        return 6;
        break; 
      case 9:
        return 7;
        break;  
      default:
        return 0;
        break;
  }
  }

  export function GerSurfaceTextAndColorByCode(code){
    switch (parseInt(code)) {
      case 0:
        return ["unknown", colorDefault]
        break;
      case 1:
        return ["asphalt", colorRoad]
        break;
      case 2:
        return ["paved", colorPaved]
        break;
      case 3:
        return ["gravel", colorGravel]
        break;
      case 4:
        return ["ground", colorGround]
        break;
      case 5:
        return ["sand", colorSand]
        break; 
      case 6:
        return ["bikepath", colorBikePath]
        break;   
      case 10:
        return ["water", colorFerry]
        break;   
      default:
        return ["unknown", colorDefault]
        break;
  }

  }
  

  export const cadadist = 100;

  export function GetIconRouteType(valor){
    valor = parseInt(valor)
    switch (valor) {
      case 0:
        return [null, null];
      case 1:
          return [direct, "lineal"];
      case 2:
          return [circular, "circular"];
      case 3:
        return [puerto, "puerto"];
      default:
        return [null, null];
    }
  }

  export function GetIconRouteActivity(valor){
    valor = parseInt(valor)
    switch (valor) {
      case 0:
        return [null, null];
      case 1:
          return [rally, "rally"];
      case 2:
          return [pruebamtb, "prueba_mtb"];
      case 3:
        return [pruebaroad, "prueba_carr"];
      default:
        return [null, null];
    }
  }

  export function GetIcon(travelMode){
    if(travelMode == "BICYCLING"){
        return "mtb"; 
    }else if(travelMode == "WALKING"){
        return "hiking"; 
    }
    else if(travelMode == "DRIVING"){
        return "road"; 
    }
    else if(travelMode == "DIRECT"){
        return "direct"; 
    }
    else{
        return "mtb"; 
    }
}
export function GetTextMode(travelMode){
  if(travelMode == "BICYCLING"){
      return "mod_bike"; 
  }else if(travelMode == "WALKING"){
      return "mod_walking"; 
  }
  else if(travelMode == "DRIVING"){
      return "mod_drive"; 
  }
  else if(travelMode == "DIRECT"){
      return "mod_eki"; 
  }
}

  export function GetIconRouteBicicleta(valor){
    valor = parseInt(valor)
    //consoleLOG("terreno es " + valor)
    switch (valor) {
      case 0:
        return [null, null];
      case 1:
          return [mtbBike, "b_mtb"];
      case 2:
          return [cicloturismo, "b_gravel"];
      case 3:
        return [roadBike, "b_carretera"];
      default:
        return [null, null];
    }
  }

  export function GetIconPinMap(ruta){
        var icon = "pin.png";
        if(ruta.actividad == 1){
          icon = "rally.png";
        }
        else if(ruta.actividad == 2){
            icon = "pruebamtb.png";
        }
        else if(ruta.actividad == 3){
            icon = "pruebaroad.png";
        }                                    
        else if(ruta.bicicleta == 1){
            icon = "roadpin.png";
        }
        else if(ruta.bicicleta == 2){
            icon = "cicloturismopin.png";
        }
        else if(ruta.bicicleta == 3){
            icon = "mtbpin.png";
        }
        return L.icon({
          iconSize: [45, 45],
          iconUrl: baseimages+icon
        });;
  }

  export function GetListTag(){
    var r = require.context('../public/icons', false, /\.(png)$/);
    let images = [];
    r.keys().map((item, index) => { 
      images.push(item.replace('./', '') )
    });
    return images;
    //return ["mountain","mountaine","mountain1","mountain2","mountain3","mountain4"];
  }

  
  export function GetIconTag(valor){
    return imagesArray[valor].default;
  }

  function importAll(r) {
    let images = {};
    r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
    return images;
  }
export function FormatPercent(val){
  val=Math.round(val);
  if(val == 0){
    return false;
  }
  return val;
}
/*

WAYTYPE
Carretera: 1,2,3    ANCHO 3 Contínuo
Pista/Camino:5   ANCHO:2  Contínuo
Sendero:4,7    Puntos discontínuo
Carril bici: 6     Rayas discontínuo
Escalones: 7    Cuadrados

SURFACE
Asfalto: 3    NEGRO
Pavimentado: 1,4,5,14,6,7    GRIS
Gravel:8,9, 10,11    AMARILLO OSCURO
Campo:2, 12,17,18     MARRÓN
Arena:15        AMARILLO CLARO

*/


export const ntobr = (string) => string.split('\n').map((line, i) => (
  <span key={i}>
      {line}
      <br/>
  </span>
))

export function brtonMeta(text){
    text = text.replace(/<br>/g, '\n');
    text = text.replace(/<br\/>/g, '\n');
    text = text.replace(/<br \/>/g, '\n');
    text = text.replace(/<br >/g, '\n');
    text = text.replace(/<BR>/g, '\n');
    text = text.replace(/<BR\/>/g, '\n');
    text = text.replace(/<BR \/>/g, '\n');
    text = text.replace(/<BR >/g, '\n');
    return text;
}

export const brton = (rawHTML) => React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });