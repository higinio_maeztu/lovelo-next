var CryptoJS = require("crypto-js");
import {
    osName,osVersion,browserName,browserVersion,deviceType
  } from "react-device-detect";


  
//export const apikeywbm =  'AIzaSyD792Mt_W7YPcwJiSiRCV3CmfaZZaOljvc';
export const apikeyekibike =  'AIzaSyAT7gRXBGjF9DdWUkvDdjsQcedMgwg4r3k';



export const locales = ['es','en'];

export const versionCode = "1.0.0";
//Versión de la base de datos (lo devuelve el ws y cambia con cada cambio)
export const VBD = 0;



/*export function Encrypt256(text){
    let encKey = "_Pumuky85"; 
    let iv = CryptoJS.enc.Hex.parse("FgLFXEr1MZl2mEnk");
    var encryptedText = CryptoJS.AES.encrypt(text, encKey, { iv: iv }).toString();
    return encryptedText;
}*/

export function CheckLogin(){
    if (typeof window !== "undefined"){
        const user =  localStorage.getItem('userID') ;
        if (user == null) {
            return null;
        }
        else{
            return {userID:localStorage.getItem('userID'), username:localStorage.getItem('user')}
        }
    }
    return null;
}

export function URLamigable(s){
        s = s.toLowerCase();
        
        var s1 = 'ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç';
        var s2 = 'AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc';
        for (var i = 0; i < s1.length; i++) s = s.replace(new RegExp(s1.charAt(i), 'g'), s2.charAt(i));
        s = s.replace(/\s/g, "_");
        s = s.replace(/__/g, "_")
        s = s.replace(/[^0-9a-zA-Z_]/g, "")
        
        

        return s;

}

export function Encrypt256(plain_text){
    var passphrase = "_Pumuky85";
    var salt = CryptoJS.lib.WordArray.random(256);
    var iv = CryptoJS.lib.WordArray.random(16);
    //for more random entropy can use : https://github.com/wwwtyro/cryptico/blob/master/random.js instead CryptoJS random() or another js PRNG

    var key = CryptoJS.PBKDF2(passphrase, salt, { hasher: CryptoJS.algo.SHA512, keySize: 64/8, iterations: 999 });

    var encrypted = CryptoJS.AES.encrypt(plain_text, key, {iv: iv});

    var data = {
        ciphertext : CryptoJS.enc.Base64.stringify(encrypted.ciphertext),
        salt : CryptoJS.enc.Hex.stringify(salt),
        iv : CryptoJS.enc.Hex.stringify(iv)    
    }

    return JSON.stringify(data);
}

export function GetParamsUserAgent(userID){
    
    return '?osName='+osName
    +'&osVersion='+osVersion
    +'&browserName='+browserName
    +'&browserVersion='+browserVersion
    +'&deviceType='+deviceType
    +'&userID='+userID;
    //+'&lat='+lat
    //+'&lng='+lng;
}



export async function SetJson(value){
    //await set('json_places', value);
    alert("setjson");
    await set('json_places', value);
}


export async function GetJson(){
    var places = await get('json_places');
    return places;
}

export function GetUserPosition(){
    const user =  localStorage.getItem('userID') ;
    return [localStorage.getItem('lat'), localStorage.getItem('lng')];
}

export function GetUserLogged(){
    const user =  localStorage.getItem('userID') ;
    if (user == null) {
        return null;
    }
    else{
        return {
            userID:  localStorage.getItem('userID'),
            username: localStorage.getItem('user')
          };
    }
}



  

  export function consoleLOG(mensaje){
        //var userID = localStorage.getItem('userID');
        
        //if(userID == 3)
            console.log(mensaje);
  }

  export function getLangCode(){
    if (typeof window !== "undefined") {
        //Damos preferencia a que la variable del idioma venga en la url
        var subdomain = window.location.host.split('.')[0];

        if(subdomain && subdomain == 'en'){
            return 'en'; 
        }
    }
    
    return 'es';    
  }


  export function GetTipo(types){
    var posibilidades = ["bar","restaurant","night_club","cafe"];

    for(var i =0;i < posibilidades.length ; i++){
        if(types.indexOf(posibilidades[i]) > -1)
            return posibilidades[i];
    }
}




export function distance(loc1, loc2) {
	if (loc1 == null || loc2 == null || loc1.lat == null || loc2.lat == null || (loc1.lat1 == loc2.lat2) && (loc1.lng == loc2.lng)) {
		return null;
	}
	else {
		var radlat1 = Math.PI * loc1.lat/180;
		var radlat2 = Math.PI * loc2.lat/180;
		var theta = loc1.lng-loc2.lng;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
        
        dist = dist * 1.609344;
        if(dist < 1){
            dist = dist.toFixed(1);
        }
        if(dist > 1){
            dist = dist.toFixed(0);
        }
		return dist+ " Km";
	}
}



/***************************** EKIBIKE  */




  export function SVGToImage(settings){
    let _settings = {
        svg: null,
        // Por lo general, todos los SVG tienen transparencia, por lo que PNG es el camino a seguir por defecto
        mimetype: "image/png",
        quality: 0.92,
        width: "auto",
        height: "auto",
        outputFormat: "base64"
    };

    // Anular la configuración predeterminada
    for (let key in settings) { _settings[key] = settings[key]; }

    return new Promise(function(resolve, reject){
        let svgNode;

        // Cree un nodo SVG si se ha proporcionado una cadena simple
        if(typeof(_settings.svg) == "string"){
            // Cree un nodo no visible para representar la cadena SVG
            let SVGContainer = document.createElement("div");
            SVGContainer.style.display = "none";
            SVGContainer.innerHTML = _settings.svg;
            svgNode = SVGContainer.firstElementChild;
        }else{
            svgNode = _settings.svg;
        }

        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d'); 

        let svgXml = new XMLSerializer().serializeToString(svgNode);
        let svgBase64 = "data:image/svg+xml;base64," + btoa(svgXml);

        const image = new Image();

        image.onload = function(){
            let finalWidth, finalHeight;

            // Calcule el ancho si se establece en automático y se especifica la altura (para preservar la relación de aspecto)
            if(_settings.width === "auto" && _settings.height !== "auto"){
                finalWidth = (this.width / this.height) * _settings.height;
            // Usar ancho original de la imagen
            }else if(_settings.width === "auto"){
                finalWidth = this.naturalWidth;
            // Usar ancho personalizado
            }else{
                finalWidth = _settings.width;
            }

            // Calcule la altura si se establece en automático y se especifica el ancho (para preservar la relación de aspecto)
            if(_settings.height === "auto" && _settings.width !== "auto"){
                finalHeight = (this.height / this.width) * _settings.width;
            // Usar altura original de la imagen
            }else if(_settings.height === "auto"){
                finalHeight = this.naturalHeight;
            // Usar altura personalizada
            }else{
                finalHeight = _settings.height;
            }

            // Definir el tamaño intrínseco del lienzo
            canvas.width = finalWidth;
            canvas.height = finalHeight;

            // Renderizar imagen en el lienzo
            context.drawImage(this, 0, 0, finalWidth, finalHeight);

            if(_settings.outputFormat == "blob"){
                // Completar y devolver la imagen de Blob
                canvas.toBlob(function(blob){
                    resolve(blob);
                }, _settings.mimetype, _settings.quality);
            }else{
                // Completar y devolver la imagen Base64
                resolve(canvas.toDataURL(_settings.mimetype, _settings.quality));
            }
        };

        // Cargue el SVG en Base64 a la imagen
        consoleLOG(svgBase64)
        image.src = svgBase64;
    });
}